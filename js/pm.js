load_data(pm_tools.clients[0],true)

function load__all_data(){

    var promises = [];

    pm_tools.clients.forEach(function(d){
        if(d !== "ALL"){
            promises.push(d3.csv("data/" + d + "-holdings.csv"));
            promises.push(d3.csv("data/" + d + "-funds.csv"));
            promises.push(d3.csv("data/" + d + "-coreholdings.csv"))
        }
    })

    Promise.all(promises).then(ready);

    function ready(all_datasets) {
        var holdings = [], funds = [], core_holdings = [];
        all_datasets.forEach(function(d,i){
            if(+i % 3 === 0){
                holdings = holdings.concat(all_datasets[i])
            } else if ((+i + 1) % 3 === 0){
                core_holdings = core_holdings.concat(all_datasets[i])
            } else {
                funds = funds.concat(all_datasets[i])
            }
        })
        holdings.columns = all_datasets[0].columns;
        draw_chart(holdings,funds,core_holdings,"chart_div",false);
    }

}

function load_data(my_client,initial_load){

    var promises = [
        d3.csv("data/" + my_client + "-holdings.csv"),
        d3.csv("data/" + my_client + "-funds.csv"),
        d3.csv("data/" + my_client + "-coreholdings.csv")
    ];

    Promise.all(promises).then(ready);

    function ready(all_datasets) {
        draw_chart(all_datasets[0],all_datasets[1],all_datasets[2],"chart_div",initial_load);
    }

}
function draw_chart(holdings,funds,core,div_id,initial_load) {

    var svg = draw_svg(div_id);
    var margin = 15;
    if(initial_load === true){
        //draw svg and chart
        svg.append("text").attr("x",margin).attr("y",margin + 15).attr("class","panel_title").text("FUNDS");
        svg.append("text").attr("class","total_funds").attr("text-anchor","end").text("Total Funds: ");
        add_button(svg,margin + 85,margin -3,"add filter","filter");
        add_button(svg,margin + 160,margin -3,"add fund","add_fund");
    }
    var asset_class = d3.set(funds, d => d.Group).values();
    var security_type = d3.set(funds, d => d.SecurityType).values();
    filter_functionality(svg,asset_class,security_type);
    pm_tools.fund_data = funds;
    pm_tools.all_funds = JSON.parse(JSON.stringify(holdings.columns)).splice(1,holdings.columns.length-1);
    pm_tools.all_holdings = holdings;
    pm_tools.filtered_holdings = holdings;
    var models = d3.set(holdings, d => d.Model).values();
    core = core.filter(d => models.indexOf(d.model) > -1);
    pm_tools.core_holdings = core.filter(d => pm_tools.all_funds.indexOf(d.ticker) > -1)
    draw_ticker_chart(svg);
    draw_table();
}

function add_fund(svg,my_fund){

    pm_tools.all_funds.push(my_fund);
    pm_tools.all_funds = pm_tools.all_funds.sort((a,b) => d3.ascending(a,b));
    add_fund(pm_tools.all_holdings,pm_tools.all_funds);
    add_fund(pm_tools.filtered_holdings,pm_tools.all_funds);
    draw_ticker_chart(svg);
    draw_table();

    function add_fund(my_dataset,my_columns){
        for(a in my_dataset){
            my_dataset[a][my_fund] = 0;
        }
        var new_columns = ["Model"];
        new_columns = new_columns.concat(my_columns);
        my_dataset.columns = new_columns;
    }
}

function filter_functionality(svg,asset_class,security_type){


    d3.selectAll(".python_button")
        .on("click",function(d){
            var my_holdings = JSON.parse(JSON.stringify(pm_tools.filtered_holdings));
            var my_json = {};
            var my_format = d3.format(".1f");
            for(m in my_holdings){
                var my_tickers = {}
                for(s in pm_tools.sell_tickers){
                    if(my_holdings[m][pm_tools.sell_tickers[s]] > 0){
                        my_tickers[pm_tools.sell_tickers[s]] = {
                            "weight": my_format(my_holdings[m][pm_tools.sell_tickers[s]]),
                            "action": "sell"
                        }
                    }
                }
                for(b in pm_tools.buy_tickers){
                    if(my_holdings[m][pm_tools.buy_tickers[b]] > 0){
                        my_tickers[pm_tools.buy_tickers[b]] = {
                            "weight": my_format(my_holdings[m][pm_tools.buy_tickers[b]]),
                            "action": "buy"
                        }
                    }
                }
                if(Object.keys(my_tickers).length > 0){
                    my_json[my_holdings[m].Model] = my_tickers;
                }
            }
            var div_html = '<span id="python_close_button" style="font-size:20px;">&#10005;</span><br>'
            d3.select(".python_div").style("visibility","visible").html(div_html + JSON.stringify(my_json));
            reset_python_button();
        })

    d3.select("#client_select")
        .selectAll('option')
        .data(pm_tools.clients)
        .enter()
        .append("option")
        .attr("id",d => "client_" + d)
        .text(d => d)

    d3.select("#client_select")
        .on("change",function(d){
            var my_client = d3.select(this).property("value");
            if(my_client === "ALL"){
                load__all_data();
            } else {
                load_data(my_client,false);
            }
        })

    d3.select("#client_" + pm_tools.clients[0]).node().selected === true;

    d3.selectAll(".button_rect")
        .on("click",function(){
            if(this.id === "filter"){
                d3.select("#add_filter").style("visibility","visible");
                d3.selectAll(".filter_type").each(function(d){
                    this.checked = false;
                })
                $('.selectpicker').selectpicker('hide');
            } else {
                var my_fund = prompt("Enter the new fund", "XXXXX");
                if(my_fund !== null){
                    add_fund(svg,my_fund);
                }
            }
        });

    d3.select("#close_button")
        .on("mouseover",function(){d3.select(this).style("cursor","pointer")})
        .on("mouseout",function(){d3.select(this).style("cursor","default")})
        .on("click",function(){
            d3.select("#add_filter").style("visibility","hidden");
        });

    function reset_python_button(){
        d3.select("#python_close_button")
            .on("mouseover",function(){
                d3.select(this).style("cursor","pointer")})
            .on("mouseout",function(){d3.select(this).style("cursor","default")})
            .on("click",function(){
                d3.select(".python_div").style("visibility","hidden");
            });
    }

    d3.select("#ticker_close_button")
        .on("mouseover",function(){d3.select(this).style("cursor","pointer")})
        .on("mouseout",function(){d3.select(this).style("cursor","default")})
        .on("click",function(){
            d3.selectAll(".ticker_rect").attr("fill","#D0D0D0");
            d3.select("#ticker_select").style("visibility","hidden");
        });

    d3.selectAll(".filter_type")
        .on("click",function(){
            var my_data = [""];
            if(this.value === "security"){
                my_data = my_data.concat(security_type);
            } else {
                my_data = my_data.concat(asset_class);
            }
            d3.select("#filter_select").selectAll("option").remove();
            d3.select("#filter_select")
                .selectAll('option')
                .data(my_data)
                .enter()
                .append("option")
                .attr("id",d => d)
                .text(d => d);
            $('.selectpicker').selectpicker('show');
            $('.selectpicker').selectpicker('render');
            $('.selectpicker').selectpicker('refresh');
            d3.selectAll(".filter_button").each(function(){this.disabled = true;})
        });

    d3.select("#filter_select")
      .on("change",function(){
            d3.selectAll(".filter_button").each(function(){this.disabled = false;})
       });

    d3.selectAll(".ticker_button")
        .on("click",function(d){
            if(this.id === "buy" && pm_tools.buy_tickers > 0){
                //only one buy allowed
            } else {
                pm_tools[this.id + "_tickers"].push(current_ticker);
                draw_filter_chart(svg);
                draw_ticker_chart(svg);
                draw_table();
                d3.select("#ticker_select").style("visibility","hidden");
            }
        })

    d3.selectAll(".filter_button")
        .on("click",function(){
            //get filter type
            var my_id = d3.selectAll(".filter_type").filter(function(){return this.checked === true}).node().id;
            var id_vars = {"asset":"Group","security":"SecurityType"};
            //get filter options and build set
            var filter_select = d3.selectAll("#filter_select").node().selectedOptions;
            var filter_set = [];
            for(f in filter_select){
                if(filter_select[f].value !== undefined){
                    filter_set.push(filter_select[f].value);
                }
            }
            if(filter_set.length > 0 && this.id === "highlight"){
                if(asset_class.indexOf(filter_set[0]) > -1){
                    pm_tools.highlight_assets.push(filter_set[0]);
                }
            }
            //build group of tickers which match criteria
            var fund_list = pm_tools.fund_data.filter(function(d){
                    if(filter_set.indexOf(d[id_vars[my_id]]) > -1){
                        return d
                    }
            });
            fund_set = d3.set(fund_list, d => d.Fund).values();

            //add them to the right list in pm_tools - ie hide or highlight
            var new_funds = []
            for(f in fund_set){
                new_funds.push(fund_set[f]);
            }
            new_funds = new_funds.sort((a,b) => d3.ascending(a,b));
            pm_tools[this.id + "_tickers"] = pm_tools[this.id + "_tickers"].concat(new_funds);
            //get the latest filter id
            var id_max = d3.max(pm_tools.current_filters, d => d.id);
            if(id_max === undefined){
                id_max = 0;
            } else {
                id_max += 1;
            }
            var highlight_count = pm_tools.current_filters.filter(d => d.type === "highlight").length;
            var my_colour = "#333333";
            if(this.id === "highlight"){
                my_colour = pm_tools.highlight_colours[highlight_count];
            }
            //add filter
            pm_tools.current_filters.push({
                "id":id_max,
                "filter": my_id.toUpperCase() + ": " + filter_set.join(", "),
                "funds": fund_set,
                "type": this.id,
                "type_name": filter_set.join(", "),
                "colour": my_colour
            });
            if(this.id === "highlight"){
                for(f in fund_set){
                    pm_tools.highlight_ticker_colours[fund_set[f]] = my_colour
                }
            }
            //draw charts - filter, ticker, table
            draw_filter_chart(svg);
            draw_ticker_chart(svg);
            draw_table();
            //close window and reset select and radio buttons
            d3.select("#add_filter").style("visibility","hidden");
            d3.selectAll(".filter_type").each(function(){
                this.checked = false;
            });
            $('.selectpicker').selectpicker('deselectAll');
        })

    d3.selectAll(".m_filter_type")
        .on("change",function(d){
            d3.select("#model_filter").node().value = "";
        })

    d3.selectAll("#model_only").on("change",function(){draw_table();});
    d3.selectAll("#core_only").on("change",function(){draw_table();});
    d3.selectAll("#remove_empty").on("change",function(){draw_table();});

    d3.select("#model_filter")
        .on("change",function(d){
            var filter_type = "";
            d3.selectAll(".m_filter_type").each(function(e){
                if(this.checked === true){
                    filter_type = this.id;
                }
            })
            var my_columns = pm_tools.filtered_holdings.columns;
            var my_filters = this.value.split("&");
            if(my_filters[0] === ""){
                pm_tools.filtered_holdings = pm_tools.all_holdings;
            } else {
                var result_needed = true;
                if(filter_type === "hide") {
                    result_needed = false;
                };
                pm_tools.filtered_holdings = pm_tools.all_holdings.filter(function(d){
                    var includes_filter = false;
                    for(f in my_filters){
                        if(d.Model.includes(my_filters[f]) === true){
                            includes_filter = true;
                        }
                    }
                    if(includes_filter === result_needed){
                        return d
                    }
                })
                pm_tools.filtered_holdings.columns = my_columns;
            }
            draw_table();
        })
}

function draw_ticker_chart(svg){

    var new_data = JSON.parse(JSON.stringify(pm_tools.all_funds));
    var my_data =  new_data.filter(d => pm_tools.hide_tickers.indexOf(d) === -1);

    var start_x = pm_tools.left_margin + pm_tools.margin;
    var width = +svg.attr("width") - start_x - (pm_tools.margin * 3);

    var my_chart = ticker_chart().width(width)
        .start_x(start_x)
        .start_y(pm_tools.margin + 5)
        .margin(pm_tools.margin)
        .my_class("ticker_chart")
        .my_data(my_data);

    my_chart(svg);

}

function draw_filter_chart(svg){

    var my_chart = filters_chart()
        .start_x(pm_tools.margin)
        .start_y(pm_tools.margin + 40)
        .my_class("filter_chart")
        .my_data(pm_tools.current_filters);

    my_chart(svg);

}

function draw_table(){

    if((pm_tools.sell_tickers.length + pm_tools.buy_tickers.length) > 0){
        d3.selectAll(".python_button").each(function(d){
            d3.select(this).node().disabled = false;
        })
    } else {
        d3.selectAll(".python_button").each(function(d){
            d3.select(this).node().disabled = true;
        })
    }
    var filter_count = pm_tools.sell_tickers.length + pm_tools.buy_tickers.length + pm_tools.highlight_tickers.length;
    if(filter_count > 0){
        d3.select("#model_only").node().disabled = false;
    } else {
        d3.select("#model_only").node().disabled = true;
    }
    var model_only = d3.select("#model_only").node().checked;
    var table_data = pm_tools.filtered_holdings;
    table_data = table_data.sort((a,b) => d3.ascending(a.Model.split(".")[1].split(" ")[0],b.Model.split(".")[1].split(" ")[0]));
    if(model_only === true){
        table_data =  model_only_filter();
    }
    d3.select("#model_count").text("TOTAL MODELS: " + table_data.length);

    var my_columns = prepare_table_data(pm_tools.filtered_holdings);
    var columns_set = d3.set(my_columns,d => d.title).values();;
    table_data = table_data.map(function(obj) {
        var my_array = {};
        for(c in columns_set){
            var my_val = obj[columns_set[c]];
            if(my_val === undefined){my_val = 0};
            my_array[columns_set[c]] = my_val;
        }
        return my_array;
    });
    for(m in my_columns){
        my_columns[m].data = +m;
        my_columns[m].data_index = m;
    }
    var cols_to_front = my_columns.filter(d => pm_tools.extra_cols_to_front.indexOf(d.title) > -1);
    cols_to_front = cols_to_front.concat(my_columns.filter(d => d.sell === true));
    cols_to_front = cols_to_front.concat(my_columns.filter(d => d.buy === true));
    for(h in pm_tools.highlight_tickers){
        if(my_columns.find(d => d.title === pm_tools.highlight_tickers[h]) !== undefined){
            cols_to_front.push(my_columns.find(d => d.title === pm_tools.highlight_tickers[h]))
        }
    }

    pm_tools.highlight_assets.forEach(function(d,i){
        var matching_funds = d3.set(pm_tools.fund_data, s => s.Group === d ? s.Fund : "").values();
        table_data.map(function(m){
            var total = 0;
            matching_funds.forEach(function(f){
                if(f.length > 0){
                    if(m[f] !== undefined){
                        total += +m[f];
                    }
                }
            })
            m[d] = total;

        });
        var data_index = my_columns.length;
        var new_column = {
            "buy": false,
            "data": data_index,
            "title": d,
            "className": "dt-left model_column",
            "data_index": data_index,
            "sell": false,
            "width": "25px"
        };
        my_columns.push(new_column);
        cols_to_front.splice(+i + 1,0,new_column);
    })
    cols_to_front = d3.set(cols_to_front, d => d.data_index).values().map(d => +d);
    var width = +d3.select("#table_div").style("width").split("px")[0];

    var my_chart = table().width(width)
        .start_x(0)
        .start_y(0)
        .my_class("ticker_table")
        .my_data(table_data)
        .cols_to_front(cols_to_front)
        .my_columns(my_columns);

    my_chart("#table_div");
}

function model_only_filter(){
    var highlighted_columns = d3.set(pm_tools.buy_tickers.concat(pm_tools.sell_tickers.concat(pm_tools.highlight_tickers))).values();
    var my_copy = JSON.parse(JSON.stringify(pm_tools.filtered_holdings));
    return my_copy.filter(function(d){
        var is_a_value = false;
        for(h in highlighted_columns){
            if(+d[highlighted_columns[h]] > 0){
                is_a_value = true;
            }
        }
        if(is_a_value === true){
            return d;
        }
    })
}
function prepare_table_data(my_data){
    var my_columns = JSON.parse(JSON.stringify(my_data.columns));
    var core_only = d3.select("#core_only").node().checked;
    if(core_only === true){
        for(var m in my_columns){
            if(+m > (0 + pm_tools.extra_cols_to_front.length)){
                var core = pm_tools.core_holdings.filter(d => d.ticker === my_columns[m] && d.iscore === "1");
                if(core.length === 0){
                    my_columns.splice(m,1);
                }
            }
        }
    }
    var remove_empty = d3.select("#remove_empty").node().checked;
    if(remove_empty === true){
        var filter_list = [];
       my_columns.forEach(function(d,i){
           if(+i > (0 + pm_tools.extra_cols_to_front.length)){
               var my_max = d3.max(pm_tools.filtered_holdings, m => +m[d]);
               if(my_max === 0){
                   filter_list.push(d);
               }
           }
       })
        my_columns = my_columns.filter(f => filter_list.indexOf(f) === -1);
    }
    var column_data = [];
    for(var c in my_columns){
        var sell = false, buy = false;
        var my_extent = d3.extent(pm_tools.filtered_holdings, d => +d[my_columns[c]] > 0 ? +d[my_columns[c]]: null);
        var my_domain = d3.range(my_extent[0],my_extent[1],(my_extent[1]-my_extent[0])/5);
        my_domain.splice(0,1);
        if(pm_tools.hide_tickers.indexOf(my_columns[c]) === -1){
            var col_width = "50px";
            var col_class = "dt-center";
            if(+c === 0){
                col_width = "100px";
                col_class = "dt-left model_column";
            }
            if(pm_tools.highlight_tickers.indexOf(my_columns[c]) > -1){
                col_class = "dt-center " + pm_tools.highlight_ticker_colours[my_columns[c]];
            }
            if(pm_tools.sell_tickers.indexOf(my_columns[c]) > -1){
                sell = true;
                col_class = "dt-center sell_ticker";
            }
            if(pm_tools.buy_tickers.indexOf(my_columns[c]) > -1){
                buy = true;
                col_class = "dt-center buy_ticker";
            }
            column_data.push({
                "data": +c,
                "title": my_columns[c],
                "className":col_class,
                "width": col_width,
                "sell": sell,
                "buy": buy
            })
        }
    }
    return column_data;

}

function add_button(svg,my_x,my_y,my_text,my_id){

    svg.append("text")
        .attr("pointer-events","none")
        .attr("class","button_text")
        .attr("id",my_id)
        .attr("y",my_y + 15.5)
        .text(my_text);

    var my_width = document.getElementById(my_id).getBoundingClientRect().width;

    svg.append("rect")
        .attr("class","button_rect")
        .attr("id",my_id)
        .attr("x",my_x)
        .attr("y",my_y)
        .attr("width",my_width + 20)
        .attr("height",25);

    d3.select("text#" + my_id)
        .attr("x",my_x + ((my_width + 20)/2))
        .raise();



}
function draw_svg(div_id){

    //draw svg - responsive to container div.
    var chart_div = document.getElementById(div_id);
    var width = chart_div.clientWidth;
    var height = chart_div.clientHeight;

    if(width < 1200){
        width = 1200;
    }
    if(d3.select("." + div_id + "_svg")._groups[0][0] === null){
        var svg = d3.select("#" + div_id)
            .append("svg")
            .attr("class",div_id + "_svg")
            .attr("width",width)
            .attr("height",height);

    } else {
        var svg = d3.select("." + div_id + "_svg");
    }
    return svg;
}
