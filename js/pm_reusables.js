function ticker_chart() {
    //REUSABLE ticker chart

    var width=0,
        margin=0,
        start_x=0,
        start_y=0,
        my_data = [],
        my_class="";


    function my(svg) {

        //calculate boxes per row, number of rows and scale
        var boxes_per_row = parseInt(width/(pm_tools.ticker_box_width+5));
        var x_scale = d3.scaleBand().paddingInner(0.05).domain(d3.range(0,boxes_per_row,1)).range([0,width]);
        var no_of_rows = (parseInt(my_data.length/boxes_per_row) + 1);
        var box_gap = (x_scale.bandwidth()*0.05);
        //get new height and reset heights of chart and middle divs
        var ticker_panel_height = (margin * 2) + (no_of_rows * pm_tools.ticker_box_height);
        var original_height = +svg.attr("height");
        svg.attr("height", ticker_panel_height + (margin*3));
        d3.select(".total_funds").attr("x",width + start_x).attr("y",ticker_panel_height + (margin*3) - 15)
            .text("TOTAL FUNDS: " + my_data.length);
        d3.select("#chart_div").style("height",(ticker_panel_height + (margin*3)) + "px");
        var height_difference = (ticker_panel_height + (margin*3)) - original_height;
        var middle_panel_height = window.innerHeight * 0.65;
        d3.select("#table_div").style("height",(middle_panel_height - height_difference) + "px");
        d3.select("#middle_div").style("height",(middle_panel_height - height_difference) + "px");
        d3.selectAll("#margin_div").style("height",(middle_panel_height - height_difference) + "px");

        //fund panel
        if(d3.select(".fund_panel")._groups[0][0] == null) {
            svg.append("rect").attr("class","fund_panel fund_panel");
        }

        d3.select(".fund_panel")
          .attr("width",width + (margin*2))
          .attr("height",ticker_panel_height)
          .attr("x", pm_tools.left_margin)
          .attr("y",margin-3);

        //ticker rects and text labels
        var my_group = svg.selectAll('.ticker_group')
            .data(my_data, d => d)
            .join(function(group){
                var enter = group.append("g").attr("class","ticker_group");
                enter.append("rect").attr("class","ticker_rect");
                enter.append("text").attr("class","ticker_text");
                return enter;
            });

        my_group.select(".ticker_rect")
            .attr("id", d => d)
            .attr("fill",get_fill)
            .attr("width",x_scale.bandwidth())
            .attr("height",pm_tools.ticker_box_height)
            .attr("x",(d,i) => x_scale(i % boxes_per_row) + 2)
            .attr("y",(d,i) => parseInt(i/boxes_per_row) * (pm_tools.ticker_box_height + box_gap))
            .attr("transform","translate(" + start_x + "," + start_y + ")")
            .on("mouseover",function(d){
                var my_fund = pm_tools.fund_data.find(f => f.Fund === d);
                if(my_fund !== undefined){
                    var tooltip_text = "Fund: " + my_fund.Fund + "<br>"
                        + "<strong>Name: </strong>" + my_fund.Name + "<br>"
                        + "<strong>AvgWgt: </strong>" + my_fund.AvgWgt + "<br>"
                        + "<strong>Trending Score: </strong>" + my_fund.TrendingScore + "<br>"
                        + "<strong>Value Score: </strong>" + my_fund.ValueScore + "<br>"
                        + "<strong>Forecast: </strong>" + my_fund.Forecast + "<br>"
                        + "<strong>Importance: </strong>" + my_fund.Importance + "<br>"
                        + "<strong>Security Type: </strong>" + my_fund.SecurityType + "<br>"
                        + "<strong>Group: </strong>" + my_fund.Group + "<br>"
                        + "<strong>Focus: </strong>" + my_fund.Focus;

                    d3.select(".ticker_tooltip")
                        .style("left",(d3.event.x + 15) + "px")
                        .style("top",d3.event.y + "px")
                        .style("visibility","visible")
                        .html(tooltip_text);
                }
            })
            .on("mouseout",function(d){
                d3.select(this).attr("cursor","default").transition().duration(300).attr('fill-opacity',1);
                d3.select(".ticker_tooltip").style("visibility","hidden");
            })
            .on("click",function(d){
                if(is_buy_sell(this.id) === true){
                    var sell_index = pm_tools.sell_tickers.indexOf(this.id);
                    if(sell_index > -1){
                        pm_tools.sell_tickers.splice(sell_index,1);
                    }
                    var buy_index = pm_tools.buy_tickers.indexOf(this.id);
                    if(buy_index > -1){
                        pm_tools.buy_tickers.splice(sell_index,1);
                    }
                    current_ticker = "";
                    d3.select(this).attr("fill",get_fill(d));
                    draw_ticker_chart(svg);
                    draw_table();
                } else {
                    current_ticker = this.id;
                    d3.select(this).attr("fill","white");
                    d3.select("#ticker_select").style("visibility","visible");
                }

            })

        my_group.select(".ticker_text")
            .attr("pointer-events","none")
            .attr("x",(d,i) => x_scale(i % boxes_per_row) + 2+ (x_scale.bandwidth()/2))
            .attr("y",(d,i) => 3 + (pm_tools.ticker_box_height/2) +  parseInt(i/boxes_per_row) * (pm_tools.ticker_box_height + box_gap))
            .attr("transform","translate(" + start_x + "," + start_y + ")")
            .text(d => d);

        function is_buy_sell(my_ticker){
            if(pm_tools.buy_tickers.indexOf(my_ticker) > -1){
                return true
            } else if (pm_tools.sell_tickers.indexOf(my_ticker) > -1){
                return true
            } else {
                return false;
            }
        }
        function get_fill(d){
            if (pm_tools.sell_tickers.indexOf(d) > -1) {
                return pm_tools.sell_colour;
            }  else if (pm_tools.buy_tickers.indexOf(d) > -1) {
                return pm_tools.buy_colour;
            } else  if (pm_tools.highlight_tickers.indexOf(d) > -1){
                return pm_tools.highlight_colour_codes[pm_tools.highlight_ticker_colours[d]]
            } else {
               return "#D0D0D0";
            }

        }

    }


    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.margin = function(value) {
        if (!arguments.length) return margin;
        margin = value;
        return my;
    };

    my.start_x = function(value) {
        if (!arguments.length) return start_x;
        start_x = value;
        return my;
    };

    my.start_y = function(value) {
        if (!arguments.length) return start_y;
        start_y = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}

function filters_chart() {
    //REUSABLE filters chart

    var start_x=0,
        start_y=0,
        my_data = [],
        my_class="";


    function my(svg) {


        //ticker rects and text labels
        var my_group = svg.selectAll('.filter_group')
            .data(my_data, d => d.id)
            .join(function(group){
                var enter = group.append("g").attr("class","filter_group");
                enter.append("text").attr("class","filter_text");
                enter.append("text").attr("class","filter_icon");
                return enter;
            });


        my_group.select(".filter_text")
            .attr("fill",d => pm_tools.highlight_colour_codes[d.colour])
            .attr("pointer-events","none")
            .attr("y",(d,i) => +i * 15)
            .attr("transform","translate(" + start_x + "," + start_y + ")")
            .text(d => d.filter);


        my_group.select(".filter_icon")
            .attr("id", d => d.id)
            .attr("x",pm_tools.left_margin - 40)
            .attr("y",(d,i) => +i * 15)
            .attr("transform","translate(" + start_x + "," + start_y + ")")
            .html("&#10005")
            .on("mouseover",function(){ d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(){ d3.select(this).attr("cursor","default")})
            .on("click",function(){
                //filter in reverse.
                var my_filter = pm_tools.current_filters.find(d => d.id === +this.id);
                if(my_filter.type === "highlight"){
                    var asset_index = pm_tools.highlight_assets.indexOf(my_filter.type_name);
                    pm_tools.highlight_assets.splice(asset_index,1);
                }
                for(f in my_filter.funds){
                    if(my_filter.type === "hide"){
                        var my_index = pm_tools.hide_tickers.indexOf(my_filter.funds[f]);
                        pm_tools.hide_tickers.splice(my_index,1);
                    } else {
                        var my_index = pm_tools.highlight_tickers.indexOf(my_filter.funds[f]);
                        pm_tools.highlight_tickers.splice(my_index,1);
                    }
                }
                pm_tools.current_filters = pm_tools.current_filters.filter(d => d.id !== +this.id);
                draw_filter_chart(svg);
                draw_ticker_chart(svg);
                draw_table();
            })


    }



    my.start_x = function(value) {
        if (!arguments.length) return start_x;
        start_x = value;
        return my;
    };

    my.start_y = function(value) {
        if (!arguments.length) return start_y;
        start_y = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}


function table() {
    //REUSABLE grouped bar chart

    var start_x=0,
        start_y=0,
        width=0,
        my_class="",
        my_data = [],
        my_columns,
        cols_to_front = [];

    function my(my_div) {

        d3.selectAll(my_div + " *").remove();

        // convert to a data array
        var raw_data = my_data.map(function (obj) {
            return Object.values(obj);
        });
        d3.selectAll("table").remove();
        var table = d3.select(my_div).append('table').attr("class", "compact cell-border hover nowrap");

        var data_table = $(table._groups[0][0]).DataTable({
            data: raw_data,
            'paging': false,
            'info': true,
            fixedHeader: {header: false},
            'ordering': false,
            'colReorder': true,
            'searching': false,
            "columns": my_columns,
            "columnDefs": [ // indexed by columns of generated table
                {
                    "targets": 0, // model
                    "render": function (data) {
                        var my_data = data;
                        var model_formatted = data.replace('.',' ').toLowerCase();
                        model_formatted = model_formatted.split(" ").join("-");
                        return '<span class="model_cell" ><a target="_blank", href = "https://sites.google.com/a/alvariumanalytics.com/alv/home?model='
                            + model_formatted +  '">' + my_data + '</a></span>'

                    }
                }],
            dom: 'ftB'
        });

        var highlight_scale = d3.scaleThreshold().domain(pm_tools.percent_thresholds)
            .range(["small","smallish","middle","largeish","large"]);

        data_table.cells().every( function (row,column) {
            var data = this.data();
            var my_format = d3.format(".1f");
            if(+data > 0){
                var current_columns = Object.keys(my_data[row])
                var my_column = current_columns[column];
                var my_row = my_data[row].Model;
                var my_core = pm_tools.core_holdings.find(d => d.model === my_row && d.ticker === my_column);
                if(my_core !== undefined){
                    if(my_core.iscore === "1"){
                        $(this.node()).addClass('core_cell');
                    }
                }

               this.data("<span class='" + highlight_scale(+data) + "'>" + my_format(data) + "</span>");
            } else {
                if(isNaN(data) === false){
                    this.data("");
                }
            }
        } );

        if (cols_to_front.length > 0) {
            var my_order = data_table.colReorder.order();
            var my_position = 1;
            for (c in cols_to_front) {
                my_order = my_order.filter(d => d !== cols_to_front[c]);
                my_order.splice(my_position, 0, cols_to_front[c]);
                my_position += 1;
            }
            data_table.colReorder.order(my_order);
        }


    }



    my.start_x = function(value) {
        if (!arguments.length) return start_x;
        start_x = value;
        return my;
    };

    my.my_columns = function(value) {
        if (!arguments.length) return my_columns;
        my_columns = value;
        return my;
    };


    my.start_y = function(value) {
        if (!arguments.length) return start_y;
        start_y = value;
        return my;
    };

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.cols_to_front = function(value) {
        if (!arguments.length) return cols_to_front;
        cols_to_front = value;
        return my;
    };



    return my;
}

