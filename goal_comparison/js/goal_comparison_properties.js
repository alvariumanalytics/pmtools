var g_comp_props = {
    colors:{"gold":"#FDC808","lightgold":"#fef1c2","green":"#6f855b","lightgreen":"#c7e0b1"},
    transition_time: 2000,
    risk_colours:{CON:"#cab2d6","MODCON":"#b2df8a","MOD":"#fdbf6f", "MODAGG": "#fb9a99","AGG":"#a6cee3","SLEEVE":"#A0A0A0","CUSTOM":"#D0D0D0"},
    risk_order:["CON","MODCON","MOD","MODAGG","AGG","SLEEVE","CUSTOM"],
    children:{"default":["default","Maximum Returns", "Small Sized","ESG"],"taxable default":["taxable default", "Tax Sensitive"]}

};
