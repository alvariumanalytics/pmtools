function goals_comparison_chart() {


    var width=0,
        height=0,
        margins=0,
        my_data = [],
        my_class="";


    function my(svg) {

        //defaults on left
        //space - left margin space, rest on right

        var default_sets = my_data.sets.filter(f => my_data.status[f][0].parent === "Default");
        default_sets.unshift("default");
        var taxable_default_sets = my_data.sets.filter(f => my_data.status[f][0].parent === "Taxable Default");
        taxable_default_sets.unshift("taxable default");
        my_data.sets = default_sets.concat(taxable_default_sets);
        var factor_width = (width - margins.right - (margins.left * my_data.sets.length-1))/my_data.sets.length;
        var factor_transform_y = 50;
        var set_height = height - margins.top - margins.bottom;
        var factor_height = (height - (margins.top*2) - margins.bottom - factor_transform_y)/my_data.factors.length;
        var position_colour = d3.scaleLinear().domain([0,5]).range([g_comp_props.colors.gold,g_comp_props.colors.lightgold])
        var taxable_position_colour = d3.scaleLinear().domain([0,5]).range([g_comp_props.colors.green,g_comp_props.colors.lightgreen])

        var triangle_x_scale = d3.scaleLinear().domain([0,10]).range([0,factor_width - 70 - (margins.left*2)]);
        var triangle = d3.symbol().type(d3.symbolTriangle).size(80);

        var my_group = svg.selectAll('.goals_group_' + my_class)
            .data(my_data.sets)
            .join(function(group){
                var enter = group.append("g").attr("class","goals_group_" + my_class);
                enter.append("rect").attr("class","goal_rect");
                enter.append("text").attr("class","goal_text");
                enter.append("g").attr("class","factor_group");
                return enter;
            });

        my_group.attr("transform",(d,i) => "translate(" + ((i * (factor_width+margins.left))+ margins.left) + "," + margins.top + ")");

        my_group.select(".goal_rect")
            .style("fill",(d,i) => d.includes("default") ? "#F0F0F0" : "white")
            .attr("width", factor_width)
            .attr("height",set_height);

        my_group.select(".goal_text")
            .attr("x",(d,i) => factor_width/2)
            .attr("y",factor_transform_y - 20)
            .text(d => d)

        my_group.select(".factor_group")
            .attr("transform","translate(" + margins.left + "," + factor_transform_y + ")");

        var factor_group = my_group.select(".factor_group").selectAll('.goals_factor_group_' + my_class)
            .data(function(d){
                var f_data = [];
                if(d === "default"){
                    f_data =  my_data.defaults;
                    f_data.map(m => m.width = factor_width - 20);
                    f_data.map(m => m.start_fill = position_colour(my_data.factors.indexOf(m.factor)));
                    f_data.map(m => m.fill = m.start_fill);
                    f_data.map(m => m.stroke_colour = g_comp_props.colors.gold);
                    f_data.map(m => m.type = "default");
                } else if (d === "taxable default") {
                    f_data =  my_data.taxable_defaults;
                    f_data.map(m => m.width = factor_width - 20);
                    f_data.map(m => m.start_fill = taxable_position_colour(my_data.factors.indexOf(m.factor)));
                    f_data.map(m => m.fill = m.start_fill);
                    f_data.map(m => m.stroke_colour = g_comp_props.colors.green);
                    f_data.map(m => m.type = "default");
                } else {
                        f_data = my_data.status[d];
                        f_data.map(m => m.width = factor_width - 20);
                        f_data.map(m => m.fill = m.parent === "Default" ? position_colour(my_data.factors.indexOf(m.factor))
                            : taxable_position_colour(my_data.factors.indexOf(m.factor)));
                        f_data.map(m => m.stroke_colour = (m.parent === "Default" ? g_comp_props.colors.gold : g_comp_props.colors.green));
                    f_data.map(m => m.start_fill = m.fill);
                     //   f_data.map(m => m.start_fill = "white");
                        f_data.map(m => m.type = "other");
                  }
                return f_data;
            })
            .join(function(group){
                var enter = group.append("g").attr("class","goals_factor_group_" + my_class);
                enter.append("rect").attr("class","factor_item factor_rect");
                enter.append("rect").attr("class","factor_item factor_text_rect");
                enter.append("text").attr("class","factor_item factor_text");
                enter.append("g").attr("class","factor_item positions_axis");
                enter.append("line").attr("class","factor_item value_change_line");
                enter.append("path").attr("class","factor_item shadow_triangle_marker shadow_positions_triangle");
                enter.append("path").attr("class","factor_item triangle_marker positions_triangle");
                return enter;
            });

        factor_group
            .attr("transform", (d,i) => "translate(0," + (i  * factor_height) + ")")
            .transition()
            .delay(g_comp_props.transition_time/2)
            .duration(g_comp_props.transition_time)
            .attr("transform", d => "translate(0," + (d.position * factor_height) + ")")

        factor_group.select(".factor_text_rect")
            .attr("pointer-events","none")
            .attr("id", d => d.factor)
            .attr("width",d => 50)
            .attr("height",factor_height - 4)
            .attr("stroke-width",0)
            .attr("fill",d => d.start_fill)

        factor_group.select(".factor_rect")
            .attr("id", d => d.factor)
            .attr("width",d => d.width)
            .attr("height",factor_height - 4)
            .attr("stroke-width",2)
            .attr("stroke",d => d.start_fill)
            .on("mouseover",function(d){
                d3.selectAll(".factor_item").attr("opacity",0);
                d3.selectAll(".factor_rect.factor_item").attr("fill","white");
                d3.selectAll("#" + this.id).attr("opacity",1);
                d3.selectAll(".factor_rect#" + this.id).attr("fill",f => f.fill);
                var tooltip_text = "<strong>" + my_data.factor_names[d.factor] + "</strong>";
                tooltip_text += "<br>(" + my_data.factor_labels[d.factor][0] +
                    (my_data.factor_labels[d.factor][1] !== "" ? " => " + my_data.factor_labels[d.factor][1] : "") +
                    " => " + my_data.factor_labels[d.factor][2] + ")"
                d3.select(".tooltip")
                    .style("visibility","visible")
                    .style("left", (d3.event.offsetX + 10) + "px")
                    .style("top",d3.event.offsetY + "px")
                    .html(tooltip_text);
            })
            .on("mouseout",function(d){
                d3.selectAll(".factor_item").attr("opacity",1);
                d3.selectAll(".factor_rect.factor_item").attr("fill","white");
                d3.select(".tooltip").style("visibility","hidden")
            })
            .attr("fill","white")
            .transition()
            .delay(g_comp_props.transition_time/2)
            .duration(g_comp_props.transition_time)
            .attr("stroke",d => d.fill);


        factor_group.select(".factor_text")
            .attr("pointer-events","none")
            .attr("id", d => d.factor)
            .attr("font-size",factor_height-20)
            .attr("x",5)
            .attr("dy",  (factor_height/2) + ((factor_height-20)/2) - 5)
            .text(d => d.factor);

        factor_group.select(".positions_axis")
            .attr("id", d => d.factor)
            .attr("transform",(d,i) => "translate(" + (d.width === factor_width ? 55 : 60) + "," + (factor_height/2) + ")")
            .call(d3.axisBottom(triangle_x_scale).tickFormat("").tickSizeOuter(0));

        factor_group.select(".value_change_line")
            .attr("id", d => d.factor)
            .attr("y1",factor_height/2)
            .attr("y2",factor_height/2)
            .attr("x1",d => ((d.width === factor_width ? 55 : 60) + triangle_x_scale(d.value - d.value_change)))
            .attr("x2",d => ((d.width === factor_width ? 55 : 60) + triangle_x_scale(d.value)))
            .attr("stroke","#333333")
            .attr("stroke-width",0)
            .transition()
            .delay(g_comp_props.transition_time*1.5)
            .duration(g_comp_props.transition_time/2)
            .attr("stroke-width",d => d.value_change === 0 ? 0 : 1)

        factor_group.select(".positions_triangle")
            .attr("id", d => d.factor)
            .style("stroke-width",1)
            .attr("d",triangle)
            .attr("transform",(d,i) => "translate(" + ((d.width === factor_width ? 55 : 60) + triangle_x_scale(d.value - d.value_change)) + "," +
                (factor_height/2) + ")")
            .on("mouseover",function(d){
                var tooltip_text = "Parent: " + d.parent + "<br>"
                + "Factor: " + my_data.factor_names[d.factor] + "<br>"
                + "Position: " + d.position + (d.position_change !== 0 ? " (" + (d.position_change > 0 ? "+" : "") + d.position_change + ")" : "") + "<br>"
                    + "Value: " + d.value + (d.value_change !== 0 ? " (" + (d.value_change > 0 ? "+" : "") + d.value_change + ")" : "") + "<br>"

                d3.select(".tooltip")
                    .style("visibility","visible")
                    .style("left",(d3.event.offsetX + 10) + "px")
                    .style("top",d3.event.offsetY + "px")
                    .html(tooltip_text)
            })
            .on("mouseout",function(d){
                d3.select(".tooltip").style("visibility","hidden");
            })
            .transition()
            .delay(g_comp_props.transition_time)
            .duration(g_comp_props.transition_time)
            .attr("transform",(d,i) => "translate(" + ((d.width === factor_width ? 55 : 60) + triangle_x_scale(d.value)) + "," +
                (factor_height/2) + ")")

        factor_group.select(".shadow_positions_triangle")
            .attr("id", d => d.factor)
            .attr("visibility", d => d.value_change === 0 ? "hidden" : "visible")
            .style("stroke-width",1)
            .attr("d",triangle)
            .attr("transform",(d,i) => "translate(" + ((d.width === factor_width ? 55 : 60) + triangle_x_scale(d.value - d.value_change)) + "," +
                (factor_height/2) + ")")



    }


    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}



function portfolio_allocation_chart() {


    var width=0,
        height=0,
        margins=0,
        my_data = [],
        my_class="";


    function my(svg) {

        var reverse_risk_order = JSON.parse(JSON.stringify(g_comp_props.risk_order));
        reverse_risk_order.reverse();

        if(d3.select(".portfolio_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","title portfolio_title" + my_class);
            reverse_risk_order.forEach(function(d,i){
                svg.append("text").attr("class","risk_label" + my_class + i);
                svg.append("rect").attr("class","risk_rect" + my_class + i);
            })
        }

        d3.select(".portfolio_title" + my_class)
            .attr("x",margins.left)
            .attr("y",20)
            .text("PORTFOLIO ALLOCATION (" + my_data.data[0].client + ")")

        var start_x = width - margins.left;

        reverse_risk_order.forEach(function(d,i){
            d3.select(".risk_rect" + my_class + i)
                .attr("width",15)
                .attr("height",8)
                .attr("x",start_x-15)
                .attr("y",10)
                .attr("fill",g_comp_props.risk_colours[d]);

            d3.select(".risk_label" + my_class + i)
                .attr("id","risk_label" + my_class + i)
                .attr("x",start_x - 20)
                .attr("y",18)
                .attr("text-anchor","end")
                .text(d);

            var risk_label_width = document.getElementById("risk_label" + my_class + i).getBoundingClientRect().width;

            start_x -= (25 + risk_label_width);

        })

        var max_count = d3.max(my_data.sets, m => my_data.data.filter(f => f.set === m).length);
        var box_height = height - margins.top - margins.bottom;

        var box_width = (width - margins.right - (margins.left * my_data.sets.length-1))/my_data.sets.length;
        var portfolio_width = box_width - margins.left - 10;
        var portfolio_height = (box_height-35)/max_count;

        var x_drag_domain = [];
        var x_drag_range = []

        for(i = 0 ; i < my_data.sets.length ; i ++){
            var my_threshold = (box_width * (i+1) + (margins.left * (i + 2)))
            if(i < (my_data.sets.length - 1)){
                x_drag_domain.push(my_threshold);
            }
            x_drag_range.push(String(i))
        }
        var x_drag = d3.scaleThreshold().domain(x_drag_domain).range(x_drag_range)


        var under_group = svg.selectAll('.under_group' + my_class)
            .data(my_data.sets)
            .join(function(group){
                var enter = group.append("g").attr("class","under_group" + my_class);
                enter.append("rect").attr("class","treemap_rect under_rect");
                return enter;
            });

        under_group.select(".under_rect")
            .attr("id",d => d.replace(/ /g,''))
            .attr("pointer-events","none")
            .attr("fill","white")
            .attr("width", box_width - 10)
            .attr("height",box_height)
            .attr("x",(d,i) => i * (box_width + margins.left))
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")

        var my_group = svg.selectAll('.fake_treemap_group_' + my_class)
            .data(my_data.sets)
            .join(function(group){
                var enter = group.append("g").attr("class","fake_treemap_group_" + my_class);
                enter.append("rect").attr("class","treemap_rect");
                enter.append("text").attr("class","treemap_text");
                enter.append("g").attr("class","portfolio_group");
                return enter;
            });

        my_group.select(".treemap_rect")
            .attr("pointer-events","none")
            .attr("fill","transparent")
            .attr("width", box_width - 10)
            .attr("height",box_height)
            .attr("x",(d,i) => i * (box_width + margins.left))
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")
            .on("mouseover",function(d){d3.select(this).attr("cursor","grab")})

        my_group.select(".treemap_text")
            .attr("pointer-events","none")
            .attr("x",(d,i) => (i * (box_width + margins.left)) + (box_width/2) + margins.left)
            .attr("y",d => margins.top + 20)
            .text(d => d);

       my_group.select(".portfolio_group")
            .attr("transform",(d,i) => "translate(" + (margins.left + (i * (box_width + margins.left)))
                + "," + (margins.top + 30) + ")");

        var portfolio_group =  my_group.select(".portfolio_group").selectAll('.portfolio_group' + my_class)
            .data(function(d,i){
                var portfolio_data = my_data.data.filter(f => f.set === d);
                portfolio_data.map(m => m.x = 5);
                portfolio_data.map((m,mi) => m.y = mi * portfolio_height)
                portfolio_data.map(m => m.translate_x = (i * (box_width + margins.left)) + margins.left);
                return portfolio_data;
            })
            .join(function(group){
                var enter = group.append("g").attr("class","portfolio_group" + my_class);
                enter.append("rect").attr("class","portfolio_rect");
                enter.append("text").attr("class","portfolio_text");
                return enter;
            });

        portfolio_group.select(".portfolio_rect")
            .attr("id",(d,i) => "portfolio_item_" + i + "_" + my_data.sets.indexOf(d.set))
            .attr("fill",d => g_comp_props.risk_colours[d.risk])
            .attr("x", d => d.x + 5)
            .attr("y", d => d.y)
            .attr("width",portfolio_width-10)
            .attr("height",portfolio_height - 2)
            .on("mouseover",function(d){d3.select(this).attr("cursor","grab")})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default")})
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended));

        portfolio_group.select(".portfolio_text")
            .attr("id",(d,i) => "portfolio_item_" + i  + "_" + my_data.sets.indexOf(d.set))
            .attr("pointer-events","none")
            .attr("x", d => d.x)
            .attr("y", d => d.y)
            .attr("transform","translate(" + (portfolio_width/2) + "," + ((portfolio_height/2)+2.5) + ")")
            .text(d => d.name);

        function dragstarted(d) {
            d.drag_start = d3.event.x;
            //bring group to the front
            d3.selectAll("#" + this.id).attr("cursor","grabbing");
        }

        function dragged(d) {
            d3.selectAll("#" + this.id)
                        .attr("x",d3.event.x)
                        .attr("y",d3.event.y)
                        .raise();
            var pos_x = x_drag(d3.event.x + d.translate_x);
            var new_set = my_data.sets[pos_x];

            d3.selectAll(".under_rect").attr("fill","white");
            var is_valid = g_comp_props.children[d.parent].indexOf(new_set) > -1;
            d3.select("#" + new_set.replace(/ /g,'')).attr("fill",is_valid === true ? "#D0D0D0" : "#fb9a99");
        }

        function dragended(d) {

            var pos_x = x_drag(d3.event.x + d.translate_x);
            var new_set = my_data.sets[pos_x];
            var new_data = JSON.parse(JSON.stringify(my_data.data));

            if(new_set === d.set){
                d3.selectAll("#" + this.id)
                    .transition()
                    .duration(300)
                    .attr("x",d.x + 5)
                    .attr("y",d.y);
            } else {
               if(g_comp_props.children[d.parent].indexOf(new_set) > -1){
                   new_data.find(f => f.id === d.id).set = new_set;
               } else {
                   d3.selectAll("#" + this.id)
                       .transition()
                       .duration(300)
                       .attr("x",d.x + 5)
                       .attr("y",d.y);
               }
            }
            draw_portfolio_chart(svg,{"data":new_data,"sets":my_data.sets})
        }

    }


    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}

