var promises = [
    d3.json("data/goals_data.json"),
    d3.json("data/alert_portfolio.json")
];

Promise.all(promises).then(ready);

function ready(all_datasets) {

    var goals_svg = draw_svg("goals_div");
    var goals_data = get_goals_data(all_datasets[0]);
    draw_goals_chart(goals_svg,goals_data);
    var portfolio_svg = draw_svg("portfolios_div");
    var portfolio_data = get_portfolio_data(all_datasets[1]);
    draw_portfolio_chart(portfolio_svg,{"data":portfolio_data,"sets":goals_data.sets});

}

function draw_goals_chart(svg,my_data) {

    var margins = {"left":10,"right":10,"top":10,"bottom":10};
    var width = +svg.attr("width");
    var height = +svg.attr("height");

    var my_chart = goals_comparison_chart()
        .width(width)
        .height(height)
        .margins(margins)
        .my_class("goals_comparison_chart")
        .my_data(my_data);

    my_chart(svg);

}

function draw_portfolio_chart(svg,my_data) {

    var margins = {"left":10,"right":10,"top":40,"bottom":10};
    var width = +svg.attr("width");
    var height = +svg.attr("height");

    var my_chart = portfolio_allocation_chart()
        .width(width)
        .height(height)
        .margins(margins)
        .my_class("portfolio_allocation_chart")
        .my_data(my_data);

    my_chart(svg);

}



function draw_svg(div_id){

    //draw svg - responsive to container div.
    var chart_div = document.getElementById(div_id);
    var width = chart_div.clientWidth;
    var height = chart_div.clientHeight;

    if(d3.select("." + div_id + "_svg")._groups[0][0] === null){
        var svg = d3.select("#" + div_id)
            .append("svg")
            .attr("class",div_id + "_svg")
            .attr("width",width)
            .attr("height",height);

    } else {
        var svg = d3.select("." + div_id + "_svg");
    }
    return svg;
}

function get_goals_data(my_data){

    var status = {}, defaults = [], taxable_defaults = [];
    var my_sets = Object.keys(my_data.sets);
    my_sets = my_sets.filter(f => f !== "Default");
    my_sets = my_sets.filter(f => f !== "Taxable Default");
    my_sets.forEach(s => status[s] = [])
    var my_factors = Object.keys(my_data.factor_names);



    my_data.sets["Default"].positions.forEach(function(p){
        defaults.push({"factor": p.id, "position":p.position,"value":p.value});
    })
    defaults = defaults.sort((a,b) => d3.ascending(a.position, b.position));
    defaults.map(m => m.value_change = 0);

    my_data.sets["Taxable Default"].positions.forEach(function(p){
        taxable_defaults.push({"factor": p.id, "position":p.position,"value":p.value});
    })
    taxable_defaults = taxable_defaults.sort((a,b) => d3.ascending(a.position, b.position));
    taxable_defaults.map(m => m.value_change = 0);


    my_factors = d3.set(defaults,s => s.factor).values();

    my_sets.forEach(function(s){
        my_factors.forEach(function(f){
            var my_parent = my_data.sets[s].parent;
            var my_default = my_data.sets[s].parent === "Default" ?  defaults.find(d => d.factor === f)
                : taxable_defaults.find(d => d.factor === f);
            var factor = my_data.sets[s].positions.find(d => d.id === f);
            var position = factor.position - my_default.position;
            var value = factor.value - my_default.value;
            status[s].push({"parent":my_parent,"factor": f, "position_change":position,"value_change":value, "position":factor.position, "value":factor.value});
        })
    })


    return {"factors":my_factors,"sets":my_sets,"defaults":defaults,"taxable_defaults":taxable_defaults,"status":status,
        "factor_names":my_data.factor_names,"factor_labels":my_data.factor_labels};
}


function assign_set_and_parent(portfolio) {
    var assignment = {};
    
    if (portfolio.includes(" NQ")) {
        assignment.set = "taxable default";
        assignment.parent = "taxable default";
    } else if (portfolio.includes(" TS")) {
        assignment.set = "Tax Sensitive";
        assignment.parent = "taxable default";
    } else if (portfolio.includes(" SML")) {
        assignment.set = "Small Sized";
        assignment.parent = "default";
    } else if (portfolio.includes(" STOCKS")) {
        assignment.set = "Maximum Returns";
        assignment.parent = "default";
    } else if (portfolio.includes(" ESG")) {
        assignment.set = "ESG";
        assignment.parent = "default";
    } else {
        assignment.set = "default";
        assignment.parent = "default";        
    }   
    return assignment;
}
    
    
function get_portfolio_data(my_data){
    var porfolios = Object.keys(my_data);
    var portfolio_data = [];
    var count = 0;
    porfolios = porfolios.filter(f => f.split(".")[0] === "KA");

    porfolios.forEach(function(p){
        var risk = p.split(".")[1].split(" ")[0];
        var assignment = assign_set_and_parent(p);
        portfolio_data.push({
            "id":count,
            //"name":p.split(".")[1].split(risk)[1].trim(),
            "name":p.split(".")[1].trim(),
            "risk":risk,
            "set":assignment.set,
            "parent":assignment.parent,
            "client": "KA"
        })
        count += 1;
    })

    portfolio_data = portfolio_data.sort((a,b) =>
        d3.ascending(g_comp_props.risk_order.indexOf(a.risk),g_comp_props.risk_order.indexOf(b.risk)))


    return portfolio_data;
}
