var promises = [
    d3.json("data/alerts.json"),
    d3.json("data/alert_portfolio.json"),
];

Promise.all(promises).then(ready);

function ready(all_datasets) {

    alert_props.all_alerts = all_datasets[0];

    var alerts = d3.set();
    var portfolios = d3.set();

    var links = [];
    for(key in all_datasets[1]){
        portfolios.add(key);
        all_datasets[1][key].forEach(function(v){
            alerts.add(v[0]);
            var my_alert = all_datasets[0].find(f => f.id === v[0]);
            var current_link = links.find(f => f.portfolio === key && f.alert === v[0]);
            if(current_link !== undefined){
                current_link.alerts += 1;
                current_link.value += ", " + v[1];
            } else {
                links.push({
                    "portfolio": key,
                    "alert": v[0],
                    "value": v[1],
                    "area": my_alert.area,
                    "importance": my_alert.importance,
                    "type": my_alert.type,
                    "alerts": 1
                })
            }
        })

    }
    alerts = alerts.values().sort((a,b) => d3.ascending(a,b));
    portfolios = portfolios.values().sort((a,b) => d3.ascending(a,b));

    var all_links = [];
    alerts.forEach(function(a){
        portfolios.forEach(function(p){
            all_links.push({
                "alert":a,
                "portfolio":p
            })
        })
    });

    var svg = draw_svg("chart_div");

    alerts = alerts.sort((a,b) => d3.ascending(links.find(f => f.alert === a).importance,links.find(f => f.alert === b).importance));

    var portfolio_filters = d3.set();

    portfolios.forEach(function(p){
        var split_1 = p.split(".");
        portfolio_filters.add(split_1[0]);
        var split_2 = split_1[1].split(" ");
        split_2.forEach(s => portfolio_filters.add(s))
    })

    alert_props.alert_filters = d3.set(all_datasets[0], d => d.area).values();
    alert_props.all_data = {"alerts":alerts,"all_links": all_links,"links":links,"portfolios":portfolios};
    draw_chart(svg,alert_props.all_data);
    draw_filter_menu(svg);
}


function filter_redraw(svg,my_type, my_val){
    var my_index = alert_props.filters[my_type].indexOf(my_val);
    if(my_index === -1){
        alert_props.filters[my_type].push(my_val);
    } else {
        alert_props.filters[my_type].splice(my_index,1)
    }

    var all_data = JSON.parse(JSON.stringify(alert_props.all_data));
    alert_props.filters["alert"].forEach(function(a){
        var filter_outs = d3.set(alert_props.all_alerts.filter(f => f.area === a), s => s.id).values();
        all_data.alerts = all_data.alerts.filter(f => filter_outs.indexOf(f) === -1);
    });

    alert_props.filters["portfolio"].forEach(function(p){
        all_data.portfolios = all_data.portfolios.filter(f => f.includes(p) === false);
    })
    all_data.links = all_data.links.filter(f => all_data.alerts.indexOf(f.alert) > -1)
    all_data.links = all_data.links.filter(f => all_data.portfolios.indexOf(f.portfolio) > -1)

    var all_links = [];
    all_data.alerts.forEach(function(a){
        all_data.portfolios.forEach(function(p){
            all_links.push({
                "alert":a,
                "portfolio":p
            })
        })
    });
    all_data.all_links = all_links;

    draw_chart(svg, all_data);

}
function draw_filter_menu(svg){

    var width = +svg.attr("width");
    var margins = {"left":width - 210,"right":5,"top":70,"bottom":25};

    var height = +svg.attr("height");

    var my_chart = filter_menu()
        .width(width)
        .height(height)
        .margins(margins)
        .my_class("filter_menu")
        .my_data({"portfolio": alert_props.portfolio_filters,"alert":alert_props.alert_filters});

    my_chart(svg);
}
function draw_chart(svg,my_data) {

    var margins = {"left":25,"right":250,"top":25,"bottom":25};
    var width = +svg.attr("width");
    var height = +svg.attr("height");

    var my_chart = alert_grid()
        .width(width)
        .height(height)
        .margins(margins)
        .my_class("alert_grid")
        .my_data(my_data);

    my_chart(svg);

}


function draw_svg(div_id){

    //draw svg - responsive to container div.
    var chart_div = document.getElementById(div_id);
    var width = chart_div.clientWidth;
    var height = chart_div.clientHeight;

    if(d3.select("." + div_id + "_svg")._groups[0][0] === null){
        var svg = d3.select("#" + div_id)
            .append("svg")
            .attr("class",div_id + "_svg")
            .attr("width",width)
            .attr("height",height);

    } else {
        var svg = d3.select("." + div_id + "_svg");
    }
    return svg;
}
