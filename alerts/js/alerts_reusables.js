function alert_grid() {
    //REUSABLE ticker chart

    var width=0,
        height=0,
        margins=0,
        my_data = [],
        my_class="",
        left_margin = 220,
        top_margins = 110;


    function my(svg) {

        var portfolio_order = ["CON","MODCON","MOD","MODAGG","AGG","SLEEVE","CUSTOM"]

        my_data.portfolios = my_data.portfolios.sort(function(a,b){
            var client_a = a.split(".")[0];
            var risk_a = a.split(".")[1].split(" ")[0];
            var risk_index_a = portfolio_order.indexOf(risk_a);
            var client_b = b.split(".")[0];
            var risk_b = b.split(".")[1].split(" ")[0];
            var risk_index_b = portfolio_order.indexOf(risk_b);
            return d3.ascending(client_a,client_b) || d3.ascending(risk_index_a,risk_index_b)
        })

        var x_width = width - margins.left - margins.right - left_margin;
        var x_scale = d3.scaleBand().domain(my_data.alerts).range([0,x_width]);
        var square_size = (height - margins.top - top_margins - margins.bottom)/my_data.portfolios.length;
        var y_height = square_size * my_data.portfolios.length;
        var y_scale = d3.scaleBand().domain(my_data.portfolios).range([0,y_height]);

        var importance_set = d3.set(my_data.links,d => d.importance).values();
        var colours = ["#fee08b","#fdae61","#d73027"];
        var importance_scale = d3.scaleOrdinal().domain(importance_set).range(colours);



        var clients = d3.set(my_data.portfolios, d => d.split(".")[0]).values();
        clients = clients.map((m) => m = {"client":m});
        clients.map(m => m.y = y_scale(my_data.portfolios.find(f => f.split(".")[0] === m.client)))
        clients.map(m => m.height = my_data.portfolios.filter(f => f.split(".")[0] === m.client).length * square_size);

        //ticker rects and text labels
        var client_group = svg.selectAll('.client_group' + my_class)
            .data(clients)
            .join(function(group){
                var enter = group.append("g").attr("class","client_group" + my_class);
                enter.append("rect").attr("class","client_rect");
                enter.append("text").attr("class","client_text");
                return enter;
            });

        client_group.select(".client_rect")
            .attr("x",0)
            .attr("y",d => d.y)
            .attr("width",left_margin + margins.left)
            .attr("height",d => d.height)
            .attr("fill",(d,i) => i % 2 === 0 ? "#F0F0F0" : "white")
            .attr("transform","translate(0," + (margins.top + top_margins) + ")");

        client_group.select(".client_text")
            .attr("font-size",10)
            .attr("font-weight","bold")
            .attr("dy",3)
            .text(d => d.client)
            .attr("transform",d => "translate(20," + (d.y + (d.height/2) + margins.top + top_margins) + ")")

        //fund panel
        if(d3.select(".x_axis" + my_class)._groups[0][0] == null) {
            svg.append("g").attr("class","axis x_axis" + my_class);
            svg.append("g").attr("class","axis y_axis" + my_class);
        }

        d3.select(".x_axis" + my_class)
            .call(d3.axisTop(x_scale).tickSizeOuter(0))
            .attr("transform","translate(" + (margins.left + left_margin) + "," + (margins.top + top_margins) + ")");

        d3.selectAll(".x_axis" + my_class + " .tick")
            .attr("transform",function(){
                var current_x = +d3.select(this).attr("transform").split("(")[1].split(",")[0];
                return "translate(" + (current_x + x_scale.bandwidth()/2) + ",0)";
            });

        d3.selectAll(".x_axis" + my_class + " .tick line")
            .attr("y2",1)
            .attr("y1",y_height);

        d3.selectAll(".x_axis" + my_class + " .tick text")
            .attr("id", d =>  "aindex_" + my_data.alerts.indexOf(d))
            .attr("text-anchor","start")
            .attr("y",0)
            .attr("transform","translate(" +(4-x_scale.bandwidth()/2) + ",-5) rotate(-90)")
            .text(d => d.replace(/_/g," "))
            .on("mouseover",function(d){

                d3.select(this).attr("cursor","pointer").style("font-weight","bold");

                var my_portfolios = my_data.links.filter(f => f.alert === d);
                my_portfolios.map(m => m.p_index = my_data.portfolios.indexOf(m.portfolio));
                var max_id = d3.max(my_portfolios, m => m.p_index);
                var portfolio_ids = d3.set(my_portfolios, s => s.p_index).values();
                var alert_index = my_data.alerts.indexOf(d);

                my_portfolios.forEach(function(p){
                    d3.selectAll("text#pindex_" + p.p_index).style("font-weight","bold").attr("visibility","visible");
                })

                d3.selectAll(".grey_rect")
                    .each(function(){
                        var my_id = this.id.split(":");
                        if(+my_id[0] < alert_index  && portfolio_ids.indexOf(my_id[1]) > -1){
                            d3.select(this).attr("opacity",1);
                        } else if(+my_id[1] < max_id  && +my_id[0] === alert_index){
                            d3.select(this).attr("opacity",1);
                        } else {
                            d3.select(this).attr("opacity",0);
                        }
                    })
            })
            .on("mouseout",function(d){
                d3.selectAll("text").style("font-weight","normal");
                d3.select(".tooltip").style("visibility","hidden");
                d3.select(this).attr("cursor","default");
                d3.selectAll(".grey_rect").attr("opacity",0);
                d3.selectAll(".y_axis" + my_class + " .tick text")
                    .attr("visibility", square_size < 10 ? "hidden" : "visible")
            })
            .call(wrap,100);

        d3.select(".y_axis" + my_class)
            .call(d3.axisLeft(y_scale).tickSizeOuter(0))
            .attr("transform","translate(" + (margins.left + left_margin) + "," + (margins.top + top_margins) + ")");

        d3.selectAll(".y_axis" + my_class + " .tick line")
            .attr("x2",1)
            .attr("x1",x_width);

        d3.selectAll(".y_axis" + my_class + " .tick text")
            .attr("font-size",10)
            .attr("id", d => "pindex_" + my_data.portfolios.indexOf(d))
            .attr("y", -y_scale.bandwidth()/2)
            .attr("visibility", square_size < 10 ? "hidden" : "visible")
            .text(d => d.split(".")[1])
            .on("mouseover",function(d){

                d3.select(this).attr("cursor","pointer").style("font-weight","bold");

                var my_alerts = my_data.links.filter(f => f.portfolio === d);
                my_alerts.map(m => m.a_index = my_data.alerts.indexOf(m.alert));
                var max_id = d3.max(my_alerts, m => m.a_index);
                var alert_ids = d3.set(my_alerts, s => s.a_index).values();
                var portfolio_index = my_data.portfolios.indexOf(d);

                my_alerts.forEach(function(a){
                    d3.selectAll("text#aindex_" + a.a_index).style("font-weight","bold").attr("visibility","visible");
                })

                d3.selectAll(".grey_rect")
                    .each(function(){
                        var my_id = this.id.split(":");
                        if(+my_id[1] < portfolio_index  && alert_ids.indexOf(my_id[0]) > -1){
                            d3.select(this).attr("opacity",1);
                        } else if(+my_id[0] < max_id  && +my_id[1] === portfolio_index){
                            d3.select(this).attr("opacity",1);
                        } else {
                            d3.select(this).attr("opacity",0);
                        }
                    })
            })
            .on("mouseout",function(d){
                d3.selectAll("text").style("font-weight","normal");
                d3.select(".tooltip").style("visibility","hidden");
                d3.select(this).attr("cursor","default");
                d3.selectAll(".grey_rect").attr("opacity",0);
                d3.selectAll(".y_axis" + my_class + " .tick text")
                    .attr("visibility", square_size < 10 ? "hidden" : "visible")
            });


        d3.selectAll(".y_axis" + my_class + " .tick")
            .attr("transform",function(){
                var current_y = +d3.select(this).attr("transform").split(",")[1].split(")")[0];
                return "translate(0," + (current_y + y_scale.bandwidth()/2) + ")";
            });


        //ticker rects and text labels
        var grey_group = svg.selectAll('.grey_squares_group' + my_class)
            .data(my_data.all_links, d => d.alert + ":" + d.portfolio)
            .join(function(group){
                var enter = group.append("g").attr("class","grey_squares_group" + my_class);
                enter.append("rect").attr("class","grey_rect");
                return enter;
            });

        grey_group.select(".grey_rect")
            .attr("opacity",0)
            .attr("id",d => my_data.alerts.indexOf(d.alert) + ":" + my_data.portfolios.indexOf(d.portfolio))
            .attr("width",x_scale.bandwidth())
            .attr("height",y_scale.bandwidth())
            .attr("x", d => x_scale(d.alert))
            .attr("y", d => y_scale(d.portfolio))
            .attr("fill","#D0D0D0")
            .attr("stroke-width",0)
            .attr("transform","translate(" + (margins.left + left_margin) + "," + (margins.top + top_margins) + ")");

        //ticker rects and text labels
        var my_group = svg.selectAll('.squares_group' + my_class)
            .data(my_data.links, d => d)
            .join(function(group){
                var enter = group.append("g").attr("class","squares_group" + my_class);
                enter.append("rect").attr("class","alert_rect");
                return enter;
            });

        my_group.select(".alert_rect")
            .attr("width",x_scale.bandwidth()-2)
            .attr("height",y_scale.bandwidth()-2)
            .attr("x", d => x_scale(d.alert) + 1)
            .attr("y", d => y_scale(d.portfolio) + 1)
            .attr("fill",d => importance_scale(d.importance))
            .attr("stroke",d => importance_scale(d.importance))
            .attr("transform","translate(" + (margins.left + left_margin) + "," + (margins.top + top_margins) + ")")
            .on("mouseover",function(d){
                d3.select(this).attr("cursor","pointer").style("fill-opacity",1);
                var tooltip_text = "<strong>Alert:</strong> " + d.alert + "<br>";
                tooltip_text +="<strong>Portfolio:</strong> " + d.portfolio + "<br>";
                if(d.alerts > 1){
                    tooltip_text += "<strong>Alert Count:</strong> " + d.alerts + "<br>";
                    tooltip_text += "<strong>Values:</strong> " + d.value + "<br>";
                } else {
                    tooltip_text += "<strong>Value:</strong> " + d.value + "<br>";
                }
                tooltip_text +="<strong>Area:</strong> " + d.area + "<br>";
                tooltip_text +="<strong>Type:</strong> " + d.type;


                d3.select(".tooltip")
                    .style("visibility","visible")
                    .style("left",(d3.event.x + 15) + "px")
                    .style("top",d3.event.y + "px")
                    .html(tooltip_text);

                var alert_index = my_data.alerts.indexOf(d.alert);
                var portfolio_index = my_data.portfolios.indexOf(d.portfolio);

                d3.selectAll("text#aindex_" + alert_index).style("font-weight","bold");
                d3.selectAll("text#pindex_" + portfolio_index).style("font-weight","bold").attr("visibility","visible");

                d3.selectAll(".grey_rect")
                    .each(function(){
                        var my_id = this.id.split(":");
                        if(+my_id[0] < alert_index  && +my_id[1] === portfolio_index){
                            d3.select(this).attr("opacity",1);
                        } else if(+my_id[1] < portfolio_index  && +my_id[0] === alert_index){
                            d3.select(this).attr("opacity",1);
                        } else {
                            d3.select(this).attr("opacity",0);
                        }
                    })
;
            })
            .on("mouseout",function(){
                d3.selectAll("text").style("font-weight","normal");
                d3.select(".tooltip").style("visibility","hidden");
                d3.select(this).attr("cursor","pointer").style("fill-opacity",0.2);
                d3.selectAll(".grey_rect").attr("opacity",0);
                d3.selectAll(".y_axis" + my_class + " .tick text")
                    .attr("visibility", square_size < 10 ? "hidden" : "visible")
            })



    }


    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}


function filter_menu() {

    var width=0,
        height=0,
        margins=0,
        my_data = [],
        my_class="";


    function my(svg) {

        var filter_groups = Object.keys(my_data.portfolio);
        filter_groups.push("");
        filter_groups.push("ALERTS");

        filter_groups = filter_groups.map(m => m = {"group": m});
        filter_groups.map(m => m.data = (m.group === "ALERTS" ? my_data.alert : (m.group === "" ? [] : my_data.portfolio[m.group])));
        var count = 0;
        filter_groups.forEach(function(d){
            d.accumulative_filters = count;
            count += d.data.length;
        })
        var total_filters = count + (filter_groups.length - 1);
        var filter_height = (height - margins.top - margins.bottom)/total_filters;

        if(filter_height > 15){
            filter_height = 15;
        }

        if(d3.select(".portfolio_title" + my_class)._groups[0][0] == null) {
            svg.append("text").attr("class","portfolio_title" + my_class);
        }

        d3.select(".portfolio_title" + my_class)
            .attr("font-weight","bold")
            .attr("font-size",14)
            .attr("x",margins.left)
            .attr("y",margins.top - 10)
            .text("PORTFOLIOS")

        var y_scale = d3.scaleLinear().domain([0,total_filters]).range([0,filter_height*total_filters]);

        var my_group = svg.selectAll('.filter_title_group' + my_class)
            .data(filter_groups)
            .join(function(group){
                var enter = group.append("g").attr("class","filter_title_group" + my_class);
                enter.append("text").attr("class","filter_title");
                enter.append("g").attr("class","filter_group")
                return enter;
            });

        my_group.select(".filter_title")
            .attr("font-weight",d => d.group === "ALERTS" ? "bold" : "normal")
            .attr("font-size",d => d.group === "ALERTS" ? 12 : 10)
            .attr("x",margins.left + 5)
            .attr("y",(d,i) => margins.top + y_scale(d.accumulative_filters) + 10 + (i * filter_height))
            .text(d => d.group);

        my_group.select(".filter_group")
            .attr("transform",(d,i) => "translate(" + margins.left + "," + (margins.top + y_scale(d.accumulative_filters) + 12 + (i * filter_height)) + ")");


        var sub_group =  my_group.select(".filter_group").selectAll('.filter_group' + my_class)
            .data(d => d.data)
            .join(function(group){
                var enter = group.append("g").attr("class","filter_group" + my_class);
                enter.append("foreignObject").attr("class","foreignobj")
                    .append("xhtml:div").attr("class","filter_checkboxdiv");
                enter.append("text").attr("class","filter_title");
                return enter;
            });

        sub_group.select(".foreignobj")
            .attr("y",(d,i) => (i * filter_height))
            .attr("width",20)
            .attr("height",20);

        sub_group.select(".filter_checkboxdiv")
            .html(d => "<input type='checkbox' class='filter_checkbox' id='" + d  + "' checked >");

        sub_group.select(".filter_title")
            .attr("x",22)
            .attr("y",(d,i) => 14 + (i * filter_height))
            .text(d => d);


        d3.selectAll(".filter_checkbox")
            .on("change",function(d){
                if(my_data.alert.indexOf(this.id) > -1){
                    filter_redraw(svg,"alert",this.id);
                } else {
                    filter_redraw(svg,"portfolio",this.id);
                }
            })

    }


    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}


function wrap(text, width) {
    text.each(function() {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.1, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
            }
        }
        text.selectAll("tspan").attr("y",-lineNumber*6)
    });
}
