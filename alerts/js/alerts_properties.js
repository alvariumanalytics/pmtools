var alert_props = {
    portfolio_filters:{
        "clients":["ALV","DA","FRL","KA","TS","PWC","MA","PWM"],
        "risk":["AGG","CON","MOD","MODAGG","MODCON"],
        "size":["LRG","SML"],
        "other":["ADV","SHARED","ESG","SLEEVE","STOCKS","PRESERVATION","CUSTOM","SHELL401K","ALLINTL","SMALLMID"],
        "tax":["NQ","Q"]},
    alert_filters:[],
    all_data:{},
    filters:{"alert":[],"portfolio":[]}
};
