//this file contains 2 x reusable charts - aster_donut and legend

function dial_chart() {
    //REUSABLE dial chart

    var width=0,
        height=0,
        my_data = [],
        new_data = [],
        my_class="",
        left_box_width= 140,
        margins = 40,
        tick_height = 20;


    function my(svg) {


        //reset group order so raise always works..
        for (var i = 0; i < my_data.length; i++) {
            d3.selectAll("#bar_group_" + i).raise();
        }
        //get bar range, calculate step and set y scale
        var bars = d3.range(my_data.length-1);
        var step = height/(bars.length+1);
        var axis_width = width - left_box_width - (margins*2);
        var y_scale = d3.scaleOrdinal().domain(bars).range(d3.range(0,height,step));
        var x_scale = d3.scaleLinear().domain([0,10]).range([0,axis_width]);
        var triangle = d3.symbol().type(d3.symbolTriangle).size(200);

        //define group
        var my_group = svg.selectAll(".bar_group").data(my_data);

        //exit remove
        my_group.exit().remove();
        //enter (and add group id and transform)
        var enter = my_group.enter().append("g").attr("class","bar_group")
                            .attr("id",d => "bar_group_" + d.id).attr("transform","translate(4,4)");
        //append group
        var items_group = enter.append("g").attr("class","bar_items").attr("pointer-events","none");

        //append items to group
        items_group.append("rect").attr("class","factor_bar");
        items_group.append("rect").attr("class","position_rect");
        items_group.append("text").attr("class","position_text");
        items_group.append("text").attr("class","position_label");
        items_group.append("text").attr("class","factor_title");
        items_group.append("rect").attr("class","restriction_rect_left");
        items_group.append("rect").attr("class","restriction_rect_right");
        items_group.append("g").attr("class","x_axis");
        items_group.append("path").attr("class","triangle_marker");

        //append invisible drag/drop bar
        enter.append("rect").attr("class","invisible_bar");
        enter.append("path").attr("class","invisible_triangle_marker");

        //merge
        my_group = my_group.merge(enter);
        //add properties to group items

        my_group.select(".triangle_marker")
            .attr("id",d => "triangle_" + d.id)
            .attr("d", triangle)
            .attr("transform",d => "translate(" + (left_box_width + margins + x_scale(d.value)) + "," + ((step*0.5)+15) + ")")


        my_group.select(".invisible_triangle_marker")
            .attr("d", triangle)
            .attr("transform",d => "translate(" + (left_box_width + margins + x_scale(d.value)) + "," + ((step*0.5)+15 + y_scale(d.position)) + ")")
            .call(d3.drag()
                    .on("start", triangle_started)
                    .on("drag", triangle_drag)
                    .on("end", triangle_dragended))


        my_group.select(".x_axis")
            .call(d3.axisBottom(x_scale).tickFormat(show_ticks).tickSizeOuter(0))
            .attr("transform","translate(" + (left_box_width + margins) + "," + (step*0.5) + ")");

        function show_ticks(d){
            var labels = alv.factor_labels[this.parentNode.parentNode.__data__.id];
            if(d === 0){
                return labels[0].toUpperCase()
            } else if (d === 5){
                return labels[1].toUpperCase()
            } else if (d === 10){
                return labels[2].toUpperCase()
            } else {
                return ""
            }
        }
        my_group.selectAll(".x_axis .tick line")
            .attr("y2",tick_height)

        my_group.selectAll(".x_axis .tick text")
            .attr("font-size","1.2em")
            .attr("dy",-tick_height+4)
            .attr("text-anchor",d => d === 0 ? "start" : (d === 5 ? "middle" : "end"))


        my_group.select(".restriction_rect_left")
            .attr("width",d => x_scale(d.restrict_left))
            .attr("height",tick_height)
            .style("fill", alv.texture.url())
            .attr("transform","translate(" + (left_box_width + margins) + "," + (step*0.5) + ")");

        my_group.select(".restriction_rect_right")
            .attr("width",d => x_scale(10) - (x_scale(d.restrict_right)))
            .attr("height",tick_height)
            .style("fill", alv.texture.url())
            .attr("transform",d => "translate(" + (left_box_width + margins + (x_scale(d.restrict_right))) + "," + (step*0.5) + ")");


        my_group.select(".position_rect")
            .attr("x",2)
            .attr("y",2)
            .attr("width",left_box_width)
            .attr("height",step-12)
            .attr("fill","#F0F0F0")


        my_group.select(".position_text")
            .attr("x",left_box_width/2)
            .attr("dy",step-36)
            .attr("font-size","6em")
            .attr("text-anchor","middle")
            .text(d => d.position+1);

        my_group.select(".position_label")
            .attr("fill","#33a02c")
            .attr("x",left_box_width/2)
            .attr("dy",40)
            .attr("font-size","2em")
            .attr("text-anchor","middle")
            .text("GOAL")

        my_group.select(".factor_title")
            .attr("fill","#1f78b4")
            .attr("x",left_box_width + ((width-left_box_width)/2))
            .attr("dy",40)
            .attr("font-size","2em")
            .attr("text-anchor","middle")
            .text(d => alv.factor_names[d.id].toUpperCase());

        my_group.select(".factor_bar")
            .attr("id",d => "fbar_" + d.id)
            .attr("width",width-8)
            .attr("stroke-width","4px")
            .attr("stroke","#A0A0A0")
            .attr("fill","white")
            .attr("height",step-8)

        //translate them
        my_group.select(".bar_items")
            .attr("id",d => "bar_item_" + d.id)
            .attr("transform",d => "translate(0," + y_scale(d.position) + ")")

        //add properties and drag action to invisible bar
        my_group.select(".invisible_bar")
            .attr("width",width)
            .attr("stroke-width","0")
            .attr("stroke","none")
            .attr("fill","transparent")
            .attr("height",step)
            .attr("y", d => y_scale(d.position))
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended));

        if(new_data.length > 0){
            var changes = new_data[0];
            if(changes.restrict_left !== undefined){
                my_group.select(".restriction_rect_left")
                    .transition()
                    .delay(d => get_delay(changes.restrict_left,d.id))
                    .duration(1000)
                    .style("fill", d => changes.restrict_left[d.id] !== undefined ? alv.texture_gold.url(): alv.texture.url())
                    .attr("width",d => changes.restrict_left[d.id] !== undefined ? x_scale(changes.restrict_left[d.id].value):  x_scale(d.restrict_left))
                    .transition()
                    .duration(1000)
                    .style("fill",alv.texture.url())
            }
            if(changes.restrict_right !== undefined){
                my_group.select(".restriction_rect_right")
                    .transition()
                    .delay(d => get_delay(changes.restrict_right,d.id))
                    .duration(1000)
                    .style("fill", d => changes.restrict_right[d.id] !== undefined ? alv.texture_gold.url(): alv.texture.url())
                    .attr("width",d => changes.restrict_right[d.id] !== undefined ? x_scale(10) - x_scale(changes.restrict_right[d.id].value):  x_scale(10) - x_scale(d.restrict_right))
                    .attr("transform", d => changes.restrict_right[d.id] !== undefined ? "translate(" + (left_box_width + margins + (x_scale(changes.restrict_right[d.id].value))) + "," + (step*0.5) + ")":  "translate(" + (left_box_width + margins + (x_scale(d.restrict_right))) + "," + (step*0.5) + ")")
                    .transition()
                    .duration(1000)
                    .style("fill",alv.texture.url())
            }
            if(changes.value !== undefined){

               my_group.select(".triangle_marker")
                    .transition()
                   .delay(d => get_delay(changes.value,d.id))
                    .duration(1000)
                    .style("fill",d => changes.value[d.id] !== undefined ? "gold": "white")
                    .attr("transform", d => changes.value[d.id] !== undefined ? get_triangle_transform(changes.value[d.id].value,0) : get_triangle_transform(d.value,0))
                    .transition()
                    .duration(1000)
                    .style("fill", "white");

               my_group.select(".invisible_triangle_marker")
                    .transition()
                   .delay(d => get_delay(changes.value,d.id))
                    .duration(1000)
                    .style("fill",d => changes.value[d.id] !== undefined ? "gold": "transparent")
                    .attr("transform", d => changes.value[d.id] !== undefined ? get_triangle_transform(changes.value[d.id].value, y_scale(d.position)) : get_triangle_transform(d.value, y_scale(d.position)))
                    .transition()
                    .duration(1000)
                    .style("fill","transparent")

            }

            function get_triangle_transform(new_x_value,extra){
                return "translate(" + (left_box_width+margins + x_scale(new_x_value)) + "," + (((step*0.5)+15) + extra) + ")";
            }

            function get_delay(my_object,my_id){
                return my_object[my_id] !== undefined ? (my_object[my_id].order * 1000) :  0;
            }

        }

        function triangle_started(d) {
            d3.selectAll("#triangle_" + d.id).style("fill","gold");
        }


        function triangle_drag(d){
            d3.selectAll("#triangle_" + d.id).attr("transform","translate(" + d3.event.x + "," + ((step*0.5)+15) + ")");
            d3.select(this).attr("transform","translate(" + d3.event.x + "," + ((step*0.5)+15 + y_scale(d.position)) + ")");
        }

        function triangle_dragended(d){
            //get the right point in the range
           var new_value = Math.round(x_scale.invert(d3.event.x - left_box_width - margins)).toFixed(0);
           if(new_value < d.restrict_left){
               new_value = d.restrict_left;
           } else if (new_value > d.restrict_right){
               new_value = d.restrict_right;
           }
            d3.selectAll("#triangle_" + d.id)
                .transition()
                .duration(1000)
                .style("fill","white")
                .attr("transform","translate(" + (left_box_width+margins + x_scale(new_value)) + "," + ((step*0.5)+15) + ")");

            d3.select(this)
                .transition()
                .duration(1000)
                .attr("transform","translate(" + (left_box_width+margins + x_scale(new_value)) + "," + ((step*0.5)+15 + y_scale(d.position)) + ")");

            var t = d3.timer(function(elapsed) {
                if (elapsed > 0) {
                    if(d.value !== new_value){
                        change_data(["value",alv.factor_names[d.id], d.id,d.value,new_value])
                    }
                    t.stop();
                }
            }, 1000);
        }


        function dragstarted(d) {
            //bring group to the front
            d3.selectAll("#bar_group_" + d.id).raise();
            d3.selectAll("#fbar_" + d.id).attr("fill","gold")
        }

        function dragged(d) {
            //change the y position (both invisible bar and group underneath)
            d3.select(this).attr("y",d3.event.y)
            d3.selectAll("#bar_item_" + d.id)
                .attr("transform",d => "translate(0," + d3.event.y + ")");
        }

        function dragended(d) {
            //get the right point in the range
            for(r in y_scale.range()){
                if(y_scale.range()[r] > d3.event.y) {
                    if(y_scale(d.position) > d3.event.y){
                        var new_position = +r;
                    } else {
                        var new_position = +r-1;
                    }

                    break;
                };
            }
            //reset if out of range
            if(new_position === undefined){new_position = y_scale.domain()[y_scale.domain().length-1];};
            //store id and old position
            var my_id = d.id;
            var old_position = d.position;

            //now change positions
            d3.selectAll(".invisible_bar").each(function(d){

                if(d.id === my_id){
                    d.position = new_position;
                } else if (d.position >= new_position  && d.position < old_position && old_position > new_position){
                        d.position += 1
                    } else if (d.position <= new_position && d.position >= old_position){
                        d.position -= 1
                    }
                d3.selectAll("#bar_item_" + d.id)
                    .attr("transform",d => "translate(0," + y_scale(d.position) + ")");
                })
                .transition()
                .duration(1000)
                .attr("y", d => y_scale(d.position))

            //reset position text values
            d3.selectAll(".position_text")
                .transition()
                .duration(1000)
                .text(d => d.position+1)

            //fade background back to normal
            d3.selectAll("#fbar_" + d.id).transition().duration(1000).attr("fill","white");

            var t = d3.timer(function(elapsed) {
                if (elapsed > 0) {
                    if(new_position !== old_position){
                        change_data(["position",alv.factor_names[d.id],d.id,old_position,new_position])
                    }
                    t.stop();
                }
            }, 1000);

        }

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.new_data = function(value) {
        if (!arguments.length) return new_data;
        new_data = value;
        return my;
    };


    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };



    return my;
}




function tree_map() {
    //REUSABLE dial chart

    var width=0,
        height=0,
        my_data = [],
        my_class="",
        bobble_radius = 25,
        colour_scale="",
        colour_var=""


    function my(svg) {

        bobble_radius = Math.min(width,height)/40;

        //set format and scales
        var my_format = d3.format(".1f");
        var sleeves = d3.set(my_data, d => d.sleeve).values();
        var x_scale = d3.scaleLinear().domain([0, width]).range([0, width]);
        var y_scale = d3.scaleLinear().domain([0, height]).range([0, height]);
        var parent_colour = d3.scaleOrdinal().domain(sleeves).range(alv.tree_fill_colours.sleeve);

        d3.selectAll(".fill_type").each(function(d){
            if(this.checked === true){
                if(this.value === "market"){
                     var markets = d3.set(my_data, d => d.Market).values();
                     colour_scale = d3.scaleOrdinal().domain(markets).range(alv.tree_fill_colours.market);
                     colour_var = "Market";
                } else if(this.value === "return"){
                    var domain = d3.range(d3.min(my_data, d => +d.TotalReturn),d3.max(my_data, d => +d.TotalReturn), (d3.max(my_data, d => +d.TotalReturn)-d3.min(my_data, d => +d.TotalReturn))/10);
                    colour_scale = d3.scaleThreshold().domain(domain).range(alv.tree_fill_colours.return);

                    colour_var = "TotalReturn";
                } else if(this.value === "weight"){
                    var domain = d3.range(d3.min(my_data, d => +d.Wgt),d3.max(my_data, d => +d.Wgt), (d3.max(my_data, d => +d.Wgt)-d3.min(my_data, d => +d.Wgt))/10);
                    colour_scale = d3.scaleThreshold().domain(domain).range(alv.tree_fill_colours.weight);
                    colour_var = "Wgt";
                } else{
                        colour_scale = d3.scaleOrdinal(d3.schemeCategory10).domain(sleeves);
                        colour_var = "sleeve";
                }
            }
        })
        var root_padding = 10;

        width = width - (root_padding*2);
        height = height - (root_padding*2);
        //set treemap
        var treemap = d3.treemap()
            .size([width, height])
            .paddingOuter(root_padding)
            .paddingInner(0)
            .round(true);

        //nest data and change key names
        my_data = d3.nest().key(d => d.sleeve).key(d => d.Ticker).entries(my_data)

        var str = JSON.stringify(my_data);
        str = str.replace(/key/g, 'name');
        str = str.replace(/values/g, 'children');

        my_data = JSON.parse(str);

        //define root (correct format) and treemap
        var root = d3.hierarchy({"name":"funds","children":my_data});
        treemap(root.sum(d => d.value)
            .sort((a, b) => b.height - a.height || b.value - a.value))

        //now the complicated bit!
        for(c in root.children){
            //loop through each parent
            var all_boxes = root.children[c].children;
            var total_boxes = all_boxes.length;

            //work out if start,middle,end,none (both vertically and horizontally)
            for(a in all_boxes) {
                all_boxes[a].id = "box_" + a + c;
                if (close_to(all_boxes[a].x0, root.children[c].x0 + root_padding) && close_to(all_boxes[a].x1, root.children[c].x1+ root_padding)) {
                    all_boxes[a].h_position = "none"
                } else if (close_to(all_boxes[a].x0, root.children[c].x0+ root_padding)) {
                    all_boxes[a].h_position = "start";
                } else if (close_to(all_boxes[a].x1, root.children[c].x1- root_padding)) {
                    all_boxes[a].h_position = "end";
                } else {
                    all_boxes[a].h_position = "middle";
                }

                if (close_to(all_boxes[a].y1, root.children[c].y1+ root_padding) && close_to(all_boxes[a].y0, root.children[c].y0+ root_padding)) {
                    all_boxes[a].v_position = "none"
                } else if (close_to(all_boxes[a].y0, root.children[c].y0+ root_padding)) {
                    all_boxes[a].v_position = "start";
                } else if (close_to(all_boxes[a].y1, root.children[c].y1- root_padding)) {
                    all_boxes[a].v_position = "end";
                } else {
                    all_boxes[a].v_position = "middle";
                }


                function close_to(val_1, val_2) {
                    //sometimes the parents and children don't quite line up (millimetres) so this check makes sure logic is working.
                    if(parseInt(val_1) === parseInt(val_2)){
                        return true
                    } else if (parseInt(val_1-1) === parseInt(val_2)){
                        return true
                    } else if (parseInt(val_1+1) === parseInt(val_2)){
                        return true
                    } else {
                        return false
                    }
                }
            }

            //now loop through the boxes and add the bobble positions.
            for(a in all_boxes){
                if(all_boxes[a].h_position === "middle"){
                    all_boxes[a].left_h = find_bobble_h_position(all_boxes[a],"left");
                   all_boxes[a].right_h = find_bobble_h_position(all_boxes[a],"right");
                }
                if(+a !== (all_boxes.length-1)){
                    if(all_boxes[a].h_position === "start" && all_boxes[+a+1].h_position === "end"){
                        all_boxes[a].right_h = find_bobble_h_position(all_boxes[a],"right");
                    }
                }
                if(all_boxes[a].v_position === "middle"){
                    all_boxes[a].top_v = find_bobble_v_position(all_boxes[a],"top");
                   all_boxes[a].bottom_v = find_bobble_v_position(all_boxes[a],"bottom");
                }
                if(a !== (all_boxes.length-1)) {
                    if (all_boxes[a].v_position === "start" && all_boxes[+a + 1].v_position === "end") {
                        all_boxes[a].bottom_v = find_bobble_v_position(all_boxes[a], "bottom");
                    }
                }
            }

            function find_bobble_h_position(my_box, direction){
                var all_joining = [], circle_already = null;

                if(direction === "left"){
                    //find all matching x0 to x1
                    all_joining = all_boxes.filter(d => close_to(d.x1, my_box.x0));
                } else if(direction === "right") {
                    //find all matching x0 to x1
                    all_joining = all_boxes.filter(d =>  close_to(d.x0, my_box.x1));
                }

                var my_y0 = parseInt(my_box.y0);
                var joining_height = 0, opposite_position = 0;

                //check if matching y0
                var matching_y0 = all_joining.find(d =>  close_to(d.y0, my_box.y0))
                //easy - find both heights, middle of lowest height (checking lowest height > bobble_radius*2)
                if(matching_y0 !== undefined){
                    joining_height = Math.min(my_box.y1 - my_y0, matching_y0.y1 - matching_y0.y0);
                    opposite_position = matching_y0.y0 + (joining_height/2);
                } else {
                    //y0 not matching so, look for closest lower than y0
                    all_joining = all_joining.sort((a,b) => d3.descending(a.y0,b.y0));
                    matching_y0 = all_joining.find(d => d.y0 < my_y0);
                    if(matching_y0.y1 < my_box.y1){
                        joining_height = matching_y0.y1 - my_y0;
                    } else {
                        joining_height = my_box.y1 - my_y0;
                    }
                    //if this one isn't wide enough, try again
                    if((joining_height/2) < (bobble_radius*2)){
                        if((my_box.y1 - my_box.y0) > (bobble_radius*2)){
                            matching_y0 = all_joining[1];
                            if(matching_y0.y1 < my_box.y1){
                                joining_height = matching_y0.y1 - my_y0;
                            } else {
                                joining_height = my_box.y1 - my_y0;
                            }
                        }
                    }
                    //set opposite position
                    opposite_position = matching_y0.y0 + (my_y0 - matching_y0.y0) + (joining_height/2);
                }
                if((joining_height/2) > (bobble_radius*2)){
                    var my_index = all_boxes.findIndex(d => d.id === matching_y0.id);
                    if(direction === "left"){
                        if(all_boxes[my_index].right_h !== undefined){ //no more than one bobble per side...
                            circle_already = all_boxes[my_index].right_h
                        } else {
                            all_boxes[my_index].right_h =  opposite_position;
                        }
                    } else if(direction === "right") {

                        if(all_boxes[my_index].left_h !== undefined){
                            circle_already = all_boxes[my_index].left_h
                        } else {
                            all_boxes[my_index].left_h =  opposite_position;
                            if(my_box.h_position ===  matching_y0.h_position){
                                var box_index = all_boxes.findIndex(d => d.id === my_box.id);
                                all_boxes[box_index].middle_middle =  true;
                            }
                        }
                    }
                    if(circle_already !== null){
                        return circle_already
                    } else {
                        return my_y0 + (joining_height/2);
                    }
                } else {
                    //hide if not enough space.. (not relevant now but used when placing dots to check logic
                    return -100;
                }
            }


            function find_bobble_v_position(my_box, direction){
                var all_joining = [], circle_already = null;
                //same logic as h_position but reversed.  Could be some code saving here but need to give myself a bit of space first!
                if(direction === "top"){
                    //find all matching x0 to x1
                    all_joining = all_boxes.filter(d =>   close_to(my_box.y0, d.y1));
                } else if(direction === "bottom") {
                    //find all matching x0 to x1
                    all_joining = all_boxes.filter(d =>  close_to(my_box.y1, d.y0));
                }

                var my_x0 = parseInt(my_box.x0);
                var joining_width = 0, opposite_position = 0;

                //check if matching y0
                var matching_x0 = all_joining.find(d => close_to(d.x0, my_box.x0));
                //easy - find both heights, middle of lowest height (checking lowest height > bobble_radius*2)
                if(matching_x0 !== undefined){
                    joining_width = Math.min(my_box.x1 - my_x0, matching_x0.x1 - matching_x0.x0);
                    opposite_position = matching_x0.x0 + (joining_width/2);
                } else {
                    //y0 not matching so, look for closest lower than y0
                    all_joining = all_joining.sort((a,b) => d3.descending(a.x0,b.x0));
                    matching_x0 = all_joining.find(d => d.x0 < my_x0);
                    if(matching_x0.x1 < my_box.x1){
                        joining_width = matching_x0.x1 - my_x0;
                    } else {
                        joining_width = my_box.x1 - my_x0;
                    }
                    if((joining_width/2) < (bobble_radius*2)){
                        if((my_box.x1 - my_box.x0) > (bobble_radius*2)){
                            matching_x0 = all_joining[0];
                            if(matching_x0.x1 < my_box.x1){
                                joining_width = matching_x0.x1 - my_x0;
                            } else {
                                joining_width = my_box.x1 - my_x0;
                            }
                        }
                    }
                    opposite_position = matching_x0.x0 + (my_x0 - matching_x0.x0) + (joining_width/2);

                }
                if((joining_width/2) > (bobble_radius*2)){
                    var my_index = all_boxes.findIndex(d => d.id === matching_x0.id);
                    if(direction === "top"){
                        if(all_boxes[my_index].bottom_v !== undefined){
                            circle_already = all_boxes[my_index].bottom_v
                        } else {
                            all_boxes[my_index].bottom_v =  opposite_position;
                        }
                    } else if(direction === "bottom") {
                        if(all_boxes[my_index].top_v !== undefined){
                            circle_already = all_boxes[my_index].top_v
                        } else {
                            all_boxes[my_index].top_v =  opposite_position;
                        }
                    }
                    if(circle_already !== null ){
                        return  circle_already
                    } else {
                        return my_x0 + (joining_width/2);
                    }
                } else {
                    return -100;
                }
            }
            root.children[c].children = all_boxes;

        }
        //display tree
        display(root);

        function display(root_data) {

            //define group
            var my_group = svg.selectAll(".tree_group")
                .data(root_data.children);

            //exit remove
            my_group.exit().remove();
            //enter (and add group id and transform)
            var enter = my_group.enter().append("g").attr("class","tree_group");

            //append items to group
            enter.append("g").attr("class","parents");

            //merge
            my_group = my_group.merge(enter);

            var child_group = my_group.select(".parents").selectAll(".tree_child_group")
                .data(d => d.children || [d]);

            child_group.exit().remove();

            var child_enter = child_group.enter().append("g").attr("class","tree_child_group");

            //append items to group
            child_enter.append("path").attr("class","child");
            child_enter.append("text").attr("class","label");

            //merge
            child_group = child_group.merge(child_enter);

            child_group.select(".child")
                .attr("id",d => d.id)
                .call(rect);

            child_group.select(".label")
                .attr("x", d => d.x0 + ((d.x1 - d.x0)/2))
                .attr("y", d => d.y0 + 30)
                .text(d => (d.y1 - d.y0) > 30 ? d.data.name : "")


        }

        function rect(rect) {

            var padding = 0;
            rect.attr("d", function(d){
                //declare corners
                var top_left =   "M" + d.x0 + "," + d.y0;
                var top_right =  "L" + (d.x1-padding) + "," +  d.y0;
                var bottom_right =  "L" + (d.x1-padding) + "," + (d.y1-padding);
                var bottom_left  = "L" + d.x0 + "," + (d.y1-padding);

                var my_path =  top_left + add_bobble(d,"top",padding) + top_right + add_bobble(d,"right",padding)
                    + bottom_right + add_bobble(d, "bottom",padding) +  bottom_left + add_bobble(d,"left",padding) + "Z";

                return my_path;

            })
                .attr("fill", d =>colour_scale(d.data.children[0][colour_var]))
                .attr("stroke",d => parent_colour(d.data.children[0].sleeve))
                .attr("stroke-width","3px")
                .on("mousemove",function(d){

                    var tooltip_text = "<strong>Ticker: </strong>" + d.data.children[0].Ticker + "<br>"
                    + "<strong>Market: </strong>" + d.data.children[0].Market + "<br>"
                        + "<strong>Total Return: </strong>" + my_format(d.data.children[0].TotalReturn) + "<br>"
                        + "<strong>Weight: </strong>" + my_format(d.data.children[0].Wgt) + "<br>"
                        + "<strong>Sleeve: </strong>" + d.data.children[0].sleeve + "<br>"
                        + "<strong>Value (Weight x Total Return): </strong>" + my_format(d.data.children[0].value)

                    d3.select(".tooltip")
                        .style("top",d3.event.y)
                        .style("left",d3.event.x)
                        .style("visibility","visible")
                        .html(tooltip_text)
                })
                .on("mouseleave",function(d){
                    d3.select(".tooltip").style("visibility","hidden");
                })
        }

        function add_bobble(d, position,padding){

            var b_path = "", arc = "";
            if(position === "top"){
                if(d.top_v > 0 && d.top_v !== undefined  && d.top_v > d.x0){
                    b_path = "L" + (d.top_v - bobble_radius) + "," + d.y0
                        + get_bobble("top",d.top_v - bobble_radius, d.y0)
                }
            } else if (position === "right"){
                if((d.right_h > 0 && d.right_h !== undefined && d.right_h > d.y0)){
                    b_path = "L" + d.x1 + "," +  (d.right_h - bobble_radius)
                        + get_bobble("right",d.x1, d.right_h - bobble_radius)
                }
            } else if(position === "bottom"){

                if(d.bottom_v > 0 && d.bottom_v !== undefined && d.bottom_v > d.x0){
                    b_path =  "L" + (d.bottom_v + bobble_radius) + "," + d.y1
                        + get_bobble("bottom",d.bottom_v + bobble_radius, d.y1)
                }
            } else if (position === "left"){
                if(d.left_h > 0 && d.left_h !== undefined && d.left_h > d.y0){
                    b_path =  "L " + d.x0 + "," + (d.left_h + bobble_radius)
                    + get_bobble("left",d.x0, d.left_h + bobble_radius);
                }
            }
            return b_path;
        }


        function get_bobble(my_direction, start_x, start_y){
            var bobble_quarter = bobble_radius/2;
            var bobble_width = bobble_radius*2;
            var q1= "",q2= "",q3= "",q4 = "";

            if(my_direction === "top"){
                q1 =  " Q " + (start_x+(bobble_quarter* 2)) + "," + start_y
                    + " " + (start_x + bobble_quarter) + "," + (start_y - bobble_quarter);

                q2 = " Q " + (start_x - (bobble_quarter*(3/5))) + "," + (start_y - (bobble_quarter*3))
                + " " + (start_x + (bobble_quarter*2)) + "," + (start_y - (bobble_quarter*3));

                q3 = " Q " + (start_x + bobble_width + (bobble_quarter*(3/5))) + "," + (start_y - (bobble_quarter*3))
                    + " " + (start_x + (bobble_quarter*3)) + "," + (start_y - bobble_quarter);

                q4 = " Q " + (start_x + (bobble_quarter*2)) + "," + start_y
                + " " + (start_x + bobble_width) + "," + start_y;

            } else if (my_direction === "bottom"){
                q1 =  " Q " + (start_x-(bobble_quarter* 2)) + "," + start_y
                    + " " + (start_x - bobble_quarter) + "," + (start_y + bobble_quarter);

                q2 = " Q " + (start_x + (bobble_quarter*(3/5))) + "," + (start_y + (bobble_quarter*3))
                    + " " + (start_x - (bobble_quarter*2)) + "," + (start_y + (bobble_quarter*3));

                q3 = " Q " + (start_x - bobble_width + (bobble_quarter*(3/5))) + "," + (start_y + (bobble_quarter*3))
                    + " " + (start_x - (bobble_quarter*3)) + "," + (start_y + bobble_quarter);

                q4 = " Q " + (start_x - (bobble_quarter*2)) + "," + start_y
                    + " " + (start_x - bobble_width) + "," + start_y;
            } else if (my_direction === "right"){
                q1 =  " Q " + start_x + "," + (start_y+(bobble_quarter* 2))
                    + " " +  (start_x + bobble_quarter) + "," + (start_y + bobble_quarter);

                q2 = " Q " + (start_x + (bobble_quarter*3)) + "," + (start_y + (bobble_quarter*(3/5)))
                    + " " + (start_x + (bobble_quarter*3)) + "," + (start_y + (bobble_quarter*2));

                q3 = " Q " + (start_x + (bobble_quarter*3)) + "," + (start_y + bobble_width +(bobble_quarter*(3/5)))
                    + " " + (start_x + bobble_quarter) + "," + (start_y + (bobble_quarter*3));

                q4 = " Q " + start_x  + "," + (start_y +  (bobble_quarter*2))
                    + " " + start_x + "," + (start_y + bobble_width);


            } else if (my_direction === "left"){
                q1 =  " Q " + start_x + "," + (start_y-(bobble_quarter* 2))
                    + " " +  (start_x - bobble_quarter) + "," + (start_y - bobble_quarter);

                q2 = " Q " + (start_x - (bobble_quarter*3)) + "," + (start_y + (bobble_quarter*(3/5)))
                    + " " + (start_x - (bobble_quarter*3)) + "," + (start_y - (bobble_quarter*2));

                q3 = " Q " + (start_x - (bobble_quarter*3)) + "," + (start_y - bobble_width -(bobble_quarter*(3/5)))
                    + " " + (start_x - bobble_quarter) + "," + (start_y - (bobble_quarter*3));

                q4 = " Q " + start_x  + "," + (start_y -  (bobble_quarter*2))
                    + " " + start_x + "," + (start_y - bobble_width);
            }

            return q1 + q2 + q3 + q4;
        }

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };



    return my;
}
