

var files = ["data/data.json", "data/tradeoffs.json"];
Promise.all(files.map(url => d3.json(url))).then(ready);

function ready(rawdata){
    console.log("ready");
    console.log(rawdata);
    data = [rawdata[0]];
    tradeoffs = rawdata[1];
    //draw svgs or resizes svg depending on whether resize, reset or initial draw
    var chart_div = document.getElementById(alv.chooser_div);

    var width = +chart_div.clientWidth;
    var height = +chart_div.clientHeight;

    if(d3.select("." + alv.chooser_div + "_svg")._groups[0][0] === null){
        //draw svg to div height and width
        var svg = d3.select("#" + alv.chooser_div)
            .append("svg")
            .attr("class",alv.chooser_div + "_svg")
            .attr("width",width)
            .attr("height",height);

        alv.texture = textures.lines().thicker().stroke("#A0A0A0");
        alv.texture_gold = textures.lines().thicker().stroke("gold");
        svg.call(alv.texture);
        svg.call(alv.texture_gold);
    }

    alv.all_data = data[0].positions.new;
    alv.factor_names = data[0].factor_names;
    alv.factor_labels = data[0].factor_labels;
    alv.tradeoffs = tradeoffs;

    draw_dial_chart(svg,data[0].positions.new,width,height,[])
    var init_changes = ["position", "Tracking Error", 0, 0, 0];
    change_data(init_changes);
}

function redraw_chart(data){
    var new_data = [];
    if(data[0].new !== undefined){
        new_data = data[0].new;
        data = data[0].old;
    }
    var chart_div = document.getElementById(alv.chooser_div);

    var width = +chart_div.clientWidth;
    var height = +chart_div.clientHeight;
    var svg =  d3.select("." + alv.chooser_div + "_svg");
    draw_dial_chart(svg,data,width,height,new_data)

}
function change_data(changes){
    console.log(changes);
    console.log("type: " + changes[0],"factor : " + changes[1],"id: " + changes[2],"old_value: " +  changes[3],"new_value: " + changes[4]);

    old = alv.all_data
    //do your magic here with the data and refresh it by passing json to ready function...
    var my_index = alv.all_data.findIndex(d => d.id === changes[2]);
    console.log("my_index: " + my_index);
    // save the change
    alv.all_data[my_index][changes[0]] = +changes[4];
	
    var pos0_index = alv.all_data.findIndex(d => d.position === 0);	
    console.log("pos0_index: " + pos0_index)
    var order = 0;
    var data = [{"old": alv.all_data, "new": [{"restrict_left": {}, "restrict_right": {}, "value": {}}]}];
	
    var starti = 0;
    var value = alv.all_data[my_index]["value"];
	
    // if this was a value and not a position change, then only cascade changes from this position down
    // DISABLED SINCE THIS WOULD REMOVE RESTRICTIONS IMPOSED BY HIGHER LEVEL GOALS
//    if (changes[0] == "value") {
//	    starti = alv.all_data[my_index]["position"];
//    }

    // remove restrictions if this is the new top goal or if it was the prior top goal (moved down)
    if ((changes[0] == "position") && ((pos0_index == my_index) || (changes[3] == 0))) {
//	    alv.all_data[pos0_index]["restrict_left"] = 0;
//	    alv.all_data[pos0_index]["restrict_right"] = 10;
	    	
	    data[0].new[0]["restrict_left"][pos0_index] = {"value": 0, "order": 0};
	    data[0].new[0]["restrict_right"][pos0_index] = {"value": 10, "order": 0};
    };
    	    
    // reset restrict_left and restrict_right
    for (var i = starti; i < Object.keys(alv.factor_names).length-1; i++) {
        var order_index = alv.all_data.findIndex(d => d.position === i);
//       data[0].new[0]["restrict_left"][order_index] = {"value": 0, "order": 0};
//       data[0].new[0]["restrict_right"][order_index] = {"value": 10, "order": 0};
       alv.all_data[order_index]["restrict_left"] = 0;
       alv.all_data[order_index]["restrict_right"] = 10;
    }
    
    // cascade changes from high to low priority goals
    for (var i = starti; i < Object.keys(alv.factor_names).length-1; i++) {
        var order_index = alv.all_data.findIndex(d => d.position === i);
        var factor = alv.factor_names[order_index.toString()];
        var value = alv.all_data[order_index]["value"];
        console.log("Applying restrictions for " + factor + " position: " + order_index + ", value: " + value);
        
        for (var j = i+1; j < Object.keys(alv.factor_names).length; j++) {
            var suborder_index = alv.all_data.findIndex(d => d.position === j);
            var subfactor = alv.factor_names[suborder_index.toString()];
            var subposition = alv.all_data[suborder_index]["position"];
            var subvalue = alv.all_data[suborder_index]["value"];
            var subleft = alv.all_data[suborder_index]["restrict_left"];
            var subright = alv.all_data[suborder_index]["restrict_right"];
            console.log("... " + subfactor + " index: " + suborder_index + " position: " + subposition + " value: " + subvalue + " left: " + subleft);
            
            // set left and adjust value if necessary; update alv.all_data and the data[0].new structure
            if ((alv.tradeoffs[factor] != undefined) && (alv.tradeoffs[factor]["restrict_left"][subfactor] != undefined)) {
	        var newleft = alv.tradeoffs[factor]["restrict_left"][subfactor][value];
                console.log("   restrict_left ... value:" + value + " new:" + newleft + " old:" + subleft);
	        if (newleft > subleft) {
	           console.log("   ... * moving restrict_left from " + subleft + " to " + newleft);
	           data[0].new[0]["restrict_left"][suborder_index] = {"value": newleft, "order": order++};
	           alv.all_data[suborder_index]["restrict_left"] = newleft;
	           if (subvalue < newleft) {
	              console.log("      ... * moving value from " + subvalue + " to " + newleft); 
	              data[0].new[0]["value"][suborder_index] = {"value": newleft, "order": order++};     
	              // need to set this so that lower goals will have restrictions set correctly
	              alv.all_data[suborder_index]["value"] = newleft;	                            
	           }
	        } 
            }
            // set right and adjust value if necessary
            if ((alv.tradeoffs[factor] != undefined) && (alv.tradeoffs[factor]["restrict_right"][subfactor] != undefined)) {
	        var newright = alv.tradeoffs[factor]["restrict_right"][subfactor][value];
                console.log("   restrict_right ... value:" + value + " new:" + newright + " old:" + subright);
	        if (newright < subright) {
	           console.log("   ... * moving restrict_right from " + subright + " to " + newright);
	           console.log(data[0].new[0]);
	           data[0].new[0]["restrict_right"][suborder_index] = {"value": newright, "order": order++};
	           alv.all_data[suborder_index]["restrict_right"] = newright;
	           if (subvalue > newright) {
	              console.log("      ... * moving value from " + subvalue + " to " + newright); 
	              data[0].new[0]["value"][suborder_index] = {"value": newright, "order": order++};                   
	              // need to set this so that lower goals will have restrictions set correctly
	              alv.all_data[suborder_index]["value"] = newright;
	           }
	        } 
            }        
        }                 
    
//        show_data();
        console.log(alv.all_data);
        console.log("new:");
        console.log(data[0].new[0]);
        redraw_chart(data); 

    }

}

// add functionality to save the data to a json file for debugging
function save_data() {
        var saveJson = JSON.stringify(alv.all_data, null, 4);
        dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(saveJson);
//        var linkElement = document.createElement('a');
//        linkElement.setAttribute('href', dataUri);
//        linkElement.setAttribute('download', 'redraw.json');
//        linkElement.click();
}

function show_data() {
    for (var i = 0; i < Object.keys(alv.factor_names).length; i++) {
        var order_index = alv.all_data.findIndex(d => d.position === i);
        console.log("Position: " + i, "Index: " + order_index, " " + alv.factor_names[order_index.toString()], "Value: " + alv.all_data[order_index]["value"],
        "Restrict_left: " + alv.all_data[order_index]["restrict_left"], "Restrict_right: " + alv.all_data[order_index]["restrict_right"]);
    }
}

function draw_dial_chart(svg,chart_data,width,height,new_data){

    var my_chart = dial_chart()
        .width(width)
        .height(height)
        .my_class(alv.chooser_div)
        .my_data(chart_data)
        .new_data(new_data);


    my_chart(svg);

}

