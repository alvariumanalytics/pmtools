
var files = ["data/tree_data.csv"];
Promise.all(files.map(url => d3.csv(url))).then(ready);

function ready(data){
    //draw svgs or resizes svg depending on whether resize, reset or initial draw
    var chart_div = document.getElementById(alv.tree_div);

    var width = +chart_div.clientWidth;
    var height = +chart_div.clientHeight;
    var margins = 10;
    if(d3.select("." + alv.chooser_div + "_svg")._groups[0][0] === null){
        //draw svg to div height and width
        var svg = d3.select("#" + alv.tree_div)
            .append("svg")
            .attr("class",alv.tree_div + "_svg")
            .attr("width",width)
            .attr("height",height);

        svg = svg.append("g")
            .attr("class",alv.tree_div + "_svg")
            .attr("transform","translate(" + margins + "," + margins + ")");

        width -= (margins*2);
        height -= (margins*2);

    }


    d3.selectAll(".fill_type")
        .on("change",function(d){
            draw_tree_chart(svg,data[0],width,height)
        })

    draw_tree_chart(svg,data[0],width,height)
}


function draw_tree_chart(svg,chart_data,width,height){

    var my_chart = tree_map()
        .width(width)
        .height(height)
        .my_class(alv.tree_div)
        .my_data(chart_data);

    my_chart(svg);

}

