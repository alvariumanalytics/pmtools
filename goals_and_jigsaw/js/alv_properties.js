var alv = {
    chooser_div: "chooser_div",
    tree_div: "tree_div",
    factor_names:{},
    factor_labels:{},
    all_data: [],
    texture:"",
    texture_gold:"",
    tree_fill_colours:{
        "market": [ "#1f78b4", "#ff7f00", "#6a3d9a","#b15928","gold"] ,
        "weight":["#a50026", "#d73027", "#f46d43", "#fdae61", "#fee08b", "#ffffbf", "#d9ef8b", "#a6d96a", "#66bd63", "#1a9850", "#006837"],

        "return":[
            "#006837", "#1a9850", "#66bd63", "#a6d96a", "#d9ef8b", "#ffffbf", "#fee08b", "#fdae61", "#f46d43", "#d73027", "#a50026"],
        "sleeve":["#ffffff","#969696","#636363"]}

};
