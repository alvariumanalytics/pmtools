
var promises = [];
var view_mode = 'summary'; //edit, summary
var my_set = "Default";

promises.push(d3.json("data/min_max_settings.json"));


Promise.all(promises).then(ready);

function ready(all_data){
    onboarding.min_max_labels = all_data[0].sets[my_set].labels;
    onboarding.min_max_b_weight = all_data[0].sets[my_set].b_weight;
    onboarding.min_max_value = all_data[0].sets[my_set].value;
    onboarding.min_max_data = all_data[0];
    onboarding.min_max_sets = Object.keys(all_data[0].sets);
    var svg = draw_svg("standalone_mm_chart_div");
    if(view_mode === 'edit'){
        draw_min_max_defaults(svg);
    } else {
        var slider_data = {};
        onboarding.min_max_labels.forEach(d => slider_data[d] = [10,25]);
        draw_min_max_sliders(svg,slider_data)
    }
}



function draw_min_max_defaults(svg){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top":80,"bottom":15};

    var my_chart = min_max_defaults()
        .width(width)
        .height(height-15)
        .margins(margins)
        .my_slider_svg("standalone_mm_chart_div_svg")
        .my_class("min_max_chart_standalone");

    my_chart(svg);
}


function draw_min_max_sliders(svg,my_data){

    debugger;

    var width = +svg.attr("width");
    var margins = {"left":5 ,"right":40,"top":260,"bottom":15};

    if(view_mode === "summary"){
        margins.top = 20;
    }

    var my_chart = min_max_sliders()
        .width(width)
        .margins(margins)
        .my_data(my_data)
        .my_title("")
        .my_class("min_max_sliders_standalone");

    my_chart(svg);
}


function draw_svg(div_id){


    if(d3.select("." + div_id + "_svg")._groups[0][0] === null){
        var svg = d3.select("#" + div_id)
            .append("svg")
            .attr("class",div_id + "_svg")
            .attr("width",740)
            .attr("height",750);


    } else {
        var svg = d3.select("." + div_id + "_svg");
    }
    return svg;
}
