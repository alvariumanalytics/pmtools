
function add_goals_restrictions(my_index,reset_pos0,pos0_id){
    var data_changed = false;

    var order = 0;
    var index_ids = Object.keys(onboarding.goals_data.factor_names);
    var all_data = JSON.parse(JSON.stringify(onboarding.goals_data.positions.current));
    console.log("add_goals_restrictions current: ")
    console.log(all_data);
    var new_data = {"restrict_left": {}, "restrict_right": {}, "value": {}};
    if(reset_pos0 === true){
        new_data["restrict_left"][pos0_id] = {"value": 0, "order": 0};
        new_data["restrict_right"][pos0_id] = {"value": 10, "order": 0};
    }
    //do your magic here with the data and refresh it by passing json to ready function...
    console.log("my_index: " + my_index);
    console.log(all_data);

    var starti = 0;

    // reset restrict_left and restrict_right
    for (var i = starti; i < Object.keys(onboarding.goals_data.factor_names).length-1; i++) {
        var order_index = all_data.findIndex(d => d.position === i);
        all_data[order_index]["restrict_left"] = 0;
        all_data[order_index]["restrict_right"] = 10;
        id = all_data[order_index]["id"];
        new_data["restrict_left"][id] = {"value": 0, "order": 0};
        new_data["restrict_right"][id] = {"value": 10, "order": 0};
    }

    console.log(all_data);
    console.log(onboarding.goals_data.factor_names);
    // cascade changes from high to low priority goals
    for (var i = starti; i < Object.keys(onboarding.goals_data.factor_names).length-1; i++) {
        var order_index = all_data.findIndex(d => d.position === i);
        var factor = onboarding.goals_data.factor_names[all_data[order_index]["id"].toString()];
        var value = all_data[order_index]["value"];

        console.log("Applying restrictions for " + factor + " id: " + all_data[order_index]["id"] + " position: " + order_index + ", value: " + value);
        for (var j = i + 1; j < Object.keys(onboarding.goals_data.factor_names).length; j++) {
            var suborder_index = all_data.findIndex(d => d.position === j);
            var subfactor = onboarding.goals_data.factor_names[all_data[suborder_index]["id"].toString()];
            var subposition = all_data[suborder_index]["position"];
            var subvalue = all_data[suborder_index]["value"];
            var subleft = all_data[suborder_index]["restrict_left"];
            var subright = all_data[suborder_index]["restrict_right"];
            var subid = all_data[suborder_index]["id"];
            console.log(all_data);
            console.log("... " + subfactor + " id: " + all_data[suborder_index]["id"] + " index: " + suborder_index + " position: " + subposition + " value: " + subvalue + " left: " + subleft + " right: " + subright);

            // set left and adjust value if necessary; update alv.all_data and the data[0].new structure
            if ((onboarding.goals_data.tradeoffs[factor] != undefined) && (onboarding.goals_data.tradeoffs[factor]["restrict_left"][subfactor] != undefined)) {
                var newleft = onboarding.goals_data.tradeoffs[factor]["restrict_left"][subfactor][value];
                console.log("   restrict_left ... value:" + value + " new:" + newleft + " old:" + subleft);
                if (newleft > subleft) {
                    console.log("   ... * moving restrict_left from " + subleft + " to " + newleft);
                    new_data["restrict_left"][subid] = {"value": newleft, "order": order++};
                    data_changed = true;
                    all_data[suborder_index]["restrict_left"] = newleft;
                    if (subvalue < newleft) {
                        console.log("      ... * moving value from " + subvalue + " to " + newleft);
                        new_data["value"][subid] = {"value": newleft, "order": order++};
                        // need to set this so that lower goals will have restrictions set correctly
                        all_data[suborder_index]["value"] = newleft;
                    }
                }
            }
            // set right and adjust value if necessary
            if ((onboarding.goals_data.tradeoffs[factor] != undefined) && (onboarding.goals_data.tradeoffs[factor]["restrict_right"][subfactor] != undefined)) {
                var newright = onboarding.goals_data.tradeoffs[factor]["restrict_right"][subfactor][value];
                console.log("   restrict_right ... value:" + value + " new:" + newright + " old:" + subright);
                if (newright < subright) {
                    console.log("   ... * moving restrict_right from " + subright + " to " + newright);
                    new_data["restrict_right"][subid] = {"value": newright, "order": order++};
                    data_changed = true;
                    all_data[suborder_index]["restrict_right"] = newright;
                    if (subvalue > newright) {
                        console.log("      ... * moving value from " + subvalue + " to " + newright);
                        new_data["value"][subid] = {"value": newright, "order": order++};
                        // need to set this so that lower goals will have restrictions set correctly
                        all_data[suborder_index]["value"] = newright;
                    }
                }
            }
        }
    }

    if(data_changed === false){
        return null;
    } else {
        return new_data;
    }

}


function change_goals_data(changes){

    console.log(changes);
    console.log("change_goals_data type: " + changes[0],"factor : " + changes[1],"id: " + changes[2],"old_value: " +  changes[3],"new_value: " + changes[4]);
    console.log("change_goals_data current: " + JSON.stringify(onboarding.goals_data.positions.current));

    // save the change
    var my_index = onboarding.goals_data.positions.current.findIndex(d => d.id === changes[2]);
    onboarding.goals_data.positions.current[my_index][changes[0]] = +changes[4];
    var pos0_index = onboarding.goals_data.positions.current.findIndex(d => d.position === 0);
    var new_data = {"restrict_left": {}, "restrict_right": {}, "value": {}};

    var reset_pos0 = false;
    if ((changes[0] == "position") && ((pos0_index == my_index) || (changes[3] == 0))) {
        reset_pos0 = true;
    };

    //cheat so you can see the allocations changing each time
//    onboarding.allocation_index += 1;
//    if(onboarding.allocation_index === 4){
//        onboarding.allocation_index = 0;
//    }
//    onboarding.goals_data.allocations = onboarding.goals_data.allocations_alternatives[onboarding.allocation_index]
    //end of cheat

    var changes = add_goals_restrictions(my_index,reset_pos0,pos0_index);
    onboarding.goals_data.allocations = {"strategic":strategic_potential(),"fund":fund_potential(),"tactical":tactical_potential()};
    console.log("change_goals_data: allocations=");
    console.log(onboarding.goals_data.allocations);
    console.log("change_goals_data: changes");
    console.log(changes);
    draw_goals_and_trade_offs(d3.select("#" + onboarding.goals_data.svg_id),"goals_trade_offs",changes)

}


function tactical_potential() {
    var all_data = JSON.parse(JSON.stringify(onboarding.goals_data.positions.current));
    console.log("fund_potential:");
    console.log(all_data);
    var te_index = all_data.findIndex(d => d.id === 'TE');
    var ts_index = all_data.findIndex(d => d.id === 'TS');
    var tr_index = all_data.findIndex(d => d.id === 'TR');
    var mmf_index = all_data.findIndex(d => d.id === 'MMF');
    var cf_index = all_data.findIndex(d => d.id === 'CF');
    te = all_data[te_index];
    tr = all_data[tr_index];
    ts = all_data[ts_index];
    mmf = all_data[mmf_index];
    cf = all_data[cf_index];
    // scale tactical directly to where we are in the range of tracking error values
    tactical = (all_data[tr_index].value * 10)/ 100 * onboarding.goal_allocation_max_vals['tactical'];

    // TODO add adjustments based on TE, CF, MMF, and TS
//    if (mmf.value <= 5) {
//    	 discount tactical for NQ through TS tax sensitivity in the range 75% to 60%
//    	mmffactor = 0.2 + (0.12 * mmf.value);
//    	tactical = tactical * mmffactor;
//    }
    return tactical;
}


function fund_potential() {
    var all_data = JSON.parse(JSON.stringify(onboarding.goals_data.positions.current));
    console.log("fund_potential:");
    console.log(all_data);
    var te_index = all_data.findIndex(d => d.id === 'TE');
    var mmf_index = all_data.findIndex(d => d.id === 'MMF');
    te = all_data[te_index];
    mmf = all_data[mmf_index];
    // scale tactical directly to where we are in the range of tracking error values
    fund = (all_data[te_index].value * 10)/ 100 * onboarding.goal_allocation_max_vals['fund'];

    if (mmf.value <= 5) {
        // discount tactical for NQ through TS tax sensitivity in the range 75% to 60%
        mmffactor = 0.2 + (0.12 * mmf.value);
        fund = fund * mmffactor;
    }
    return fund;
}


function strategic_potential() {
    var all_data = JSON.parse(JSON.stringify(onboarding.goals_data.positions.current));
    console.log("strategic_potential:");
    console.log(all_data);
    var ts_index = all_data.findIndex(d => d.id === 'TS');
    var te_index = all_data.findIndex(d => d.id === 'TE');
    ts = all_data[ts_index];
    te = all_data[te_index];
    // scale strategic directly to where we are in the range of tracking error values
    strategic = (all_data[te_index].value * 10)/ 100 * onboarding.goal_allocation_max_vals['strategic'];

    if (ts.value >= 5) {
        // discount strategic for NQ through TS tax sensitivity in the range 75% to 60%
        tsfactor = 0.6 + 0.03 * (10 - ts.value);
        strategic = strategic * tsfactor;
    }
    return strategic;
}

