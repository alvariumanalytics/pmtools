function progress_bar() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data = [],
        my_class="";

    function my(svg) {

        //extra 3 questions for goals and trade offs
        var total_questions = d3.sum(my_data, d => d.questions.length) + 3;
        onboarding.progress_x_scale = d3.scaleLinear().domain([0,total_questions]).range([0,width-margins.left-margins.right-height]);

        my_data.forEach(function(s,i){
            s.stage_x = 0;
            if(+i > 0){
                s.stage_x = my_data[+i-1].stage_x + my_data[+i-1].questions.length;
                //add three for goals so space for text
                if(s.stage_id > 3){
                    s.stage_x += 3;
                }
            }
        });
        if(d3.select(".progress_background" + my_class)._groups[0][0] === null) {
            svg.append("rect").attr("class","progress_background progress_background" + my_class);
            svg.append("rect").attr("class","progress_actual" + my_class);
        }
        d3.select(".progress_background" + my_class)
            .attr("x",margins.left)
            .attr("width",width - margins.left - margins.right)
            .attr("height",height)
            .attr("fill",onboarding.colors.lightgold);


        var stage_group = svg.selectAll('.stage_group' + my_class)
            .data(my_data)
            .join(function(group){
                var enter = group.append("g").attr("class","stage_group" + my_class);
                enter.append("g").attr("class","question_group");
                enter.append("circle").attr("class","stage_under_circle");
                enter.append("circle").attr("class","stage_stroke stage_circle");
                enter.append("rect").attr("class","stage_stroke stage_wings");
                enter.append("rect").attr("class","stage_fill stage_body");
                enter.append("text").attr("class","stage_text");
                return enter;
            });

        stage_group.select(".stage_under_circle")
            .attr("id",d => "stageundercircle_" + d.stage_id)
            .attr("fill",onboarding.colors.gold)
            .attr("r",height/2)
            .attr("cx",d => onboarding.progress_x_scale(d.stage_x))
            .attr("cy",height/2)
            .attr("transform","translate(" + (margins.left + (height/2)) + ",0)");


        stage_group.select(".stage_circle")
            .attr("id",d => "stagestroke_" + d.stage_id)
            .attr("r",(height/2)-4)
            .attr("cx",d => onboarding.progress_x_scale(d.stage_x))
            .attr("cy",height/2)
            .attr("transform","translate(" + (margins.left + (height/2)) + ",0)");

        stage_group.select(".stage_text")
            .style("text-anchor","middle")
            .attr("x",d => onboarding.progress_x_scale(d.stage_x) + onboarding.progress_x_scale((d.questions.length/2) + 0.5)
            + (d.stage_id === 3 ? onboarding.progress_x_scale(1.5) : 0))
            .attr("y",(height/2) + 6)
            .text(d => d.title);

        stage_group.select(".stage_wings")
            .attr("id",d => "stagestroke_" + d.stage_id)
            .attr("x",d => 7 + onboarding.progress_x_scale(d.stage_x))
            .attr("y",(height/2)-4)
            .attr("width",height-14)
            .attr("height",8)
            .attr("transform","translate(" + margins.left + ",0)");

        stage_group.select(".stage_body")
            .attr("id",d => "stagefill_" + d.stage_id)
            .attr("x",d => (height/2) - 5 + onboarding.progress_x_scale(d.stage_x))
            .attr("y",10)
            .attr("width",10)
            .attr("height",height-20)
            .attr("transform","translate(" + margins.left + ",0)");

        d3.selectAll(".stage_fill")
            .style("fill",onboarding.colors.midgreen);

        d3.selectAll(".stage_stroke")
            .style("stroke",onboarding.colors.midgreen);

        for(i = 0; i <= onboarding.progress_stage ; i++){

            d3.selectAll("#stagefill_" + i)
                .style("fill",onboarding.colors.darkgreen);

            d3.selectAll("#stagestroke_" + i)
                .style("stroke",onboarding.colors.darkgreen)
        }
        stage_group.select(".question_group")
            .attr("transform",d => "translate(" + (margins.left + onboarding.progress_x_scale(d.stage_x)) + ",0)");


        var question_group = stage_group.select(".question_group").selectAll('.question_group' + my_class)
            .data(d => d.questions)
            .join(function(group){
                var enter = group.append("g").attr("class","question_group" + my_class);
                enter.append("line").attr("class","question_line");
                return enter;
            });

        question_group.select(".question_line")
            .attr("x1",d => onboarding.progress_x_scale(d.question_id+1))
            .attr("x2",d => onboarding.progress_x_scale(d.question_id+1))
            .attr("y1",5)
            .attr("y2",height-5)
            .attr("stroke","white")
            .attr("stroke-width",d => d.show_line === false ? 0 : 1);

    }


    my.reset_progress = function(){

        d3.select(".progress_actual" + my_class)
            .attr("x",margins.left + (height/2))
            .attr("height",height)
            .attr("fill",onboarding.colors.gold)
            .transition()
            .duration(1000)
            .attr("width",onboarding.progress_x_scale(onboarding.progress_position) - (height/2));
    }
    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}


function risk_categories() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        base_data=[],
        my_class="",
        slider_margin_left=250,
        circle_radius=12,
        tree_data = {};

    function my(svg) {

        var my_data = onboarding.risk_categories;
        var my_colours = onboarding.risk_colours;

        my_data.forEach(d => tree_data[d] = 0);

        if(d3.select(".risk_cat_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","risk_cat_title" + my_class);
            svg.append("text").attr("class","risk_label label_left" + my_class);
            svg.append("text").attr("class","risk_label label_right" + my_class);
        }

        d3.select(".risk_cat_title" + my_class)
            .attr("x",margins.left)
            .attr("y",margins.top + circle_radius)
            .attr("fill","#404040")
            .text("Indicate the relative importance of each category");

        d3.select(".label_left" + my_class)
            .attr("x",margins.left + slider_margin_left)
            .attr("y",margins.top + 35)
            .text("unimportant");

        d3.select(".label_right" + my_class)
            .attr("x",width - margins.right)
            .attr("y",margins.top + 35)
            .text("critical");

        margins.top += 45;
        var y_scale = d3.scaleBand().domain(my_data).range([0,height - margins.top - margins.bottom]);
        var x_scale = d3.scalePoint().domain(d3.range(0,11,1)).range([0,width - slider_margin_left - margins.left - margins.right]);

        var category_group = svg.selectAll('.category_group' + my_class)
            .data(my_data)
            .join(function(group){
                var enter = group.append("g").attr("class","category_group" + my_class);
                enter.append("rect").attr("class","category_bar");
                enter.append("circle").attr("class","category_circle");
                enter.append("text").attr("class","category_circle_label");
                enter.append("text").attr("class","category_label");
                return enter;
            });

        category_group.select(".category_label")
            .attr("fill", d => my_colours[d])
            .attr("y", d => y_scale(d) + (y_scale.bandwidth()/2))
            .text(d => d)
            .attr("transform","translate(" + margins.left + "," + margins.top + ")");

        category_group.select(".category_bar")
            .attr("x",slider_margin_left)
            .attr("y", d => y_scale(d) + (y_scale.bandwidth()/2) - circle_radius)
            .attr("width",width - margins.left - margins.right - slider_margin_left)
            .attr("height",circle_radius)
            .attr("fill", d => my_colours[d])
            .attr("transform","translate(" + margins.left + "," + margins.top + ")");

        category_group.select(".category_circle")
            .attr("id",(d,i) => "circleitem_" + i)
            .attr("cx",slider_margin_left)
            .attr("cy", d => y_scale(d) + (y_scale.bandwidth()/2)-(circle_radius/2))
            .attr("r",circle_radius)
            .attr("fill", d => my_colours[d])
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")
            .call(d3.drag()
                .on("start", circle_drag_start)
                .on("drag", circle_drag)
                .on("end", circle_drag_end));

        category_group.select(".category_circle_label")
            .attr("id",(d,i) => "circleitem_" + i)
            .attr("pointer-events","none")
            .attr("x",slider_margin_left)
            .attr("y", d => y_scale(d) + (y_scale.bandwidth()/2))
            .attr("fill", "white")
            .text("0")
            .attr("transform","translate(" + margins.left + "," + margins.top + ")");


        function circle_drag_start() {
            d3.selectAll("#" + this.id).transition().duration(50).style("stroke-width",2);
        }


        function circle_drag(){
            if(d3.event.x > (slider_margin_left + circle_radius)  && d3.event.x < (width - margins.right)){
                d3.selectAll("#" + this.id).attr("transform","translate(" + ((d3.event.x-(slider_margin_left + circle_radius)) + margins.left) + "," + margins.top + ")");
                d3.select("text#" + this.id).text("?");
            }
        }

        function circle_drag_end(d){
            if(onboarding.progress_position < 1){
                onboarding.progress_position = 1;
                onboarding.progress_chart.reset_progress();
            }
           var my_position = d3.event.x - slider_margin_left - circle_radius;
           var my_x = 0;
           for(i = 0 ; i < x_scale.domain().length ; i ++){
               var x = x_scale.domain()[i];
               if(i === (x_scale.domain().length - 1)){
                   my_x = x;
               } else {
                   if(x_scale(x+1) > my_position){
                       my_x = x;
                       break;
                   }
               }
           }
            d3.selectAll("#" + this.id)
                .transition()
                .duration(200)
                .attr("transform","translate(" + (x_scale(my_x) + margins.left) + "," + margins.top + ")");
            d3.select("text#" + this.id).text(my_x);
            tree_data[d] = my_x;
            draw_tree_map(tree_data,height);
        }

    }


    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.base_data = function(value) {
        if (!arguments.length) return base_data;
        base_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}


function tax_status() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        base_data=[],
        my_class="",
        current_split=[0.5,0.5];

    function my(svg) {

        var x_scale = d3.scaleLinear().domain([0,1]).range([0,(width-margins.left-margins.right)/2]);
        var extra_space = 30;
        var percent_format = d3.format(".0%");
        var position_y = svg.node().getBoundingClientRect().top;
        position_y -= document.getElementById("questions_div").getBoundingClientRect().top;
        position_y += margins.top - 10;

        if(d3.select(".tax_stat_bar_left" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","tax_title" + my_class);
            svg.append("rect").attr("class","tax_bar tax_stat_bar_left" + my_class);
            svg.append("rect").attr("class","tax_bar tax_stat_bar_right" + my_class);
            svg.append("rect").attr("class","tax_bar tax_stat_handle" + my_class);
            svg.append("text").attr("class","label_left" + my_class);
            svg.append("text").attr("class","label_right" + my_class);
            svg.append("text").attr("class","label_top_left" + my_class);
            svg.append("text").attr("class","label_top_right" + my_class);
            svg.append("rect").attr("class","tax_bar nq_question nq_background" + my_class);
            svg.append("text").attr("class","nq_question nq_yes" + my_class);
            svg.append("text").attr("class","nq_question nq_no" + my_class);
            svg.append("text").attr("class","nq_question nq_question" + my_class);
            svg.append("circle").attr("class","nq_question nq_circle" + my_class);
        }

        d3.select(".tax_title" + my_class)
            .attr("x",margins.left)
            .attr("y",margins.top + 12)
            .attr("fill","#404040")
            .text("Indicate the relative importance of qualified vs. non-qualified accounts");

        d3.select(".nq_question" + my_class)
            .attr("x", margins.left + (width/2) + 10)
            .attr("y",margins.top + 12 + extra_space + 15)
            .attr("fill","#404040")
            .text("Are any NQs Tax Sensitive?");

        d3.select(".nq_yes" + my_class)
            .attr("x",margins.left + width - margins.right - 70)
            .attr("y",margins.top + 15 + extra_space + 10)
            .attr("text-anchor","end")
            .text("YES")
            .on("mouseover",function(){d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(){d3.select(this).attr("cursor","default")})
            .on("click",function(){
                check_progress();
                var current_x = width - margins.right - 15;
                d3.select(".nq_circle" + my_class).transition().duration(200).attr("cx",current_x-20);
            });


        d3.select(".nq_no" + my_class)
            .attr("x",width - margins.right)
            .attr("y",margins.top + 15 + extra_space + 10)
            .text("NO")
            .on("mouseover",function(){d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(){d3.select(this).attr("cursor","default")})
            .on("click",function(){
                check_progress();
                var current_x = width - margins.right - 15;
                d3.select(".nq_circle" + my_class).transition().duration(200).attr("cx",current_x);
            });

        d3.select(".nq_circle" + my_class)
            .attr("r",8)
            .attr("cx",width - margins.right - 15)
            .attr("cy",margins.top + 10 + extra_space + 10)
            .attr("fill","#808080")
            .on("mouseover",function(){d3.select(this).attr("cursor","grab")})
            .on("mouseout",function(){d3.select(this).attr("cursor","default")})
            .call(d3.drag()
                .on("drag", function(){
                    check_progress();
                    var current_x = width - margins.right - 15;
                    if(d3.event.x < current_x){
                        d3.select(this).transition().duration(200).attr("cx",current_x - 20);
                    } else {
                        d3.select(this).transition().duration(200).attr("cx",current_x);
                    }
                })
                .on("end",function(){
                    var current_x = width - margins.right - 15;
                    if(d3.event.x < current_x){
                        d3.selectAll(".nq_question_yes").attr("visibility","visible");
                    } else {
                        d3.selectAll(".nq_question_yes").attr("visibility","hidden");
                    }
                }));


        d3.select(".nq_background" + my_class)
            .attr("x",margins.left + width - margins.right - 65)
            .attr("y",margins.top + extra_space + 10)
            .attr("width", 40)
            .attr("height",20)
            .attr("fill","#D0D0D0")
            .on("mouseover",function(){d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(){d3.select(this).attr("cursor","default")})
            .on("click",function(){
                check_progress();
                var current_x = width - margins.right - 15;
                if(d3.event.x < (current_x-15)){
                    d3.select(".nq_circle" + my_class).transition().duration(200).attr("cx",current_x - 20);
                } else {
                    d3.select(".nq_circle" + my_class).transition().duration(200).attr("cx",current_x);
                }
            });

        d3.select(".tax_stat_handle" + my_class)
            .attr("x",margins.left + x_scale(current_split[0])-5)
            .attr("y",margins.top + 5 + extra_space)
            .attr("width",10)
            .attr("height",35)
            .attr("fill", onboarding.colors.darkgreen)
            .call(d3.drag()
                .on("drag",  handle_drag)
                .on("end", handle_drag_end));



        draw_slider_elements();

        function draw_slider_elements(){

            d3.select(".tax_stat_bar_left" + my_class)
                .attr("x",margins.left)
                .attr("y",margins.top + 10 + extra_space)
                .attr("width",x_scale(current_split[0]))
                .attr("height",25)
                .attr("fill",onboarding.colors.midgreen);

            d3.select(".tax_stat_bar_right" + my_class)
                .attr("x",margins.left + x_scale(current_split[0]))
                .attr("y",margins.top + 10 + extra_space)
                .attr("width",x_scale(current_split[1]))
                .attr("height",25)
                .attr("fill",onboarding.colors.greengold);

            d3.select(".label_top_left" + my_class)
                .attr("font-weight","bold")
                .attr("x",margins.left +5)
                .attr("y",margins.top + 5 + extra_space)
                .text("Q");

            d3.select(".label_top_right" + my_class)
                .attr("font-weight","bold")
                .attr("text-anchor","end")
                .attr("x",margins.left + x_scale(1) - 5)
                .attr("y",margins.top + 5 + extra_space)
                .text("NQ");

            d3.select(".label_left" + my_class)
                .attr("visibility",x_scale(current_split[0]) < 35 ? "hidden" : "visible")
                .attr("x",margins.left +5)
                .attr("y",margins.top + 27 + extra_space)
                .attr("fill", "white")
                .text(percent_format(current_split[0]));

            d3.select(".label_right" + my_class)
                .attr("visibility",x_scale(current_split[1]) < 35 ? "hidden" : "visible")
                .attr("x",margins.left + x_scale(1) - 5)
                .attr("y",margins.top + 27 + extra_space)
                .attr("text-anchor","end")
                .attr("fill", "white")
                .text(percent_format(current_split[1]));

        }

        function handle_drag(){
            if(d3.event.x > margins.left  && d3.event.x < (margins.left + x_scale(1))){
                var my_x = d3.event.x - margins.left;
                d3.select(this).attr("x",d3.event.x-5);
                current_split[0] = x_scale.invert(my_x);
                current_split[1] = 1 - current_split[0];
                draw_slider_elements();
                draw_tax_bar(current_split,height,position_y);
            }
        }

        function check_progress(){
            if(onboarding.progress_position < 2){
                onboarding.progress_position = 2;
                onboarding.progress_chart.reset_progress();
            }
            draw_tax_bar(current_split,height,position_y);
        }
        function handle_drag_end(){
            check_progress();
            if(parseInt(current_split[1]*100) === 0){
                d3.selectAll(".nq_question").attr("visibility","hidden");
            } else {
                d3.selectAll(".nq_question").attr("visibility","visible");
            }
        }
    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.base_data = function(value) {
        if (!arguments.length) return base_data;
        base_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}


function tree_map() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data={},
        tree_data=[],
        my_class="";
    function my(svg) {


        var my_colours = onboarding.risk_colours;
        var x_scale = d3.scaleLinear().domain([0, width]).range([0, width-margins.left-margins.right]);
        var y_scale = d3.scaleLinear().domain([0, height]).range([0, height-margins.top-margins.bottom]);
        var transform_str = "translate(" + margins.left + "," + margins.top + ")";
        var percent_format = d3.format(".0%");
        onboarding.risk_categories.forEach((d,i)=> tree_data.push({
            "order":i,
            "name": d,
            "value": my_data[d]
        }));
        var total_vals = d3.sum(tree_data, d => d.value);

        if(d3.select(".risk_tree_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","risk_tree_title" + my_class);
        }

        d3.select(".risk_tree_title" + my_class)
            .attr("font-weight","bold")
            .attr("x",margins.left + 5)
            .attr("y",margins.top - 15)
            .text(total_vals > 0 ? "Risk Category Profile" : "");

        const treemap = d3.treemap()
            .size([width, height])
            .padding(3)
            .round(true);

        //display
        show_treemap();

        function show_treemap() {

            var root = d3.hierarchy({"name":"categories","children":tree_data})
                .sum(d => d.value)
                .sort((a, b) => b.order - a.order);

            var my_group = svg.selectAll(".tree_group"+ my_class)
                .data(treemap(root).leaves());
            //exit remove
            my_group.exit().remove();
            //enter
            var enter = my_group.enter().append("g")
                .attr("class","tree_group"+ my_class);

            //append
            enter.append("rect").attr("class","child");
            enter.append("rect").attr("class","parent");
            enter.append("foreignObject").attr("class","foreignobj")
                .append("xhtml:div").attr("class","tree_textdiv");
            //merge
            my_group = my_group.merge(enter);

            my_group.select(".child")
                .attr("id",(d,i) => "treeid_" + i)
                .call(rect);

            // add title to parents
            my_group.select(".parent")
                .attr("id",(d,i) => "treeid_" + i)
                .call(rect)
                .append("title")
                .text(d => d.data.key);

            /* Adding a foreign object instead of a text object, allows for text wrapping */
            my_group.select(".foreignobj")
                .attr("id",(d,i) => "treeid_" + i)
                .call(rect)


            my_group.select(".tree_textdiv")
                .html(function (d) {return'<span class="tree_span title_span"> ' + (d.data.name).replace(/_/g,' ') + '</span>'
                });

            my_group.selectAll(".tree_textdiv")
                .each(function(d){
                var my_width = d.x1 - d.x0;
                var title_width = d3.select(this).select(".title_span")._groups[0][0].offsetWidth;
                if((title_width + 4) > my_width){
                    d3.select(this).html("");
                }
            });


            function rect(rect) {

                rect
                    .transition()
                    .duration(200)
                    .attr("x", d => x_scale(d.x0))
                    .attr("y", d => y_scale(d.y0))
                    .attr("width", d => x_scale(d.x1) - x_scale(d.x0))
                    .attr("height", d => y_scale(d.y1) - y_scale(d.y0))
                    .attr("fill", d => my_colours[d.data.name])
                    .attr("transform",transform_str)

            }

        }




    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    return my;
}



function account_size_circles() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        chart_data=[],
        my_labels={},
        my_class="";
    function my(svg) {

        var account_sizes = ["u50","50-1m","a1m"];
        var x_scale = d3.scaleBand().domain(account_sizes).range([0,width-margins.left - margins.right]);
        var radius_range = d3.scaleLinear().domain([0,10]).range([0,40]);
        var colour_scale = d3.scaleOrdinal().domain(account_sizes).range(["#B0B0B0","#808080","#606060"]);

        if(d3.select(".account_size_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","account_size_title" + my_class);
        }
        d3.select(".account_size_title" + my_class)
            .attr("font-weight","bold")
            .attr("x",margins.left + 5)
            .attr("y",margins.top)
            .text("Account Size Profile");

        account_sizes.forEach(d => chart_data.push({"name":d,"value":my_data[d]}))
        var accounts_group = svg.selectAll('.accounts_group' + my_class)
            .data(chart_data)
            .join(function(group){
                var enter = group.append("g").attr("class","accounts_group" + my_class);
                enter.append("circle").attr("class","account_circle");
                enter.append("text").attr("class","account_circle_label");
                return enter;
            });

        accounts_group.select(".account_circle_label")
            .attr("font-size",12)
            .attr("x",d => x_scale(d.name)  + (x_scale.bandwidth()/2))
            .attr("y",height - 15)
            .attr("text-anchor","middle")
            .text(d => my_labels[d.name])
            .attr("transform","translate(" + margins.left + "," + margins.top + ")");

        accounts_group.select(".account_circle")
            .attr("cx",d => x_scale(d.name) + (x_scale.bandwidth()/2))
            .attr("cy",height - 15 - 70)
            .attr("r",d => radius_range(d.value))
            .attr("fill",d => colour_scale(d.name))
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_labels = function(value) {
        if (!arguments.length) return my_labels;
        my_labels = value;
        return my;
    };

    return my;
}



function tax_bar() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data=[],
        my_class="";
    function my(svg) {


        var bar_data = [{"name": "Q","value": my_data[0]},{"name": "NQ","value": my_data[1]}];
        var chart_height = height - margins.bottom - 35;

        var colour_scale = d3.scaleOrdinal().domain(["Q","NQ"]).range([onboarding.colors.midgreen,onboarding.colors.greengold])
        var x_scale = d3.scaleBand().domain(["Q","NQ"]).range([0, width-margins.left-margins.right]);
        var y_scale = d3.scaleLinear().domain([0, 1]).range([0, chart_height]);
        var transform_str = "translate(" + margins.left + "," + margins.top + ")";
        var percent_format = d3.format(".0%");

        if(d3.select(".tax_bar_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","tax_bar_title" + my_class);
            svg.append("g").attr("class","axis x_axis" + my_class);
        }
        d3.select(".tax_bar_title" + my_class)
            .attr("font-weight","bold")
            .attr("x",margins.left + 5)
            .attr("y",margins.top - 15)
            .text("Tax Status Profile");

        d3.select(".x_axis" + my_class)
            .call(d3.axisBottom(x_scale).tickSizeOuter(0))
            .attr("transform","translate(" + margins.left + "," + (margins.top + chart_height + 10) + ")")

        var bar_group = svg.selectAll('.bar_group' + my_class)
            .data(bar_data)
            .join(function(group){
                var enter = group.append("g").attr("class","bar_group" + my_class);
                enter.append("rect").attr("class","tax_status_bar");
                enter.append("text").attr("class","tax_status_bar_label");
                return enter;
            });

        bar_group.select(".tax_status_bar")
            .attr("x",d => x_scale(d.name))
            .attr("width",x_scale.bandwidth()-1)
            .attr("fill",d => colour_scale(d.name))
            .attr("transform",transform_str)
            .transition()
            .duration(100)
            .attr("y",d => y_scale(1) - y_scale(d.value)+10)
            .attr("height", d => y_scale(d.value))

        bar_group.select(".tax_status_bar_label")
            .attr("x",d => x_scale(d.name) + (x_scale.bandwidth()/2) - 1)
            .attr("transform",transform_str)
            .transition()
            .duration(100)
            .attr("y",d => y_scale(1) - y_scale(d.value)+8)
            .text(d => percent_format(d.value))



    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    return my;
}


function team_chart() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data=[],
        my_class="",
        team_data={}

    function my(svg) {

        my_data.forEach((d,i) => team_data[i] = {"comms":[],"ic_meetings":false,"og_meetings":false,"name":d.name})
        var colour_scale = d3.scaleOrdinal().domain(["Lead Advisor","Advisor","Operations",""]).range([onboarding.colors.gold,onboarding.colors.greengold,onboarding.colors.midgreen,"#808080"]);
        var position_y = svg.node().getBoundingClientRect().top;
        position_y -= document.getElementById("questions_div").getBoundingClientRect().top;
        var required_height = 75 + (my_data.length * 35);
        svg.attr("height",required_height);
        onboarding.different_height[svg.attr("id")] = required_height;
        if(d3.select(".tax_bar_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","role" + my_class);
            svg.append("text").attr("class","comms" + my_class);
            svg.append("text").attr("class","icmeetings" + my_class);
            svg.append("text").attr("class","ogmeetings" + my_class);
        }
        d3.select(".role" + my_class)
            .attr("font-size",12)
            .attr("font-weight","bold")
            .attr("text-anchor","middle")
            .attr("x",margins.left + 240)
            .attr("y",margins.top - 15)
            .text("Role");

        d3.select(".comms" + my_class)
            .attr("font-size",12)
            .attr("font-weight","bold")
            .attr("text-anchor","middle")
            .attr("x",margins.left + 330)
            .attr("y",margins.top - 15)
            .text("Communications");

        d3.select(".icmeetings" + my_class)
            .attr("font-size",12)
            .attr("font-weight","bold")
            .attr("text-anchor","middle")
            .attr("x",margins.left + 460)
            .attr("y",margins.top - 15)
            .text("Investment Committee");

        d3.select(".ogmeetings" + my_class)
            .attr("font-size",12)
            .attr("font-weight","bold")
            .attr("text-anchor","middle")
            .attr("x",margins.left + 580)
            .attr("y",margins.top - 15)
            .text("Operations Group");

        var team_group = svg.selectAll('.team_group' + my_class)
            .data(my_data)
            .join(function(group){
                var enter = group.append("g").attr("class","team_group" + my_class);
                enter.append("text").attr("class","fa team_icon team_item");
                enter.append("text").attr("class","team_name team_item");
                enter.append("foreignObject").attr("class","team_input_object")
                    .append("xhtml:body").append("div").attr("class","pd_div");
                enter.append("foreignObject").attr("class","team_input_object_2")
                    .append("xhtml:body").append("div").attr("class","pd_div_2");
                enter.append("circle").attr("class","outer_circle ic_outer_circle");
                enter.append("circle").attr("class","inner_circle ic_inner_circle");
                enter.append("circle").attr("class","outer_circle og_outer_circle");
                enter.append("circle").attr("class","inner_circle og_inner_circle");
                enter.append("text").attr("class","delete_team");

                return enter;
            });

        team_group.select(".team_name")
            .attr("class", d => "team_name team_item " + d.name.replace(/ /g,'').toLowerCase())
            .attr("x",margins.left + 40)
            .attr("y",(d,i) => margins.top + 18 + (+i * 35))
            .text(d => d.name);

        team_group.select(".team_icon")
            .attr("class", d => "fa team_icon team_item " + d.name.replace(/ /g,'').toLowerCase())
            .attr("id",(d,i) => "icon_" + i)
            .attr("x",margins.left + 10)
            .attr("y",(d,i) => margins.top + 20 + (+i * 35))
            .attr("font-size", 20)
            .attr("fill","#808080")
            .text('\uf007');

        team_group.select(".team_input_object")
            .attr("width",90)
            .attr("height",20)
            .attr("x",margins.left + 200)
            .attr("y",(d,i) => margins.top  + 3 + (+i * 35))

        d3.selectAll(".pd_div")
            .html((d,i) => "<select id='role_" + i + "' class='role_select'><option></option><option>Lead Advisor</option><option>Advisor</option>><option>Operations</option></select>")

        d3.selectAll(".role_select")
            .on("change",function(d,i){
               d3.select("text#icon_" + i)
                   .attr("fill",colour_scale(this.value));
                team_data[i].role = this.value;
                d3.selectAll("#og_" + i).attr("visibility","visible");
               if(this.value === "Lead Advisor"){
                   check_box("trading",i);
                   check_box("research",i);
                   check_box("commentary",i);
                   d3.selectAll("#og_" + i).attr("visibility","hidden");
                   click_radio(i,"ic");
               } else if (this.value === "Operations"){
                   check_box("trading",i);
                   click_radio(i,"og");
               } else {
                   d3.selectAll("#og_" + i).attr("visibility","hidden");
               }
                check_progress();
                draw_team_profile(d3.select(".profile_div_svg"),team_data,height,position_y);
            })
        team_group.select(".team_input_object_2")
            .attr("width",90)
            .attr("height",40)
            .attr("x",margins.left + 300)
            .attr("y",(d,i) => margins.top  - 3 + (+i * 35))

        d3.selectAll(".pd_div_2")
            .html((d,i) => "<p><input type='checkbox' class='comms_select' id='trading_" + i + "'>TRADING</input></p><p><input type='checkbox' class='comms_select' id='research_" + i + "'>RESEARCH</input><br class='comms_break'></p><p><input type='checkbox' class='comms_select' id='commentary_" + i + "'>COMMENTARY</input></p>")


        d3.selectAll(".comms_select")
            .on("click",function(d){
                var my_val = this.id.split("_")[0];
                var my_id = this.id.split("_")[1];
                if(this.checked === true){
                    team_data[my_id].comms.push(my_val);
                } else {
                    var my_index = team_data[my_id].comms.indexOf(my_val);
                    team_data[my_id].comms.splice(my_index,1);
                }
                check_progress();
                draw_team_profile(d3.select(".profile_div_svg"),team_data,height,position_y);
            })
        team_group.select(".ic_outer_circle")
            .attr("id",(d,i) => "ic_" + i)
            .attr("cx",margins.left + 460)
            .attr("cy",(d,i) => margins.top + 12 + (+i * 35))
            .attr("r",10);

        team_group.select(".og_outer_circle")
            .attr("id",(d,i) => "og_" + i)
            .attr("cx",margins.left + 580)
            .attr("cy",(d,i) => margins.top + 12 + (+i * 35))
            .attr("r",10);

        team_group.select(".ic_inner_circle")
            .attr("id",(d,i) => "ic_" + i)
            .attr("cx",margins.left + 460)
            .attr("cy",(d,i) => margins.top + 12 + (+i * 35))
            .attr("r",8)
            .on("mousemove",function(d){d3.select(this).attr("cursor","pointer").style("fill","#A0A0A0")})
            .on("mouseout",function(d){
                var my_id = this.id.split("_")[1];
                var my_fill = "#D0D0D0";
                if(team_data[my_id].ic_meetings === true){my_fill = "#333333"};
                d3.select(this).attr("cursor","pointer").style("fill",my_fill)
            })
            .on("click",function(d){click_radio(this.id.split("_")[1],"ic")})

        team_group.select(".og_inner_circle")
            .attr("id",(d,i) => "og_" + i)
            .attr("cx",margins.left + 580)
            .attr("cy",(d,i) => margins.top + 12 + (+i * 35))
            .attr("r",8)
            .on("mousemove",function(d){d3.select(this).attr("cursor","pointer").style("fill","#A0A0A0")})
            .on("mouseout",function(d){
                var my_id = this.id.split("_")[1];
                var my_fill = "#D0D0D0";
                if(team_data[my_id].og_meetings === true){my_fill = "#333333"};
                d3.select(this).attr("cursor","pointer").style("fill",my_fill)
            })
            .on("click",function(d){
                var my_id = this.id.split("_")[1];
                if(team_data[my_id].role !== "Operations"){
                    team_data[my_id].role = "Operations";
                    d3.select("#role_" + my_id).node().value = "Operations";
                    check_box("trading",my_id);
                }
                click_radio(this.id.split("_")[1],"og")
            })


        team_group.select(".delete_team")
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default")})
            .attr("x",margins.left + 640)
            .attr("y",(d,i) => margins.top + 15 + (+i * 35))
            .text("x")
            .on("click",function(d){
                var new_data  = my_data.filter(f => f.name !== d.name);
                draw_team_chart(svg,new_data);
            })

        function check_box(my_type,i){
            if(team_data[i].comms.indexOf(my_type) === -1){
                d3.select("#" + my_type + "_" + i).node().click();
            }
        }

        function click_radio(my_id,my_type){
            if(team_data[my_id][my_type + "_meetings"] === true){
                team_data[my_id][my_type + "_meetings"] = false;
                d3.select(".inner_circle#" + my_type + "_" + my_id).style("fill","#D0D0D0");
            } else {
                team_data[my_id][my_type + "_meetings"] = true;
                d3.select(".inner_circle#" + my_type + "_" + my_id).style("fill","#333333");
            }
            check_progress();
            draw_team_profile(d3.select(".profile_div_svg"),team_data,height,position_y);
        }
        function check_progress(){

            if(onboarding.progress_position < 4){
                onboarding.progress_position = 4;
                onboarding.progress_chart.reset_progress();
            }

        }

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };


    my.base_data = function(value) {
        if (!arguments.length) return base_data;
        base_data = value;
        return my;
    };


    return my;
}


function team_profile() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data=[],
        my_class="";

    function my(svg) {

        var colour_scale = d3.scaleOrdinal().domain(["lead","advisor","operations",undefined]).range([onboarding.colors.gold,onboarding.colors.greengold,onboarding.colors.midgreen,"#808080"]);
        var translate_roles = {"Lead Advisor":"LEAD ADVISORS","Advisor":"ADVISORS","Operations":"OPERATIONAL STAFF"}
        var chart_data = [];
        Object.keys(my_data).forEach(k => chart_data.push(my_data[k]));
        var roles_nest = d3.nest(chart_data).key(k => k.role).entries(chart_data.filter(f => f.role !== undefined));
        var ic_group = JSON.parse(JSON.stringify(chart_data.filter(f => f.ic_meetings === true)));
        var og_group = JSON.parse(JSON.stringify(chart_data.filter(f => f.og_meetings === true)));

        var ic_icon_group = svg.selectAll('.ic_icon_group' + my_class)
            .data(ic_group)
            .join(function(group){
                var enter = group.append("g").attr("class","ic_icon_group" + my_class);
                enter.append("text").attr("class","fa ic_icon");
                return enter;
            });

        ic_icon_group.select(".ic_icon")
            .attr("class",d => "fa ic_icon team_item " + d.name.replace(/ /g,'').toLowerCase())
            .attr("font-size", 20)
            .attr("fill",d => colour_scale(d.role))
            .text('\uf007')
            .on("mouseover",function(d){ show_team_tooltip(this,d)})
            .on("mouseout",function(d){
                d3.selectAll(".team_item").attr("opacity",1);
                d3.select(this).attr("cursor","default");
                d3.select(".tooltip")
                    .style("visibility","hidden");
            })

        d3.forceSimulation()
            .force("collide", d3.forceCollide(15))
            .force("x", d3.forceX(width - margins.right - 150).strength(0.4))
            .force("y", d3.forceY(margins.top + (height/4)).strength(0.8))
            .nodes(ic_group)
            .on("tick", ic_ticked);


        function ic_ticked(){

            d3.selectAll(".ic_icon")
                .attr("x",d => d.x)
                .attr("y",d => d.y);

        }

        var og_icon_group = svg.selectAll('.og_icon_group' + my_class)
            .data(og_group)
            .join(function(group){
                var enter = group.append("g").attr("class","og_icon_group" + my_class);
                enter.append("text").attr("class","fa og_icon team_item");
                return enter;
            });

        og_icon_group.select(".og_icon")
            .attr("class",d => "fa og_icon team_item " + d.name.replace(/ /g,'').toLowerCase())
            .attr("font-size", 20)
            .attr("fill",d => colour_scale(d.role))
            .text('\uf007')
            .on("mouseover",function(d){ show_team_tooltip(this,d)})
            .on("mouseout",function(d){
                d3.selectAll(".team_item").attr("opacity",1);
                d3.select(this).attr("cursor","default");
                d3.select(".tooltip")
                    .style("visibility","hidden");
            })

        function show_team_tooltip(my_object, d){
            d3.selectAll(".team_item").attr("opacity",0.2);
            d3.selectAll("." + d.name.replace(/ /g,'').toLowerCase()).attr("opacity",1);
            d3.select(my_object).attr("cursor","pointer");
            var tooltip_text = 'Name: ' + d.name + "<br>";
            if(d.role !== undefined){
                tooltip_text += "Role: " + d.role + "<br>"
            }
            if(d.comms.length > 0){
                tooltip_text += "Comms: " + d.comms.join(", ") + "<br>"
            }
            d3.select(".tooltip")
                .style("left",(d3.event.x + 15) + "px")
                .style("top",d3.event.y + "px")
                .style("visibility","visible")
                .html(tooltip_text);

        }

        d3.forceSimulation()
            .force("collide", d3.forceCollide(15))
            .force("x", d3.forceX(width - margins.right - 150).strength(0.4))
            .force("y", d3.forceY(margins.top + (height/4) + 100).strength(0.8))
            .nodes(og_group)
            .on("tick", og_ticked);


        function og_ticked(){

            d3.selectAll(".og_icon")
                .attr("x",d => d.x)
                .attr("y",d => d.y);

        }

        if(d3.select(".team_profile_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","team_profile_title" + my_class);
            svg.append("text").attr("class","ic_title" + my_class);
            svg.append("text").attr("class","og_title" + my_class);
        }

        d3.select(".team_profile_title" + my_class)
            .attr("font-weight","bold")
            .attr("x",margins.left + 5)
            .attr("y",margins.top)
            .text("Team Profile");

        d3.select(".ic_title" + my_class)
            .attr("font-weight","bold")
            .attr("text-anchor","middle")
            .attr("x",width - margins.right - 150)
            .attr("y",margins.top + 15)
            .text("Investment Committee");

        d3.select(".og_title" + my_class)
            .attr("text-anchor","middle")
            .attr("font-weight","bold")
            .attr("x",width - margins.right - 150)
            .attr("y",margins.top + 100)
            .text("Operations Group");

        margins.top += 20;

        var roles_group = svg.selectAll('.roles_group' + my_class)
            .data(roles_nest)
            .join(function(group){
                var enter = group.append("g").attr("class","roles_group" + my_class);
                enter.append("text").attr("class","roles_label");
                enter.append("g").attr("class","staff_group");
                return enter;
            });

        roles_group.select(".roles_label")
            .attr("fill", d => colour_scale(d.key))
            .attr("x",margins.left + 7)
            .attr("y",(d,i) => margins.top + 18 + (+i * 25))
            .text(d => translate_roles[d.key]);

        roles_group.select(".staff_group")
            .attr("transform",(d,i) => "translate(0," + (margins.top + 18 + (+i * 25)) + ")")
            .attr("id",d => d.key);

        var staff_group = roles_group.select(".staff_group").selectAll('.staff_group' + my_class)
            .data(d => d.values)
            .join(function(group){
                var enter = group.append("g").attr("class","staff_group" + my_class);
                enter.append("text").attr("class","staff_icon fa");
                return enter;
            });

        staff_group.select(".staff_icon")
            .attr("class",d => "fa staff_icon team_item " + d.name.replace(/ /g,'').toLowerCase())
            .attr("x",(d,i) => margins.left + 200 +  (+i * 25))
            .attr("font-size", 20)
            .attr("fill",function(d){return colour_scale(this.parentElement.parentElement.id);})
            .text('\uf007')
            .on("mouseover",function(d,i){
                show_team_tooltip(this,d)
            })
            .on("mouseout",function(d){
                d3.selectAll(".team_item").attr("opacity",1);
                d3.select(this).attr("cursor","default");
                d3.select(".tooltip")
                    .style("visibility","hidden");
            })



    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };


    return my;
}




function account_size() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        base_data=[],
        my_class="",
        my_data={}

    function my(svg) {

        var x_scale = d3.scalePoint().domain(d3.range(0,11,1)).range([0,width-margins.left - margins.right - 150]);
        var account_sizes = ["u50","50-1m","a1m"];
        var account_labels = {"u50":"SMALL (< $50k)","50-1m":"MEDIUM ($50k - $1m)","a1m":"LARGE (> $1m)"};
        var colour_scale = d3.scaleOrdinal().domain(account_sizes).range(["#B0B0B0","#808080","#606060"]);
        var extra_y = 70;
        var position_y = svg.node().getBoundingClientRect().top;
        position_y -= document.getElementById("questions_div").getBoundingClientRect().top;

        account_sizes.forEach(d => my_data[d] = 0);

        if(d3.select(".account_size_title")._groups[0][0] === null) {
            svg.append("text").attr("class", "account_size_title");
            svg.append("text").attr("class", "risk_label account_size_left");
            svg.append("text").attr("class", "risk_label account_size_right");
        }

        d3.select(".account_size_title")
            .attr("x",margins.left)
            .attr("y",margins.top + 35)
            .text("Indicate the relative importance of account sizes");

        d3.select(".account_size_left")
            .attr("x",margins.left + 150)
            .attr("y",margins.top + extra_y - 15)
            .text("unimportant");

        d3.select(".account_size_right")
            .attr("x",width - margins.right)
            .attr("y",margins.top + extra_y - 15)
            .text("critical");


        var asize_group = svg.selectAll('.a_size_group')
            .data(account_sizes)
            .join(function(group){
                var enter = group.append("g").attr("class","a_size_group" );
                enter.append("rect").attr("class","account_size_bar");
                enter.append("circle").attr("class","account_size_circle");
                enter.append("text").attr("class","account_size_circle_label");
                enter.append("text").attr("class","risk_label account_size_label");
                return enter;
            })


        asize_group.select(".account_size_label")
            .attr("x",margins.left)
            .attr("y",(d,i) => margins.top + (+i * 25) + extra_y + 5)
            .text(d => account_labels[d])

        asize_group.select(".account_size_bar")
            .attr("x",margins.left + 150)
            .attr("y",(d,i) => margins.top + (+i * 25) + extra_y)
            .attr("width",width - margins.left - margins.right - 150)
            .attr("height",5)
            .attr("fill",d => colour_scale(d));

        asize_group.select(".account_size_circle")
            .attr("id",(d,i) => "accountsize_" + i)
            .attr("cx",margins.left + 150)
            .attr("cy",(d,i) => margins.top + (+i * 25) + extra_y)
            .attr("r",8)
            .attr("fill",d => colour_scale(d))
            .attr("stroke","#333333")
            .call(d3.drag()
                .on("start", circle_drag_start)
                .on("drag", circle_drag)
                .on("end", circle_drag_end));

        asize_group.select(".account_size_circle_label")
            .attr("pointer-events","none")
            .attr("id",(d,i) => "accountsize_" + i)
            .attr("text-anchor","middle")
            .attr("x",margins.left + 150)
            .attr("y",(d,i) => margins.top + (+i * 25) + extra_y + 5)
            .attr("fill","white")
            .text("0");

        function circle_drag_start() {
            d3.selectAll("#" + this.id).transition().duration(50).style("stroke-width",2);
        }

        function circle_drag(d,i){

            if(d3.event.x > (margins.left + 150)  && d3.event.x < (width - margins.right)){
                d3.selectAll("#" + this.id)
                    .attr("transform","translate(" + (d3.event.x-(margins.left + 150)) + ",0)");
                d3.select("text#" + this.id).text("?");
            }
        }

        function circle_drag_end(d){
            if(onboarding.progress_position < 3){
                onboarding.progress_position = 3;
                onboarding.progress_chart.reset_progress();
            }
            var my_position = d3.event.x - margins.left  - 150;
            var my_x = 0;
            for(i = 0 ; i < x_scale.domain().length ; i ++){
                var x = x_scale.domain()[i];
                if(i === (x_scale.domain().length - 1)){
                    my_x = x;
                } else {
                    if(x_scale(x+1) > my_position){
                        my_x = x;
                        break;
                    }
                }
            }
            d3.selectAll("#" + this.id)
                .transition()
                .duration(200)
                .attr("transform","translate(" + x_scale(my_x) + ",0)");
            d3.select("text#" + this.id).text(my_x);
            my_data[d] = my_x;
            draw_account_size_chart(d3.select(".profile_div_svg"),my_data,account_labels,height,position_y)
        }

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.base_data = function(value) {
        if (!arguments.length) return base_data;
        base_data = value;
        return my;
    };


    return my;
}




function fund_families() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data={},
        fund_data=[],
        my_class="",
        families_per_row=4;

    function my(svg) {

        var rect_width = (width-margins.left-margins.right-15)/families_per_row;
        var rect_height = 25;
        var families = d3.set(fund_data, d => d.Family).values();

        //add top  to top of list.  They are deliberately in reverse order.
        onboarding.top_families.forEach(function(d){
            families = families.filter(f => f !== d);
            families.unshift(d);
        })
        var all_families = JSON.parse(JSON.stringify(families));

        families = families.filter(f => onboarding.families_filter_no.indexOf(f) === -1);

        var required_height = margins.top + margins.bottom + ((parseInt(families.length/families_per_row) + 1) * (rect_height+5)) + 15;
        svg.attr("height",required_height);
        onboarding.different_height[svg.attr("id")] = required_height;


        if(d3.select(".fund_family_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","fund_family_title" + my_class);
            svg.append("text").attr("class","filter_text" + my_class);
            svg.append("text").attr("class","favourites_text" + my_class);
            svg.append("foreignObject").attr("class","family_filter")
                .append("xhtml:div").attr("class","family_filter_div");
        }

        d3.select(".fund_family_title" + my_class)
            .attr("x",margins.left)
            .attr("y",margins.top - 30)
            .attr("fill","#404040")
            .text("Indicate your favourite fund families");

        d3.select(".filter_text" + my_class)
            .attr("x",width - margins.right - 95)
            .attr("y",margins.top - 32)
            .attr("fill","#404040")
            .attr("text-anchor","end")
            .text("Filter:");

        d3.select(".favourites_text" + my_class)
            .attr("font-size",12)
            .attr("x",margins.left)
            .attr("y",margins.top-8)
            .attr("fill",onboarding.colors.darkgold)
            .text("* Alvarium Analytics Favourites (top 3 selected by default)");

        d3.select(".family_filter")
            .attr("width",100)
            .attr("height",25)
            .attr("x",width - margins.right - 90)
            .attr("y",margins.top - 47);

        d3.select(".family_filter_div")
            .html("<input type='text' id='family_filter_input' name='family_filter'>")

        d3.select("#family_filter_input").node().value = onboarding.families_filter_value;

        if(onboarding.families_filter_value !== ""){
            d3.select("#family_filter_input").node().focus();
        }

        d3.select("#family_filter_input")
            .on("input",function(d){
                var matching_families = all_families.filter(f => f.toLowerCase().includes(this.value.toLowerCase()) === true);
                var missing_families = all_families.filter(f => matching_families.findIndex(m => m === f) === -1);
                onboarding.families_filter_no = d3.set(missing_families).values();
                onboarding.families_filter_value = this.value;
                draw_fund_families(svg,my_data);
            })

        var family_group = svg.selectAll('.family_group')
            .data(families)
            .join(function(group){
                var enter = group.append("g").attr("class","family_group" );
                enter.append("rect").attr("class","family_rect");
                enter.append("text").attr("class","family_rect_label");
                enter.append("circle").attr("class","family_button_left");
                enter.append("circle").attr("class","family_button_right");
                enter.append("text").attr("class","fa family_button_left_icon");
                enter.append("text").attr("class","fa family_button_right_icon");
                return enter;
            })

        family_group.select(".family_rect")
            .attr("stroke", "#A0A0A0")
            .attr("fill", get_fill)
            .attr("stroke-width", d => is_selected(d) === true ? 1: 0.5)
            .attr("x", (d,i) => (+i%families_per_row) * (rect_width+5))
            .attr("y",(d,i) => parseInt(+i/families_per_row) * (rect_height + 5) + is_favourite(d))
            .attr("width",rect_width)
            .attr("height",rect_height)
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default")})
            .on("click",function(d){
                var yes_index = onboarding.families_yes.indexOf(d);
                if(yes_index === -1){
                    add_to_yes(d);
                } else {
                    add_to_no(d);
                }
            });

        family_group.select(".family_rect_label")
            .attr("pointer-events","none")
            .attr("fill-opacity", d => is_selected(d) === true > -1 ? 1 : 0.8)
            .attr("x", (d,i) => ((+i%families_per_row) * (rect_width+5)) + (rect_width/2))
            .attr("y",(d,i) => (parseInt(+i/families_per_row) * (rect_height + 5)) + 16.5 + is_favourite(d))
            .style("fill",d => onboarding.top_families.indexOf(d) === -1 ? "#333333" : onboarding.colors.darkgold)
            .text(d => onboarding.top_families.indexOf(d) === -1 ? d : "* " + d)
            .attr("transform","translate(" + margins.left + "," + margins.top + ")");

        family_group.select(".family_button_left")
            .attr("opacity", d => onboarding.families_yes.indexOf(d) > -1 ? 1 : 0.3)
            .attr("cx", (d,i) => ((+i%families_per_row) * (rect_width+5)) + 15)
            .attr("cy",(d,i) => (parseInt(+i/families_per_row) * (rect_height + 5)) + 12.5 + is_favourite(d))
            .attr("r",11)
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")
            .attr("fill","white")
            .attr("stroke","green")
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer").attr("fill",onboarding.colors.gold);})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default").attr("fill","white");})
            .on("click",add_to_yes)

        family_group.select(".family_button_right")
            .attr("opacity", d => onboarding.families_no.indexOf(d) > -1 ? 1 : 0.3)
            .attr("cx", (d,i) => ((+i%families_per_row) * (rect_width+5)) + rect_width - 15)
            .attr("cy",(d,i) => (parseInt(+i/families_per_row) * (rect_height + 5)) + 12.5 + is_favourite(d))
            .attr("r",11)
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")
            .attr("stroke","red")
            .attr("fill","white")
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer").attr("fill",onboarding.colors.gold);})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default").attr("fill","white");})
            .on("click",add_to_no);

        family_group.select(".family_button_left_icon")
            .attr("opacity", d => onboarding.families_yes.indexOf(d) > -1 ? 1 : 0.3)
            .attr("pointer-events","none")
            .attr("x", (d,i) => ((+i%families_per_row) * (rect_width+5)) + 15)
            .attr("y",(d,i) => (parseInt(+i/families_per_row) * (rect_height + 5)) + 17 + is_favourite(d))
            .attr("text-anchor","middle")
            .attr("font-size",14)
            .attr("fill","green")
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")
            .text("\uf164");


        family_group.select(".family_button_right_icon")
            .attr("opacity", d => onboarding.families_no.indexOf(d) > -1 ? 1 : 0.3)
            .attr("pointer-events","none")
            .attr("x", (d,i) => ((+i%families_per_row) * (rect_width+5)) + rect_width - 15)
            .attr("y",(d,i) => (parseInt(+i/families_per_row) * (rect_height + 5)) + 18 + is_favourite(d))
            .attr("text-anchor","middle")
            .attr("font-size",14)
            .attr("fill","red")
            .attr("transform","translate(" + margins.left + "," + margins.top + ")")
            .text("\uf165");


        function add_to_yes(d){
            var no_index = onboarding.families_no.indexOf(d);
            if(no_index > -1){
                onboarding.families_no.splice(no_index,1);
            }
            onboarding.families_yes.push(d);
            onboarding.families_yes = d3.set(onboarding.families_yes).values();
            if(onboarding.families_hierarchy.find(f => f.name === d) === undefined){
                onboarding.families_hierarchy.push({"name":d, "value":1});
            }
            var sunburst_data = {"name":"all","children":onboarding.families_hierarchy};
            draw_fund_sunburst(d3.select(".profile_div_svg"),sunburst_data);
            draw_fund_favourites();
            draw_fund_families(svg,my_data);
        }

        function add_to_no(d){
            var yes_index = onboarding.families_yes.indexOf(d);
            if(yes_index > -1){
                onboarding.families_yes.splice(yes_index,1);
                onboarding.families_hierarchy = onboarding.families_hierarchy.filter(f => f.name !== d);
            }
            onboarding.families_no.push(d);
            onboarding.families_no = d3.set(onboarding.families_no).values();
            draw_fund_families(svg,my_data);
            var sunburst_data = {"name":"all","children":onboarding.families_hierarchy};
            draw_fund_sunburst(d3.select(".profile_div_svg"),sunburst_data);
            draw_fund_favourites();
        }
        function is_favourite(d){
            if(onboarding.top_families.indexOf(d) === -1  && onboarding.families_filter_no.length === 0){
                return 0;
            } else {
                return 0;
            }
        }

        function is_selected(d){
            if(onboarding.families_yes.indexOf(d) > -1){
                return true
            } else if (onboarding.families_no.indexOf(d) > -1){
                return true
            } else {
                return false
            }
        }

        function get_fill(d){
            if(onboarding.families_yes.indexOf(d) > -1){
                return "#e5f5e0"
            } else if (onboarding.families_no.indexOf(d) > -1){
                return "#fcbba1"
            } else {
                return "#F0F0F0"
            }
        }

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.fund_data = function(value) {
        if (!arguments.length) return fund_data;
        fund_data = value;
        return my;
    };

    return my;
}

function fund_sunburst() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data={},
        my_class="";

    function my(svg) {

        const radius = Math.min((width-margins.left-margins.right)/2,350);

        const colour_scale = d3.scaleLinear().domain([0,my_data.children.length]).range([onboarding.colors.lightgold,onboarding.colors.gold]);
        const priority_scale = d3.scaleOrdinal().domain(['avoid','preferred','optimal']).range([onboarding.colors.midgreen,onboarding.colors.gold,onboarding.colors.darkgreen])
        const depth_1_names = d3.set(my_data.children, d => d.name).values();
        //const transform_x = ((width-margins.left-margins.right)/2);
        const transform_x = margins.left + radius;
        const format = d3.format("$,");

        if(d3.select(".sunburst_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","sunburst_title" + my_class);
        }

        d3.select(".sunburst_title" + my_class)
            .attr("font-weight","bold")
            .attr("x",margins.left + 5)
            .attr("y",margins.top + 20)
            .text("Preferred Funds");

        const arc = d3.arc()
            .startAngle(d => d.x0)
            .endAngle(d => d.x1)
            .padAngle(0.01)
            .padRadius(radius / 2)
            .innerRadius(d => d.y0)
            .outerRadius(d => d.y1 - 3);

        const partition = p_data => {
            const root = d3.hierarchy(p_data)
                .sum(d => d.value)
                .sort((a, b) => b.value - a.value);
            return d3.partition()
                .size([2 * Math.PI, radius])
                (root);
        }

        const root = partition(my_data);

        const sunburst_group = svg.selectAll('.sunburst_group')
            .data(root.descendants().slice(1))
            .join(function(group){
                var enter = group.append("g").attr("class","sunburst_group");
                enter.append("path").attr("class","sunburst_path");
                enter.append("text").attr("class","sunburst_text");
                return enter;
            });

        sunburst_group
            .attr("transform","translate(" + (margins.left + transform_x) + " " + (margins.top + radius + 20) + ")");

        sunburst_group.select(".sunburst_path")
            .attr("fill",d => d.depth === 0 ? "none" : (d.depth === 1 ? colour_scale(depth_1_names.indexOf(d.data.name)) :colour_scale(depth_1_names.indexOf(d.parent.data.name))))
            .attr("stroke-width",0.5)
            .attr("stroke",d => onboarding.funds_priority[d.data.name] === undefined ? "none" : priority_scale(onboarding.funds_priority[d.data.name]))
            .attr("d", arc)
            .on("mouseover",function(d){
                var is_family = onboarding.fund_family_data.find(f => f.Family === d.data.name) !== undefined;
                if(is_family === true){
                    var tooltip_text = "Fund Family: " + d.data.name + "<br>"
                    + "Total Assets: " + format(d3.sum(onboarding.fund_family_data.filter(f => f.Family === d.data.name), s => +s.Assets)) + "<br>"
                        if(onboarding.top_families.indexOf(d) > -1){
                            tooltip_text += "<span id='favourite'>Alvarium Favourite</span>"
                        }
                } else {
                   var my_fund = onboarding.fund_family_data.find(f => f.Ticker === d.data.name);
                    var tooltip_text = "Fund Name: " + my_fund.Name + "<br>"
                    + "Ticker: " + my_fund.Ticker + "<br>"
                        + "Family: " + my_fund.Family + "<br>"
                        + "Asset Class: " + my_fund.AssetClass + "<br>"
                        + "Asset Class: " + format(my_fund.Assets) + "<br>"
                }
                d3.select(".tooltip")
                    .style("left",(d3.event.pageX + 15) + "px")
                    .style("top",d3.event.pageY + "px")
                    .style("visibility","visible")
                    .html(tooltip_text);
            })
            .on("mouseout",function(d){
                d3.select(".tooltip").style("visibility","hidden");
            })

        sunburst_group.select(".sunburst_text")
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("font-family", "sans-serif")
            .attr("transform", function(d) {
                const x = ((d.x0 + d.x1) / 2 * 180 / Math.PI) ;
                const y = ((d.y0 + d.y1) / 2) ;
                return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;
            })
            .attr("dy", "0.35em")
            .text(d => d.depth > 0 ? d.data.name : "");
        ;




    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    return my;
}

function fund_favourites() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data=[],
        fund_data=[],
        my_class="";

    function my(svg) {

        var families = d3.set(my_data, d => d.name).values();
        var funds = fund_data.filter(d => families.indexOf(d.Family) > -1);
        var all_funds = JSON.parse(JSON.stringify(funds));
        funds = funds.filter(f => onboarding.funds_filter_no.indexOf(f.Ticker) === -1);
        var required_height = margins.top + margins.bottom + (funds.length * 25);
        svg.attr("height",required_height);
        onboarding.different_height[svg.attr("id")] = required_height;
        funds = funds.filter(f => onboarding.funds_no.indexOf(f.Ticker) === -1);
        funds = funds.sort((a,b) => d3.descending(a[onboarding.funds_sort_by], b[onboarding.funds_sort_by]))
        funds = funds.concat(all_funds.filter(f => onboarding.funds_no.indexOf(f.Ticker) > -1));
        var value_format = d3.format("$.2s");

        if(d3.select(".fund_favourites_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","fund_favourites_title" + my_class);
            svg.append("text").attr("class","filter_title" + my_class);
            svg.append("text").attr("class","sort_by" + my_class);
            svg.append("rect").attr("class","sort_rect asset_rect" + my_class + " sort_item" + my_class);
            svg.append("rect").attr("class","sort_rect fund_rect" + my_class + " sort_item" + my_class);
            svg.append("text").attr("class","risk_label asset" + my_class + " sort_item" + my_class);
            svg.append("text").attr("class","risk_label fund" + my_class + " sort_item" + my_class);
            svg.append("foreignObject").attr("class","fund_filter")
                .append("xhtml:div").attr("class","fund_filter_div");
        }

        d3.select(".fund_favourites_title" + my_class)
            .attr("x",margins.left)
            .attr("y",margins.top - 30)
            .attr("fill","#404040")
            .text("Indicate your favourites from the list of funds in your favourite families");

        d3.select(".filter_title" + my_class)
            .attr("text-anchor","end")
            .attr("x",width - margins.right - 95)
            .attr("y",margins.top - 32)
            .attr("fill","#404040")
            .text("Filter: ");

        d3.select(".sort_by" + my_class)
            .attr("font-weight","bold")
            .attr("font-size",12)
            .attr("text-anchor","end")
            .attr("x",width - margins.right - 80)
            .attr("y",margins.top-8)
            .attr("fill","#404040")
            .text("sort by: ");

        d3.select(".asset_rect" + my_class)
            .attr("id","asset_item" + my_class)
            .attr("opacity",get_opacity)
            .attr("x",width - margins.right - 75)
            .attr("y",margins.top-18)
            .attr("width",35)
            .attr("height",15);

        d3.select(".fund_rect" + my_class)
            .attr("id","fund_item" + my_class)
            .attr("opacity",get_opacity)
            .attr("opacity",0.2)
            .attr("x",width - margins.right - 35)
            .attr("y",margins.top-18)
            .attr("width",35)
            .attr("height",15);

        d3.select(".asset" + my_class)
            .attr("id","asset_item" + my_class)
            .attr("opacity",get_opacity)
            .attr("pointer-events","none")
            .attr("x",width - margins.right - 57.5)
            .attr("y",margins.top-7)
            .text("asset");

        d3.select(".fund" + my_class)
            .attr("id","fund_item" + my_class)
            .attr("pointer-events","none")
            .attr("opacity",get_opacity)
            .attr("x",width - margins.right - 17.5)
            .attr("y",margins.top-7)
            .text("fund");

        d3.selectAll(".sort_item" + my_class)
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer").attr("opacity",0.5).style("fill","white")})
            .on("mouseout",function(d){
                d3.select(this)
                .attr("cursor","pointer")
                    .attr("opacity",get_opacity)
                .style("fill","#F0F0F0")})
            .on("click",function(d){
                d3.selectAll(".sort_item" + my_class)
                    .attr("opacity",0.2);
                d3.selectAll("#" + this.id).attr("opacity",1);
                var my_type = this.id.split("_")[0];
                if(my_type === "asset"){
                    onboarding.funds_sort_by = "Assets";
                } else {
                    onboarding.funds_sort_by = "Name";
                }
                draw_fund_favourites();
            })

        function get_opacity(d,i,object){
            var my_type = object[0].id.split("_")[0];
            if(my_type === "asset"  &&  onboarding.funds_sort_by === "Assets"){
               return 1
            } else  if (my_type === "fund"  && onboarding.funds_sort_by === "Name"){
               return 1
            } else {
                return 0.2
            }
        }
        d3.select(".fund_filter")
            .attr("width",100)
            .attr("height",25)
            .attr("x",width - margins.right - 90)
            .attr("y",margins.top - 47);

        d3.select(".fund_filter_div")
            .html("<input type='text' id='fund_filter_input' name='fund_filter'>")

        d3.select("#fund_filter_input").node().value = onboarding.funds_filter_value;

        if(onboarding.funds_filter_value !== ""){
            d3.select("#fund_filter_input").node().focus();
        }
        d3.select("#fund_filter_input")
            .on("input",function(d){
                var matching_funds = all_funds.filter(f => (f.Name.toLowerCase() + f.Ticker.toLowerCase()).includes(this.value.toLowerCase()) === true);
                var missing_funds = all_funds.filter(f => matching_funds.findIndex(m => m === f) === -1);
                onboarding.funds_filter_no = d3.set(missing_funds, s => s.Ticker).values();
                onboarding.funds_filter_value = this.value;
                draw_fund_favourites();
            })


        const funds_group = svg.selectAll('.funds_group')
            .data(funds)
            .join(function(group){
                var enter = group.append("g").attr("class","funds_group");
                enter.append("circle").attr("class","family_button_yes");
                enter.append("text").attr("class","fa family_button_yes_icon");
                enter.append("circle").attr("class","family_button_no");
                enter.append("text").attr("class","fa family_button_no_icon");
                enter.append("circle").attr("class","inner_circle");
                enter.append("text").attr("class","fund_name");

                return enter;
            });


        funds_group.select(".family_button_yes")
            .attr("opacity", d => onboarding.funds_yes.indexOf(d.Ticker) > -1 ? 1 : 0.3)
            .attr("cx",margins.left + 10)
            .attr("cy",(d,i) => margins.top + 10 + (+i * 25))
            .attr("r",10)
            .attr("fill","white")
            .attr("stroke","green")
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer").attr("fill",onboarding.colors.gold);})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default").attr("fill","white");})
            .on("click",function(d){
                if(onboarding.progress_position < 6){
                    onboarding.progress_position = 6;
                    onboarding.progress_chart.reset_progress();
                }
                onboarding.families_hierarchy.forEach(function(f){
                    if(f.name === d.Family){
                        delete(f.value);
                        if(f.children === undefined){f.children = []};
                        f.children.push({"name": d.Ticker, "value": 1})
                    }
                })
                var no_index = onboarding.funds_no.indexOf(d.Ticker);
                if(no_index > -1){
                    onboarding.funds_no.splice(no_index,1);
                }
                onboarding.funds_yes.push(d.Ticker);
                var sunburst_data = {"name":"all","children":onboarding.families_hierarchy};
                draw_fund_sunburst(d3.select(".profile_div_svg"),sunburst_data);
                draw_fund_favourites();
            })

        funds_group.select(".family_button_no")
            .attr("opacity", d => onboarding.funds_no.indexOf(d.Ticker) > -1 ? 1 : 0.3)
            .attr("cx",margins.left + 35)
            .attr("cy",(d,i) => margins.top + 10 + (+i * 25))
            .attr("r",10)
            .attr("fill","white")
            .attr("stroke","red")
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer").attr("fill",onboarding.colors.gold);})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default").attr("fill","white");})
            .on("click",function(d){
                var yes_index = onboarding.funds_yes.indexOf(d.Ticker);
                if(yes_index > -1){
                    onboarding.funds_yes.splice(yes_index,1);
                    onboarding.families_hierarchy.forEach(function(f){
                        if(f.name === d.Family){
                            f.children = f.children.filter(c => c.name !== d.Ticker);
                            if(f.children.length === 0){
                                f.value = 1;
                            }
                        }
                    })
                }
                onboarding.funds_no.push(d.Ticker);
                var sunburst_data = {"name":"all","children":onboarding.families_hierarchy};
                draw_fund_sunburst(d3.select(".profile_div_svg"),sunburst_data);
                draw_fund_favourites();
            });

        funds_group.select(".family_button_yes_icon")
            .attr("opacity", d => onboarding.funds_yes.indexOf(d.Ticker) > -1 ? 1 : 0.3)
            .attr("pointer-events","none")
            .attr("x",margins.left + 10)
            .attr("y",(d,i) => margins.top + 10 + (+i * 25) + 4)
            .attr("text-anchor","middle")
            .attr("font-size",14)
            .attr("fill","green")
            .text("\uf164");

        funds_group.select(".family_button_no_icon")
            .attr("opacity", d => onboarding.funds_no.indexOf(d.Ticker) > -1 ? 1 : 0.3)
            .attr("pointer-events","none")
            .attr("x",margins.left + 35)
            .attr("y",(d,i) => margins.top + 10 + (+i * 25) + 5)
            .attr("text-anchor","middle")
            .attr("font-size",14)
            .attr("fill","red")
            .text("\uf165");

        funds_group.select(".fund_name")
            .attr("fill",get_fill)
            .attr("font-size",12)
            .attr("x", margins.left + 50)
            .attr("y",(d,i) => margins.top + 14 + (+i * 25))
            .text(d => d.Name + " (" + d.Ticker + ")");

        function get_fill(d){
            if(onboarding.funds_yes.indexOf(d.Ticker) > -1){
                return "#006d2c"
            } else if (onboarding.funds_no.indexOf(d.Ticker) > -1){
                return "#cb181d"
            } else {
                return "#333333"
            }
        }

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.fund_data = function(value) {
        if (!arguments.length) return fund_data;
        fund_data = value;
        return my;
    };

    return my;
}


function benchmark_entry() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data=[],
        my_class="",
        selected_benchmark = "",
        compare_benchmark = "";

    function my(svg) {

        var percent_format = d3.format(".0%");
        var decimal_percent_format = d3.format(".2~%");
        var sharpe_format = d3.format(".2f")
        var benchmarks = d3.set(my_data, d => d.Benchmark).values();
        var risk_categories = d3.set(my_data, d => d["Risk Category"]).values();
        var risk_order = ["Aggressive", "Moderate Aggressive", "Moderate", "Moderate Conservative", "Conservative"];
        risk_categories = risk_categories.sort((a,b) => d3.ascending(risk_order.indexOf(a),risk_order.indexOf(b)));
        var results_sort_order = ["Return","Volatility","Sharpe Ratio","Maximum Drawdown"];
         var historic_categories = d3.set(my_data.filter(f => f.Type === "Historic"), d => d.Category).values();
        var forecast_categories = d3.set(my_data.filter(f => f.Type === "Forecast"), d => d.Category).values();
        historic_categories = historic_categories.sort((a,b) => d3.ascending(results_sort_order.indexOf(a),results_sort_order.indexOf(b)))
        forecast_categories = forecast_categories.sort((a,b) => d3.ascending(results_sort_order.indexOf(a),results_sort_order.indexOf(b)))
        benchmarks.unshift("");

        if(d3.select(".benchmarks_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","benchmarks_title" + my_class);
            svg.append("text").attr("class","benchmarks_selected" + my_class);
            svg.append("foreignObject").attr("class","bm_input_object")
                .append("xhtml:body").append("div").attr("class","bm_div");
            svg.append("text").attr("class","compare_items benchmarks_compare" + my_class);
            svg.append("foreignObject").attr("class","bm_input_object_2")
                .append("xhtml:body").append("div").attr("class","bm_div_2");
            svg.append("text").attr("class","compare_items historical_title" + my_class);
            svg.append("text").attr("class","compare_items forecast_title" + my_class);
        }


        d3.select(".benchmarks_title" + my_class)
            .attr("x",margins.left)
            .attr("y",margins.top - 30)
            .attr("fill","#404040")
            .text("Use the comparison tool to choose your Benchmarks");

        var other_categories = ["Sharpe Ratio","Volatility","Maximum Drawdown","Return"];
        var investment_categories = d3.set(my_data, d => d.Category).values().filter(f => other_categories.indexOf(f) === -1);

        const investment_group = svg.selectAll('.investment_group')
            .data(investment_categories)
            .join(function(group){
                var enter = group.append("g").attr("class","investment_group");
                enter.append("text").attr("class","investment_label");
                enter.append("rect").attr("class","investment_rect");
                return enter;
            });

        investment_group.select(".investment_rect")
            .attr("y",margins.top - 40)
            .attr("width",20)
            .attr("height",12)
            .attr("fill", d => onboarding.asset_colours[d])
            .attr("stroke","#333333")
            .attr("stroke-width",0.25);

        investment_group.select(".investment_label")
            .attr("font-size",11)
            .attr("id",(d,i) => "inv_label" + i)
            .attr("y",margins.top - 30)
            .text(d => d);

        var inv_x = 0;

        d3.selectAll(".investment_rect").each(function(d,i){
            d3.select(this).attr("x",inv_x);
            d3.select("#inv_label" + i).attr("x", inv_x + 25);
            var text_width = document.getElementById("inv_label" + i).getBoundingClientRect().width;
            inv_x += (30 + text_width)
        })

        investment_group.attr("transform","translate(" + (width - margins.right - inv_x - 5) + ",0)");


        d3.select(".benchmarks_selected" + my_class)
            .attr("x",margins.left)
            .attr("y",margins.top - 5)
            .attr("fill","#404040")
            .text("SELECTED");

        d3.select(".bm_input_object")
            .attr("width",120)
            .attr("height",20)
            .attr("x",margins.left)
            .attr("y",(d,i) => margins.top);

        d3.select(".bm_div")
            .html("<select id='bm_selected' style='background-color: #FDD649'></select>");

        d3.selectAll("#bm_selected")
            .style("width",120)
            .on("change",function(d){
                if(this.value === ""){
                    d3.selectAll(".compare_items").style("visibility","hidden");
                    reset_other_select("#bm_selected",this.value);
                    draw_selected(this.value,[]);
                    draw_benchmark_bar(d3.select(".profile_div_svg"),[],150,0)

                    draw_compare(this.value,[],false);
                    document.getElementById("bm_compare").value = "";
                } else {
                    if(onboarding.progress_position < 7){
                        onboarding.progress_position = 7;
                        onboarding.progress_chart.reset_progress();
                    }
                    d3.selectAll(".compare_items").style("visibility","visible");
                    draw_selected(this.value,risk_categories);
                    var bar_data = my_data.filter(f => f.Type === "Investment"  && f.Benchmark === selected_benchmark);
                    draw_benchmark_bar(d3.select(".profile_div_svg"),bar_data,150,0)
                    draw_compare("",risk_categories,true);
                }
                reset_other_select("#bm_compare",this.value);
            })
            .selectAll("option")
            .data(benchmarks)
            .join("option")
            .text(d => d);

        d3.select(".benchmarks_compare" + my_class)
            .style("visibility","hidden")
            .attr("x",margins.left)
            .attr("y",margins.top + 40)
            .attr("fill","#404040")
            .text("COMPARE");

        d3.select(".bm_input_object_2")
            .attr("width",120)
            .attr("height",20)
            .attr("x",margins.left)
            .attr("y",(d,i) => margins.top + 45);

        d3.select(".bm_div_2")
            .html("<select class='compare_items' id='bm_compare' style='background-color: #c7e0b1'></select>");

        d3.selectAll("#bm_compare")
            .style("visibility","hidden")
            .on("change",function(d){
                if(this.value !== ""){
                    draw_compare(this.value,risk_categories,false);
                } else {
                    draw_compare(this.value,risk_categories,true)
                }
            })
            .selectAll("option")
            .data(benchmarks)
            .join("option")
            .text(d => d);

        var risk_left = margins.left + 130;
        var risk_width = (width - margins.right - risk_left)/risk_categories.length;

        d3.select(".historical_title" + my_class)
            .style("visibility","hidden")
            .attr("x",risk_left + ((width - margins.right - risk_left)/2))
            .attr("y",margins.top + 90)
            .attr("text-anchor","middle")
            .attr("font-weight","bold")
            .text("HISTORICAL");

        d3.select(".forecast_title" + my_class)
            .style("visibility","hidden")
            .attr("x",risk_left + ((width - margins.right - risk_left)/2))
            .attr("y",margins.top + 210)
            .attr("text-anchor","middle")
            .attr("font-weight","bold")
            .text("FORECAST");

        const historic_labels_group = svg.selectAll('.historic_labels_group')
            .data(historic_categories)
            .join(function (group) {
                var enter = group.append("g").attr("class", "compare_items historic_labels_group")
                    .style("visibility","hidden") ;
                enter.append("text").attr("class", "historic_label");
                return enter;
            });

        historic_labels_group.select(".historic_label")
            .attr("x",risk_left+5)
            .attr("font-size",12)
            .attr("y",(d,i) => margins.top + 112 + (i * 20))
            .attr("text-anchor","end")
            .text(d => d);

        const forecast_labels_group = svg.selectAll('.forecast_labels_group')
            .data(forecast_categories)
            .join(function (group) {
                var enter = group.append("g").attr("class", "forecast_labels_group compare_items")
                    .style("visibility","hidden");
                enter.append("text").attr("class", "forecast_label");
                return enter;
            });

        forecast_labels_group.select(".forecast_label")
            .attr("x",risk_left+5)
            .attr("font-size",12)
            .attr("y",(d,i) => margins.top + 232 + (i * 20))
            .attr("text-anchor","end")
            .text(d => d);


        function reset_other_select(my_id,my_value){
            var other_options = benchmarks.filter(f => f !== my_value);
            if(my_value === "") {
                other_options = benchmarks;
            }
            d3.selectAll(my_id)
                .selectAll("option")
                .data(other_options)
                .join("option")
                .text(d => d);
        }

        function draw_compare(my_value,categories,just_selected) {
            compare_benchmark = my_value;

            const compare_risk_group = svg.selectAll('.compare_risk_group')
                .data(categories)
                .join(function (group) {
                    var enter = group.append("g").attr("class", "compare_risk_group");
                    enter.append("g").attr("class", "changes_group");
                    enter.append("g").attr("class", "historical_group");
                    enter.append("g").attr("class", "forecast_group");
                    return enter;
                });

            compare_risk_group.select(".changes_group")
                .attr("transform", (d, i) => "translate(" + ((i * risk_width) + risk_left + 5)
                    + "," + (margins.top + 55) + ")");

            compare_risk_group.select(".historical_group")
                .attr("transform", (d, i) => "translate(" + ((i * risk_width) + risk_left + 5)
                    + "," + (margins.top + 100) + ")");

            compare_risk_group.select(".forecast_group")
                .attr("transform", (d, i) => "translate(" + ((i * risk_width) + risk_left + 5)
                    + "," + (margins.top + 220) + ")");

            const compare_risk_items_group = compare_risk_group.select(".changes_group")
                .selectAll('.compare_risk_items_group')
                .data(function (d) {
                    var investment_data = my_data.filter(f => f["Risk Category"] === d && f.Type === "Investment");
                    return (just_selected === true ? [] : get_nest(investment_data,d));
                })
                .join(function (group) {
                    var enter = group.append("g").attr("class", "compare_risk_items_group");
                    enter.append("rect").attr("class", "changes_rect");
                    enter.append("text").attr("class", "changes_text");
                    enter.append("rect").attr("class", "changes_line_rect");
                    return enter;
                });

            var changes_width = (risk_width - 10) / 4;

            compare_risk_items_group.select(".changes_line_rect")
                .attr("x", (d, i) => (i * changes_width))
                .attr("y", 4)
                .attr("width", changes_width)
                .attr("height", 3)
                .attr("fill", d => onboarding.asset_colours[d.key])

            compare_risk_items_group.select(".changes_rect")
                .attr("x", (d, i) => (i * changes_width))
                .attr("y", -11)
                .attr("width", changes_width - 2)
                .attr("height", 15)
                .attr("fill", "transparent")
                .on("mousemove", function (d) {
                    var id_risk = d.risk.replace(/ /g, '').toLowerCase();
                    var id_asset = d.key.replace(/ /g, '').toLowerCase();
                    var tooltip_text = "<span id='" + id_risk + "'>" + d.risk.toUpperCase() + "</span><br>" +
                        "<span id='" + id_asset + "'<strong>" + d.key + "</strong></span> ("
                        + (d.value.value > 0 ? "+" : "") + percent_format(d.value.value) + ")<br>"
                        + selected_benchmark + ": " + percent_format(d.value.selected) + "<br>"
                        + compare_benchmark + ": " + percent_format(d.value.compare) + "<br>"

                    d3.select(".tooltip")
                        .style("left", (d3.event.x + 25) + "px")
                        .style("top", d3.event.y + "px")
                        .style("visibility", "visible")
                        .html(tooltip_text);

                    d3.select("#" + id_risk).style("color", onboarding.risk_colours[d.risk])
                    d3.select("#" + id_asset).style("color", onboarding.asset_colours[d.key])
                })
                .on("mouseout", function (d) {
                    d3.select(this).attr("cursor", "default");
                    d3.select(".tooltip").style("visibility", "hidden");
                })

            compare_risk_items_group.select(".changes_text")
                .attr("pointer-events", "none")
                .attr("x", (d, i) => (i * changes_width) + (changes_width / 2) - 1)
                .attr("font-size", 12)
                .attr("font-weight", "bold")
                .attr("text-anchor", "middle")
                .attr("fill", d => onboarding.asset_colours[d.key])
                .text(d => d.value.value > 0 ? "+" + percent_format(d.value.value) : percent_format(d.value.value));

            const historical_group = compare_risk_group.select(".historical_group")
                .selectAll('.compare_historical_group')
                .data(function (d) {
                    var investment_data = my_data.filter(f => f["Risk Category"] === d && f.Type === "Historic");
                    return get_nest(investment_data,d);
                })
                .join(function (group) {
                    var enter = group.append("g").attr("class", "compare_historical_group");
                    enter.append("rect").attr("class", "historical_rect");
                    enter.append("text").attr("class", "historical_text");
                    return enter;
                });

            historical_group.select(".historical_rect")
                .attr("display",just_selected === true ? "none" : "block")
                .attr("x", 5)
                .attr("y", (d,i) => i * 20)
                .attr("width", risk_width-10)
                .attr("height", 18)
                .attr("fill", d => get_compare_result(d)[2])
                .on("mousemove", function (d) {
                    var id_risk = d.risk.replace(/ /g, '').toLowerCase();
                    var compare_result = get_compare_result(d);
                    var my_format = d.key === "Sharpe Ratio" ? sharpe_format : decimal_percent_format;
                    var tooltip_text = "<span id='" + id_risk + "'>" + d.risk.toUpperCase() + "</span><br>" +
                        "<strong>" + d.key.toUpperCase() + "</strong> ("
                        + (compare_result[1] > 0 ? "+" : "") + my_format(compare_result[1]) + ")<br>"
                        + "Best Results: <span id='result_span' >" + compare_result[0] + "</span><br>"
                        + selected_benchmark + ": " + my_format(d.value.selected) + "<br>"
                        + compare_benchmark + ": " + my_format(d.value.compare) + "<br>"

                    d3.select(".tooltip")
                        .style("left", (d3.event.x + 25) + "px")
                        .style("top", d3.event.y + "px")
                        .style("visibility", "visible")
                        .html(tooltip_text);

                    d3.select("#" + id_risk).style("color", onboarding.risk_colours[d.risk]);
                    d3.select("#result_span").style("color", compare_result[2]);

                })
                .on("mouseout", function (d) {
                    d3.select(this).attr("cursor", "default");
                    d3.select(".tooltip").style("visibility", "hidden");
                })


            historical_group.select(".historical_text")
                .attr("pointer-events","none")
                .attr("x", (risk_width/2) + 20)
                .attr("y", (d,i) => 14 + (i * 20))
                .attr("text-anchor","end")
                .text(d =>
                    just_selected === true ? (d.key === "Sharpe Ratio" ? sharpe_format(d.value.selected) : decimal_percent_format(d.value.selected))
                        : (d.key === "Sharpe Ratio" ? sharpe_format(get_compare_result(d)[1]) : decimal_percent_format(get_compare_result(d)[1])))


            const forecast_group = compare_risk_group.select(".forecast_group")
                .selectAll('.compare_forecast_group')
                .data(function (d) {
                    var investment_data = my_data.filter(f => f["Risk Category"] === d && f.Type === "Forecast");
                    return get_nest(investment_data,d);
                })
                .join(function (group) {
                    var enter = group.append("g").attr("class", "compare_forecast_group");
                    enter.append("rect").attr("class", "forecast_rect");
                    enter.append("text").attr("class", "forecast_text");
                    return enter;
                });

            forecast_group.select(".forecast_rect")
                .attr("display",just_selected === true ? "none" : "block")
                .attr("x", 5)
                .attr("y", (d,i) => i * 20)
                .attr("width", risk_width-10)
                .attr("height", 18)
                .attr("fill", d => get_compare_result(d)[2])
                .on("mousemove", function (d) {
                    var id_risk = d.risk.replace(/ /g, '').toLowerCase();
                    var compare_result = get_compare_result(d);
                    var my_format = d.key === "Sharpe Ratio" ? sharpe_format : decimal_percent_format;
                    var tooltip_text = "<span id='" + id_risk + "'>" + d.risk.toUpperCase() + "</span><br>" +
                        "<strong>" + d.key.toUpperCase() + "</strong> ("
                        + (compare_result[1] > 0 ? "+" : "") + my_format(compare_result[1]) + ")<br>"
                        + "Best Results: <span id='result_span' >" + compare_result[0] + "</span><br>"
                        + selected_benchmark + ": " + my_format(d.value.selected) + "<br>"
                        + compare_benchmark + ": " + my_format(d.value.compare) + "<br>"

                    d3.select(".tooltip")
                        .style("left", (d3.event.x + 25) + "px")
                        .style("top", d3.event.y + "px")
                        .style("visibility", "visible")
                        .html(tooltip_text);

                    d3.select("#" + id_risk).style("color", onboarding.risk_colours[d.risk]);
                    d3.select("#result_span").style("color", compare_result[2]);

                })
                .on("mouseout", function (d) {
                    d3.select(this).attr("cursor", "default");
                    d3.select(".tooltip").style("visibility", "hidden");
                })


            forecast_group.select(".forecast_text")
                .attr("pointer-events","none")
                .attr("x", (risk_width/2) + 20)
                .attr("y", (d,i) => 14 + (i * 20))
                .attr("text-anchor","end")
                .text(d =>
                 just_selected === true ? (d.key === "Sharpe Ratio" ? sharpe_format(d.value.selected) : decimal_percent_format(d.value.selected))
                    : (d.key === "Sharpe Ratio" ? sharpe_format(get_compare_result(d)[1]) : decimal_percent_format(get_compare_result(d)[1])))

            function get_compare_result(d){
                var desirable = onboarding.benchmarks_desirable[d.key];
                if(desirable = "high"){
                    if(d.value.selected > d.value.compare){
                        return [selected_benchmark,d.value.selected-d.value.compare,onboarding.colors.middlegold]
                    } else if (d.value.selected < d.value.compare){
                        return [compare_benchmark,d.value.selected-d.value.compare,onboarding.colors.lightgreen]
                    } else {
                        return ["equal",0,"white"]
                    }
                } else {
                    if(d.value.selected < d.value.compare){
                        return [selected_benchmark,d.value.selected-d.value.compare,onboarding.colors.middlegold]
                    } else if (d.value.selected > d.value.compare){
                        return [compare_benchmark,d.value.selected-d.value.compare,onboarding.colors.lightgreen]
                    } else {
                        return ["equal",0,"white"]
                    }
                }
            }


            function get_nest(nest_data,d){


                var nest = d3.nest().key(k => k.Category)
                    .rollup(function (r) {
                            var compare_value = 0;
                            if(compare_benchmark !== ""){
                                compare_value = +r.find(f => f.Benchmark === compare_benchmark).Value;
                            }
                            var selected_value = +r.find(f => f.Benchmark === selected_benchmark).Value;
                            return {
                                "compare": compare_value,
                                "selected": selected_value,
                                "value": compare_value - selected_value
                            }
                        }
                    ).entries(nest_data);
                nest.map(m => m.risk = d);
                nest = nest.sort((a,b) => d3.ascending(results_sort_order.indexOf(a.key),results_sort_order.indexOf(b.key)))
                return nest;
            }


        }

        function draw_selected(my_value,categories){

            selected_benchmark = my_value;

            const risk_group = svg.selectAll('.risk_group')
                .data(categories)
                .join(function(group){
                    var enter = group.append("g").attr("class","risk_group");
                    enter.append("text").attr("class","risk_title");
                    enter.append("rect").attr("class","bar_rect");
                    enter.append("g").attr("class","bar_group");

                    return enter;
                });

            risk_group.select(".risk_title")
                .attr("x",(d,i) => (risk_width/2) + (i * risk_width) + risk_left)
                .attr("y",margins.top)
                .attr("fill",d => onboarding.risk_colours[d])
                .attr("font-size",11)
                .style("text-transform","uppercase")
                .attr("text-anchor","middle")
                .text(d => onboarding.risk_short_labels[d]);

            risk_group.select(".bar_rect")
                .attr("x",(d,i) =>  (i * risk_width) + risk_left + 5)
                .attr("y",margins.top + 5)
                .attr("fill","transparent")
                .attr("stroke","#333333")
                .attr("stroke-width",0.25)
                .attr("width",risk_width - 10)
                .attr("height",20);

            risk_group.select(".bar_group")
                .attr("transform",(d,i) => "translate(" + ((i * risk_width) + risk_left + 5.25)
            + "," + (margins.top + 5.25) + ")");

            const bar_x_scale = d3.scaleLinear().domain([0,1]).range([0,risk_width-10.5]);

            const bar_group =  risk_group.select(".bar_group").selectAll('.bar_risk_group')
                .data(function(d){
                    var bar_data = [],cumulative = 0;
                    var investment_data = my_data.filter(f => f["Risk Category"] === d
                        && f.Benchmark === selected_benchmark && f.Type === "Investment");
                    investment_data.forEach(function(i){
                        bar_data.push({
                            "investment": i.Category,
                            "risk": d,
                            "value": +i.Value,
                            "cumulative_value": cumulative
                        })
                        cumulative += +i.Value;
                    })
                    return bar_data;
                })
                .join(function(group){
                    var enter = group.append("g").attr("class","bar_risk_group");
                    enter.append("rect").attr("class","bar_risk_rect");
                    return enter;
                });

            bar_group.select(".bar_risk_rect")
                .attr("x", d => bar_x_scale(d.cumulative_value))
                .attr("width",d => bar_x_scale(d.value))
                .attr("height",19.5)
                .attr("fill", d => onboarding.asset_colours[d.investment])
                .on("mousemove",function(d){
                    var id_risk =  d.risk.replace(/ /g,'').toLowerCase();
                    var id_inv = d.investment.split(".").join("").replace(/ /g,'').toLowerCase();
                    d3.select(this).attr("cursor","pointer");
                    var tooltip_text = "<strong>" + selected_benchmark.toUpperCase() + "</strong><br>"
                        + "<span id='" + id_risk + "':>" + d.risk.toUpperCase() + "</span><br>"
                    + "<span id='" + id_inv + "'>" + d.investment + "</span>: " + percent_format(d.value);
                    d3.select(".tooltip")
                        .style("left",(d3.event.x + 25) + "px")
                        .style("top",d3.event.y + "px")
                        .style("visibility","visible")
                        .html(tooltip_text);

                    d3.select("#" + id_risk).style("color",onboarding.risk_colours[d.risk])
                    d3.select("#" + id_inv).style("color",onboarding.asset_colours[d.investment])
                })
                .on("mouseout",function(d){
                    d3.select(this).attr("cursor","default");
                    d3.select(".tooltip").style("visibility","hidden");
                })
        }




    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    return my;
}


function benchmark_bar() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data=[],
        bar_data=[],
        my_class="";

    function my(svg) {

        const bar_width = (Math.min((width-margins.left-margins.right)/2,350)*2) + margins.left;
        var categories = d3.set(my_data, d => d.Category).values();
        var risk_order = ["Aggressive", "Moderate Aggressive", "Moderate", "Moderate Conservative", "Conservative"];
        var my_format = d3.format(".0%");

        var nest = d3.nest()
            .key(d => d["Risk Category"])
            .entries(my_data);

        nest.forEach(function(d){
            var nest_data = {};
            nest_data["risk"] = d.key;
            categories.forEach(c => nest_data[c] = d.values.find(f => f.Category === c).Value);
            bar_data.push(nest_data)
        })

        var series = d3.stack()
            .keys(categories)
            (bar_data)
            .map(d => (d.forEach(v => v.key = d.key), d));

        var x_scale = d3.scaleBand().domain(risk_order).range([0, bar_width-margins.right]).padding(0.05);
        var y_scale = d3.scaleLinear().domain([0, 1]).range([height - margins.bottom - 20, 0]);

        if(d3.select(".x_axis" + my_class)._groups[0][0] === null) {
            svg.append("g").attr("class","text_axis x_axis" + my_class);
            svg.append("text").attr("class","benchmark_bar_title" + my_class);
        }

        d3.select(".benchmark_bar_title" + my_class)
            .attr("x",margins.left + 5)
            .attr("y",margins.top + 30)
            .attr("fill","#404040")
            .text(my_data[0].Benchmark.toUpperCase() + " asset class distribution");

        d3.select(".x_axis" + my_class)
            .call(d3.axisBottom(x_scale).tickSizeOuter(0).tickFormat(d => onboarding.risk_short_labels[d]))
            .attr("transform","translate(" + margins.left + "," + (margins.top + height - margins.bottom + 15) + ")");

        d3.selectAll(".x_axis" + my_class + " .tick text")
            .style("text-transform","uppercase")
            .attr("y",12)
            .attr("fill",d => onboarding.risk_colours[d])

        const stacked_bar_group = svg.selectAll('.stacked_bar_group')
            .data(series.flat())
            .join(function(group){
                var enter = group.append("g").attr("class","stacked_bar_group");
                enter.append("rect").attr("class","stacked_bar");
                enter.append("text").attr("class","stacked_bar_label");
                return enter;
            });


        stacked_bar_group.select(".stacked_bar")
            .attr("fill", d => onboarding.asset_colours[d.key])
            .attr("x", d => x_scale(d.data.risk))
            .attr("y", d => y_scale(d[1]))
            .attr("height", d => y_scale(d[0]) - y_scale(d[1]))
            .attr("width", x_scale.bandwidth())
            .attr("transform","translate(" + margins.left + "," + (margins.top + 40) + ")");

        stacked_bar_group.select(".stacked_bar_label")
            .attr("fill", "white")
            .attr("text-anchor","middle")
            .attr("font-size",10)
            .attr("x", d => x_scale(d.data.risk) + (x_scale.bandwidth()/2))
            .attr("y", d => y_scale(d[1]) + 12)
            .attr("visibility", d => (y_scale(d[0]) - y_scale(d[1])) > 12 ? "visible" : "hidden")
            .attr("transform","translate(" + margins.left + "," + (margins.top + 40) + ")")
            .text(d => d.key + ": " + my_format(d.data[d.key]));


    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    return my;
}



function min_max_defaults() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        show_rhs=false,
        my_class="",
        current_values={},
        my_slider_svg="";

    function my(svg) {

        onboarding.min_max_labels.forEach(function(d){
            current_values[d] = [10,25];
        })
        if(d3.select(".benchmarks_title" + my_class)._groups[0][0] === null) {
            svg.append("foreignObject").attr("class","foreignobj" + my_class)
                .append("xhtml:div").attr("class","min_max_selectdiv");
            svg.append("text").attr("class","min_max_title" + my_class);
            svg.append("text").attr("class","min_max_title_2" + my_class);
            svg.append("rect").attr("class","tax_bar min_max_mod_rect" + my_class);
            svg.append("text").attr("class","min_max_title_mod" + my_class);
            svg.append("text").attr("class","minmax_col_title min_max_inner_left" + my_class);
            svg.append("text").attr("class","minmax_col_title min_max_inner_right" + my_class);
            svg.append("text").attr("class","minmax_col_title min_max_outer_left" + my_class);
            svg.append("text").attr("class","minmax_col_title min_max_outer_right" + my_class);
        }

        d3.select(".min_max_title" + my_class)
            .attr("x",margins.left)
            .attr("y",margins.top - 30)
            .attr("fill","#404040")
            .text("Indicate default bounds for ");

        d3.select(".foreignobj" + my_class)
             .attr("width",200)
            .attr("height",25)
            .attr("x",width - 180)
            .attr("y",32);

        d3.select(".min_max_selectdiv")
            .html("<select class='min_max_set_select_box'></select>")

        d3.select(".min_max_set_select_box")
            .attr("id","min_max_set_select_box")
            .on("change",function(d){
                onboarding.current_min_max_set = this.value;
                var my_set = onboarding.min_max_data.sets[this.value];
                onboarding.min_max_labels = my_set.labels;
                onboarding.min_max_b_weight = my_set.b_weight;
                onboarding.min_max_value = my_set.value;

                draw_min_max_defaults(svg);
                draw_min_max_sliders(d3.select("." + my_slider_svg),current_values,400);
            })
            .selectAll("option")
            .data(onboarding.min_max_sets)
            .enter()
            .append("option")
            .text(d => d)

        d3.select(".min_max_set_select_box").node().value = onboarding.current_min_max_set;


        var my_title_width = 168;

        d3.select(".min_max_mod_rect" + my_class)
            .attr("x",margins.left + my_title_width + 5)
            .attr("y",margins.top - 45)
            .attr("height",20)
            .attr("width",100)
            .attr("fill",onboarding.risk_colours["Moderate"]);

        d3.select(".min_max_title_mod" + my_class)
            .attr("id","min_max_title")
            .attr("x",margins.left + 55 + my_title_width)
            .attr("y",margins.top - 30)
            .attr("fill","white")
            .attr("text-anchor","middle")
            .text("MODERATE");

        d3.select(".min_max_title_2" + my_class)
            .attr("x",margins.left + my_title_width + 110)
            .attr("y",margins.top - 30)
            .attr("fill","#404040")
            .attr("font-size",10)
            .attr("font-style","italic")
            .text("(we will ask about the other risk categories later on)");


        const col_width = (width - margins.left - margins.right)/2;

        d3.select(".min_max_inner_left" + my_class)
            .attr("fill",onboarding.min_max_colours["inner"])
            .attr("x",margins.left + 155 + 25)
            .attr("y",margins.top)
            .text("NORMAL");

        d3.select(".min_max_outer_left" + my_class)
            .attr("fill",onboarding.min_max_colours["outer"])
            .attr("x",margins.left + 155 + 50 + 35 + 25)
            .attr("y",margins.top)
            .text("MAXIMUM")

        d3.select(".min_max_inner_right" + my_class)
            .attr("fill",onboarding.min_max_colours["inner"])
            .attr("x",margins.left + 155 + 25 + col_width)
            .attr("y",margins.top)
            .text("NORMAL");

        d3.select(".min_max_outer_right" + my_class)
            .attr("fill",onboarding.min_max_colours["outer"])
            .attr("x",margins.left + 155 + 50 + 35 + 25 + col_width)
            .attr("y",margins.top)
            .text("MAXIMUM")

        const min_max_group = svg.selectAll('.min_max_group' + my_class)
            .data(onboarding.min_max_labels)
            .join(function(group){
                var enter = group.append("g").attr("class","min_max_group" + my_class);
                enter.append("text").attr("class","text_12_right group_label");
                enter.append("foreignObject").attr("class","object_lower")
                    .append("xhtml:body").append("div").attr("class","div_lower");
                enter.append("path").attr("class","minmax_triangle triangle_up_lower")
                enter.append("path").attr("class","minmax_triangle triangle_down_lower")
                enter.append("foreignObject").attr("class","object_upper")
                    .append("xhtml:body").append("div").attr("class","div_upper");
                enter.append("path").attr("class","minmax_triangle triangle_up_upper")
                enter.append("path").attr("class","minmax_triangle triangle_down_upper").attr("id","upper_down");
                enter.append("text").attr("class","group_remove");
                return enter;
            });

        min_max_group.select(".group_label")
            .attr("fill",d => onboarding.asset_colours[d])
            .attr("x", (d,i) => ((i % 2) * col_width) + 145)
            .attr("y", (d,i) => parseInt(i / 2) * 25)
            .text(d => d)
            .attr("transform","translate(" + margins.left + "," + (margins.top + 20) + ")")

        min_max_group.select(".triangle_up_lower")
            .attr("id",(d,i) => "lower_up_" + i)
            .attr("d","M0 00 L 10 0 L 5 -5 Z")
            .attr("stroke",onboarding.min_max_colours["inner"])
            .attr("fill",onboarding.min_max_colours["inner"])
            .attr("transform",(d,i) => "translate(" + (margins.left + ((i % 2) * col_width) + 215)
                + "," + (margins.top + 14 + (parseInt(i / 2) * 25)) + ")");

        min_max_group.select(".triangle_down_lower")
            .attr("id",(d,i) => "lower_down_" + i)
            .attr("d","M0 00 L 10 0 L 5 5 Z")
            .attr("stroke",onboarding.min_max_colours["inner"])
            .attr("fill",onboarding.min_max_colours["inner"])
            .attr("transform",(d,i) => "translate(" + (margins.left + ((i % 2) * col_width) + 215)
                + "," + (margins.top + 17 + (parseInt(i / 2) * 25)) + ")");

        min_max_group.select(".object_lower")
            .attr("width",60)
            .attr("height",25)
            .attr("x",(d,i) => ((i % 2) * col_width) + 150)
            .attr("y", (d,i) => parseInt(i / 2) * 25)
            .attr("transform","translate(" + margins.left + "," + (margins.top + 5) + ")")

        min_max_group.select(".div_lower")
            .html((d,i) => "<input type='text' class='minmax_input' id='minmax_lower_" + i + "' name='minmax_lower' placeholder='10'>")

        min_max_group.select(".object_upper")
            .attr("width",60)
            .attr("height",25)
            .attr("x",(d,i) => ((i % 2) * col_width) + 235)
            .attr("y", (d,i) => parseInt(i / 2) * 25)
            .attr("transform","translate(" + margins.left + "," + (margins.top + 5) + ")")

        min_max_group.select(".div_upper")
            .html((d,i) => "<input type='text' class='minmax_input' id='minmax_upper_" + i + "' name='minmax_upper' placeholder='25'>")

        min_max_group.select(".triangle_up_upper")
            .attr("id",(d,i) => "upper_up_" + i)
            .attr("d","M0 00 L 10 0 L 5 -5 Z")
            .attr("stroke",onboarding.min_max_colours["outer"])
            .attr("fill",onboarding.min_max_colours["outer"])
            .attr("transform",(d,i) => "translate(" + (margins.left + ((i % 2) * col_width) + 300)
                + "," + (margins.top + 14 + (parseInt(i / 2) * 25)) + ")");

        min_max_group.select(".triangle_down_upper")
            .attr("id",(d,i) => "upper_down_" + i)
            .attr("d","M0 00 L 10 0 L 5 5 Z")
            .attr("stroke",onboarding.min_max_colours["outer"])
            .attr("fill",onboarding.min_max_colours["outer"])
            .attr("transform",(d,i) => "translate(" + (margins.left + ((i % 2) * col_width) + 300)
                + "," + (margins.top + 17 + (parseInt(i / 2) * 25)) + ")")

        d3.selectAll(".minmax_input")
            .on("change",function(d){
                if(onboarding.progress_position < 8){
                    onboarding.progress_position = 8;
                    if(onboarding.progress_chart !== ""){
                        onboarding.progress_chart.reset_progress();
                    }
                }
                show_rhs = true;
                var my_type = this.id.split("_")[1];
                var my_id = this.id.split("_")[2];
                if(isNaN(this.value) === false){
                    current_values[onboarding.min_max_labels[my_id]][my_type === "lower" ? 0 : 1] = +this.value;
                } else {
                    this.value = current_values[onboarding.min_max_labels[my_id]][my_type === "lower" ? 0 : 1];
                }
                draw_min_max_sliders(d3.select("." + my_slider_svg),current_values,400)

            })
        d3.selectAll(".minmax_triangle")
            .on("mouseover",function(d){d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(d){d3.select(this).attr("cursor","default")})
            .on("click",function(d){
                if(onboarding.progress_position < 8){
                    onboarding.progress_position = 8;
                    if(onboarding.progress_chart !== ""){
                        onboarding.progress_chart.reset_progress();
                    }
                }
                show_rhs = true;
                var my_type = this.id.split("_")[0];
                var my_direction = this.id.split("_")[1];
                var my_index = this.id.split("_")[2];
                var my_val = current_values[d][my_type === "lower" ? 0 : 1];
                if(my_val === ""){
                    my_val = (my_type === "lower" ? 10 : 25);
                }
                if(my_direction === "up"){
                    my_val += 1;
                } else {
                    my_val -= 1;
                }
                current_values[d][my_type === "lower" ? 0 : 1] = my_val;
                d3.select("#minmax_" + my_type + "_" + my_index).node().value = my_val;
                draw_min_max_sliders(d3.select("." + my_slider_svg),current_values,400)

            })


    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_slider_svg = function(value) {
        if (!arguments.length) return my_slider_svg;
        my_slider_svg = value;
        return my;
    };

    return my;
}


function min_max_sliders() {
    //REUSABLE blank chart

    var width=0,
        height=0,
        margins={},
        my_data=[],
        my_title="",
        my_class="",
        col_count = 1;

    function my(svg) {

        const col_width = (width - margins.right - margins.left)/col_count;
        const x_scale = d3.scaleLinear().domain([0,100]).range([0,col_width-20]);
        const percent_format = d3.format(".0%");

        if(d3.select(".minmax_slider_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","minmax_slider_title" + my_class);
        }

        d3.select(".minmax_slider_title" + my_class)
            .attr("x",margins.left + 30 )
            .attr("y",margins.top - 20)
            .attr("fill","#404040")
            .text(my_title);

        const min_max_slider_group = svg.selectAll('.min_max_slider_group')
            .data(onboarding.min_max_labels, d => my_data)
            .join(function(group){
                var enter = group.append("g").attr("class","min_max_slider_group");
                enter.append("text").attr("class","text_9_left slider_title");
                enter.append("rect").attr("class","slider_background_rect");
                enter.append("rect").attr("class","slider_rect_lower");
                enter.append("rect").attr("class","slider_rect_upper");
                enter.append("rect").attr("class","slider_rect_lower_mid");
                enter.append("rect").attr("class","slider_rect_upper_mid");
                enter.append("line").attr("class","slider_background_line");
                enter.append("line").attr("class","slider_benchmark_line");
                enter.append("line").attr("class","slider_value_line");
                enter.append("rect").attr("class","slider_value_rect");
                enter.append("rect").attr("class","slider_benchmark_rect");
                enter.append("text").attr("class","slider_value_label");
                enter.append("text").attr("class","slider_benchmark_label");
                enter.append("g").attr("class","slider_positions");
                return enter;
            });

        var min_max_height = 70;

        min_max_slider_group
            .attr("transform", (d,i) => "translate(" + (((i % col_count) * col_width) + margins.left + 30)
                + "," + ((parseInt(i/col_count) * min_max_height) + margins.top) + ")")

        min_max_slider_group.select(".slider_title")
            .style("font-size",12)
            .style("font-weight","bold")
            .attr("fill",d => onboarding.asset_colours[d])
            .attr("y",42)
            .text(d => d);

        min_max_slider_group.select(".slider_background_rect")
            .attr("id",(d,i) => "slider_background_rect" + i)
            .attr("height",30)
            .attr("fill-opacity",0.1)
            .attr("fill","#1FC932");

        min_max_slider_group.select(".slider_background_line")
            .attr("x2", col_width - 20)
            .attr("y1", 29)
            .attr("y2", 29)
            .attr("stroke","#333333")
            .attr("stroke-width",1);

        min_max_slider_group.select(".slider_value_line")
            .attr("visibility",d => onboarding.min_max_value[d] === null ? "hidden" : "visible")
            .attr("x1", d => x_scale(onboarding.min_max_value[d]))
            .attr("x2", d => x_scale(onboarding.min_max_value[d]))
            .attr("y2",  29)
            .attr("stroke","#A0A0A0")
            .attr("stroke-width",5)
        .on("mousemove",function(d){
            var tooltip_text = "Value: " + percent_format(onboarding.min_max_value[d]/100);
            d3.select(".tooltip")
                .style("visibility","visible")
                .style("left",(d3.event.pageX+10) + "px")
                .style("top",d3.event.pageY + "px")
                .html(tooltip_text);
        })
            .on("mouseout",function(){
                d3.select(".tooltip").style("visibility","hidden");
            })


        min_max_slider_group.select(".slider_benchmark_line")
            .attr("x1", d => x_scale(onboarding.min_max_b_weight[d]))
            .attr("x2", d => x_scale(onboarding.min_max_b_weight[d]))
            .attr("y2",  29)
            .attr("stroke","#333333")
            .attr("stroke-width",10)
            .on("mousemove",function(d){
                var tooltip_text = "Benchmark Weight: " + percent_format(onboarding.min_max_value[d]/100);
                d3.select(".tooltip")
                    .style("visibility","visible")
                    .style("left",(d3.event.pageX+10) + "px")
                    .style("top",d3.event.pageY + "px")
                    .html(tooltip_text);
            })
            .on("mouseout",function(){
                d3.select(".tooltip").style("visibility","hidden");
            })


        min_max_slider_group.select(".slider_value_rect")
            .attr("height",15)
            .attr("width",70)
            .attr("x",margins.left + x_scale.range()[1] - 75)
            .attr("y",32)
            .attr("fill","#333333")

        min_max_slider_group.select(".slider_value_label")
            .attr("x",margins.left + x_scale.range()[1] - 40)
            .attr("text-anchor","middle")
            .attr("y",43)
            .attr("fill","white")
            .attr("font-size","11")
            .text(d => "value: " + percent_format(onboarding.min_max_value[d]/100) );

        min_max_slider_group.select(".slider_benchmark_rect")
            .attr("height",15)
            .attr("width",90)
            .attr("x",margins.left + x_scale.range()[1] - 165)
            .attr("y",32)
            .attr("fill","#707070");


        min_max_slider_group.select(".slider_benchmark_label")
            .attr("x",margins.left + x_scale.range()[1] - 120)
            .attr("text-anchor","middle")
            .attr("y",43)
            .attr("fill","white")
            .attr("font-size","11")
            .text(d => "benchmark: " + percent_format(onboarding.min_max_b_weight[d]/100) );

        min_max_slider_group.select(".slider_rect_lower")
            .attr("id",(d,i) => "slider_rect_lower" + i)
            .attr("fill","#F82B61")
            .attr("fill-opacity",0.1)
            .attr("height",30);

        min_max_slider_group.select(".slider_rect_lower_mid")
            .attr("id",(d,i) => "slider_rect_lower_mid" + i)
            .attr("fill","#FBB401")
            .attr("fill-opacity",0.1)
            .attr("height",30);

        min_max_slider_group.select(".slider_rect_upper_mid")
            .attr("id",(d,i) => "slider_rect_upper_mid" + i)
            .attr("fill","#FBB401")
            .attr("fill-opacity",0.1)
            .attr("height",30);

        min_max_slider_group.select(".slider_rect_upper")
            .attr("id",(d,i) => "slider_rect_upper" + i)
            .attr("fill","#F82B61")
            .attr("fill-opacity",0.1)
            .attr("height",30);


        const bound_labels = {"outer_minus":"Absolute Minimum","outer_plus":"Absolute Maximum",
            "inner_minus":"Normal Minimum","inner_plus":"Normal Maximum"}
        const min_max_slider_positions_group = min_max_slider_group.select(".slider_positions").selectAll('.min_max_slider_group')
            .data(function(d,i){
                var types = ["outer_minus","outer_plus","inner_minus","inner_plus"];
                var group_data = [];
                var my_limits = my_data[d];
                var my_value = onboarding.min_max_value[d];
                types.forEach(function(t){
                    var my_type = t.split("_");
                    var type_val = my_value;
                    if(my_type[1] === "minus"){
                        type_val -= my_limits[my_type[0] === "inner" ? 0 : 1]
                    } else {
                        type_val += my_limits[my_type[0] === "inner" ? 0 : 1]
                    }
                    if(type_val < 0 ){type_val = 0};
                    if(type_val > 100){type_val = 100};
                    group_data.push({
                        "fill":onboarding.min_max_colours[my_type[0]],
                        "value":type_val,
                        "bound": my_type[0],
                        "type":my_type[1]
                    })
                });

                var lower_limit = d3.min(group_data, m => m.value);
                if(lower_limit < 0){lower_limit = 0};

                d3.select("#slider_rect_lower" + i)
                        .attr("width",x_scale(lower_limit));

                var upper_limit = d3.max(group_data, m => m.value);
                if(upper_limit > 100){upper_limit = 100;}

                d3.select("#slider_rect_upper" + i)
                    .attr("x",x_scale(upper_limit))
                    .attr("width",x_scale(100 - upper_limit));

                var lower_mid_limit = group_data.find(f => f.bound === "inner" && f.type === "minus");
                if(lower_mid_limit < 0){lower_mid_limit = 0};

                d3.select("#slider_rect_lower_mid" + i)
                    .attr("x",x_scale(lower_limit))
                    .attr("width",x_scale(lower_mid_limit.value - lower_limit));
                lower_limit = lower_mid_limit.value;

                var upper_mid_limit = group_data.find(f => f.bound === "inner" && f.type === "plus");
                if(upper_mid_limit > 100){upper_mid_limit = 100;}

                d3.select("#slider_rect_upper_mid" + i)
                    .attr("x",x_scale(upper_mid_limit.value))
                    .attr("width",x_scale(upper_limit - upper_mid_limit.value));
                upper_limit = upper_mid_limit.value;

                d3.select("#slider_background_rect" + i)
                    .attr("x",x_scale(lower_limit > 0 ? lower_limit : 0))
                    .attr("width",x_scale(upper_limit - (lower_limit > 0 ? lower_limit : 0)))
                return group_data
            })
            .join(function(group){
                var enter = group.append("g").attr("class","min_max_slider_group");
                enter.append("line").attr("class","slider_position_line");
                enter.append("line").attr("class","slider_tooltip_item slider_position_line_tooltip");
                enter.append("circle").attr("class","slider_tooltip_item slider_position_circle");
                enter.append("text").attr("class","slider_position_text");

                return enter;
            });

        min_max_slider_positions_group.select(".slider_position_circle")
            .attr("cx", d => x_scale(d.value))
            .attr("stroke",d => d.fill)
            .attr("r",8.5)
            .attr("fill","white")
            .attr("stroke-width",1)
            .attr("cursor","pointer");

        min_max_slider_positions_group.select(".slider_position_text")
            .attr("pointer-events","none")
            .attr("x", d => x_scale(d.value))
            .attr("fill","#333333")
            .attr("text-anchor","middle")
            .attr("font-size","7")
            .attr("dy",2.5)
            .text(d => percent_format(d.value/100))


        min_max_slider_positions_group.select(".slider_position_line")
            .attr("x1", d => x_scale(d.value))
            .attr("x2", d => x_scale(d.value))
            .attr("y2",30)
            .attr("stroke","#333333")
            .attr("stroke-width",1);

        min_max_slider_positions_group.select(".slider_position_line_tooltip")
            .attr("x1", d => x_scale(d.value))
            .attr("x2", d => x_scale(d.value))
            .attr("y2",30)
            .attr("stroke","transparent")
            .attr("stroke-width",5);

        d3.selectAll(".slider_tooltip_item")
            .on("mousemove",function(d){
                var tooltip_text = bound_labels[d.bound + "_" + d.type] + ": " + percent_format(d.value/100);
                d3.select(".tooltip")
                    .style("visibility","visible")
                    .style("left",(d3.event.pageX+10) + "px")
                    .style("top",d3.event.pageY + "px")
                    .html(tooltip_text);
            })
            .on("mouseout",function(){
                d3.select(".tooltip").style("visibility","hidden");
            });


    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.my_title = function(value) {
        if (!arguments.length) return my_title;
        my_title = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    return my;
}


function dial_chart() {
    //REUSABLE dial chart

    var width=0,
        height=0,
        my_data = [],
        changes = {},
        my_class="",
        left_box_width= 140,
        margins = 40,
        tick_height = 20,
        view_mode = "edit";


    function my(svg) {

        if(d3.select(".foreignobj" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","dial_chart_title" + my_class);
            svg.append("foreignObject").attr("class","foreignobj" + my_class)
                .append("xhtml:div").attr("class","goals_selectdiv");
            svg.append("text").attr("class","starting_points" + my_class);
            svg.append("rect").attr("class","download_item" + my_class + " download_rect download_rect" + my_class)
            svg.append("text").attr("class","download_item" + my_class + " fa download_icon" + my_class)
            svg.append("rect").attr("class","download_item" + my_class + " download_rect save_rect" + my_class)
            svg.append("text").attr("class","download_item" + my_class + " fa save_icon" + my_class)
            svg.append("rect").attr("class","download_item" + my_class + " download_rect cancel_rect" + my_class)
            svg.append("text").attr("class","download_item" + my_class + " fas cancel_icon" + my_class)
        }

        d3.select(".dial_chart_title" + my_class)
            .attr("x",15)
            .attr("y",45)
            .attr("fill","#404040")
            .text(view_mode === "edit"? "Specify portfolio goals by moving the panels and the triangles" : "Portfolio Goals");

        d3.select(".foreignobj" + my_class)
            .attr("visibility",view_mode === "edit" ? "visible" : "hidden")
            .attr("width",200)
            .attr("height",25)
            .attr("x",width - 180)
            .attr("y",32);

        d3.select(".goals_selectdiv")
            .html("<select class='goals_set_select_box'></select>")

        d3.select(".goals_set_select_box")
            .attr("id","goals_select_box")
            .on("change",function(d){
                if(this.value !== ""){
                    onboarding.goals_data.positions = {"current": JSON.parse(JSON.stringify(onboarding.goals_data.sets[this.value].positions))};
//                    onboarding.goals_data.allocations = JSON.parse(JSON.stringify(onboarding.goals_data.sets[this.value].allocations));
                    var top_index = onboarding.goals_data.positions.current.findIndex(d => d.position === 0);
                    var changes = add_goals_restrictions(top_index,false,onboarding.goals_data.positions.current[top_index].id);
                    onboarding.current_goal_start = this.value;
                    onboarding.goals_data.allocations = {"strategic":strategic_potential(),"fund":fund_potential(),"tactical":tactical_potential()};                    
                    draw_goals_and_trade_offs(svg,my_class,changes,1000)
                } else {
                    this.value = onboarding.current_goal_start
                }
            })
            .selectAll("option")
            .data(onboarding.goals_data.all_sets)
            .enter()
            .append("option")
            .text(d => d)

        d3.select(".goals_set_select_box").node().value = onboarding.current_goal_start;

        d3.select(".starting_points" + my_class)
            .attr("visibility",view_mode === "edit" ? "visible" : "hidden")
            .attr("x",width - 158)
            .attr("y",46)
            .attr("text-anchor","end")
            .text("Templates");

        d3.select(".save_rect" + my_class)
            .attr("visibility", "hidden")
            .attr("cursor","pointer")
            .attr("fill","#F0F0F0")
            .attr("stroke","#A0A0A0")
            .attr("x",width - 255)
            .attr("y",31)
            .attr("height",20)
            .attr('width',20)
            .on("click",function(){save_button_click();});

        d3.select(".save_icon" + my_class)
            .attr("visibility", "hidden")
            .attr("pointer-events","none")
            .attr("text-anchor","middle")
            .attr("fill","#A0A0A0")
            .attr("font-size",12)
            .attr("x",width - 245)
            .attr("y",45)
            .text("\uf0c7");

        d3.select(".cancel_rect" + my_class)
            .attr("visibility", "hidden")
            .attr("cursor","pointer")
            .attr("fill","#F0F0F0")
            .attr("stroke","#A0A0A0")
            .attr("x",width - 280)
            .attr("y",31)
            .attr("height",20)
            .attr('width',20)
            .on("click",function(){cancel_button_click();});;

        d3.select(".cancel_icon" + my_class)
            .attr("visibility", "hidden")
            .attr("pointer-events","none")
            .attr("text-anchor","middle")
            .attr("fill","#A0A0A0")
            .attr("font-size",12)
            .attr("x",width - 270)
            .attr("y",45)
            .text("\uf715");


        d3.select(".download_rect" + my_class)
            .attr("visibility",onboarding.current_goal_start === "" ? "visible" : "hidden")
            .attr("fill","#F0F0F0")
            .attr("stroke","#A0A0A0")
            .attr("x",width - 10)
            .attr("y",31)
            .attr("height",20)
            .attr('width',20)
            .on("mouseover",function(d){
                d3.select(this).attr("cursor","pointer").attr("fill","white");
            })
            .on("mouseout",function(d){
                d3.select(this).attr("cursor","default").attr("fill","#F0F0F0");
            })
            .on("click",function(d){
                var element = document.createElement('a');
                var download_data = {"positions": onboarding.goals_data.positions,
            "allocations":onboarding.goals_data.allocations};

                element.setAttribute('href', 'data:text/plain;charset=utf-8,'
                    + encodeURIComponent(JSON.stringify(download_data)));
                element.setAttribute('download', "my_filename");

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            })

        d3.select(".download_icon" + my_class)
            .attr("pointer-events","none")
            .attr("visibility",onboarding.current_goal_start === "" ? "visible" : "hidden")
            .attr("text-anchor","middle")
            .attr("fill","#A0A0A0")
            .attr("font-size",12)
            .attr("x",width)
            .attr("y",45)
            .text(view_mode === "edit" ? "\uf0c7" : "\uf019");

        //reset group order so raise always works..
        for (var i = 0; i < my_data.length; i++) {
            d3.selectAll("#bar_group_" + i + my_class).raise();
        }
        //get bar range, calculate step and set y scale
        var bars = d3.range(my_data.length-1);
        var step = height/(bars.length+1);
        var axis_width = width - left_box_width - (margins*2);
        var y_scale = d3.scaleOrdinal().domain(bars).range(d3.range(0,height,step));
        var x_scale = d3.scaleLinear().domain([0,10]).range([0,axis_width]);
        var triangle = d3.symbol().type(d3.symbolTriangle).size(200);
        var triangle_bigger = d3.symbol().type(d3.symbolTriangle).size(600);

        //define group
        var my_group = svg.selectAll(".bar_group" + my_class).data(my_data);

        //exit remove
        my_group.exit().remove();
        //enter (and add group id and transform)
        var enter = my_group.enter().append("g").attr("class","bar_group" + my_class)
            .attr("id",d => "bar_group_" + d.id + my_class).attr("transform","translate(4,4)");
        //append group
        var items_group = enter.append("g").attr("class","bar_items").attr("pointer-events","none");

        //append items to group
        items_group.append("rect").attr("class","factor_bar");
        items_group.append("rect").attr("class","position_rect");
        items_group.append("text").attr("class","position_text");
        items_group.append("text").attr("class","position_label");
        items_group.append("text").attr("class","factor_title");
        items_group.append("rect").attr("class","restriction_rect_left");
        items_group.append("rect").attr("class","restriction_rect_right");
        items_group.append("g").attr("class","x_axis");
        items_group.append("path").attr("class","triangle_marker");

        //append invisible drag/drop bar
        enter.append("rect").attr("class","invisible_bar");
        enter.append("path").attr("class","invisible_triangle_marker");

        //merge
        my_group = my_group.merge(enter);
        //add properties to group items

        my_group.attr("transform","translate(15,60)")

        my_group.select(".triangle_marker")
            .attr("id",d => "triangle_" + d.id)
            .attr("d", triangle)
            .attr("transform",d => "translate(" + (left_box_width + margins + x_scale(d.value)) + "," + ((step*0.5)+15) + ")")


        my_group.select(".invisible_triangle_marker")
            .attr("d", triangle_bigger)
            .attr("transform",d => "translate(" + (left_box_width + margins + x_scale(d.value)) + "," + ((step*0.5)+15 + y_scale(d.position)) + ")")
            .call(d3.drag()
                .on("start", triangle_started)
                .on("drag", triangle_drag)
                .on("end", triangle_dragended))


        my_group.select(".x_axis")
            .call(d3.axisBottom(x_scale).tickFormat(show_ticks).tickSizeOuter(0))
            .attr("transform","translate(" + (left_box_width + margins) + "," + (step*0.5) + ")");

        function show_ticks(d){
            var labels = onboarding.goals_data.factor_labels[this.parentNode.parentNode.__data__.id];
            if(d === 0){
                return labels[0].toUpperCase()
            } else if (d === 5){
                return labels[1].toUpperCase()
            } else if (d === 10){
                return labels[2].toUpperCase()
            } else {
                return ""
            }
        }
        my_group.selectAll(".x_axis .tick line")
            .attr("y2",tick_height)

        my_group.selectAll(".x_axis .tick text")
            .attr("font-size","1.2em")
            .attr("dy",-tick_height+4)
            .attr("text-anchor",d => d === 0 ? "start" : (d === 5 ? "middle" : "end"))

        my_group.select(".restriction_rect_left")
            .attr("width",d => x_scale(d.restrict_left))
            .attr("height",tick_height)
            .style("fill", onboarding.texture.url())
            .attr("transform","translate(" + (left_box_width + margins) + "," + (step*0.5) + ")");

        my_group.select(".restriction_rect_right")
            .attr("width",d => x_scale(10) - (x_scale(d.restrict_right)))
            .attr("height",tick_height)
            .style("fill", onboarding.texture.url())
            .attr("transform",d => "translate(" + (left_box_width + margins + (x_scale(d.restrict_right))) + "," + (step*0.5) + ")");

        my_group.select(".position_rect")
            .attr("x",2)
            .attr("y",2)
            .attr("width",left_box_width)
            .attr("height",step-12)
            .attr("fill","#F0F0F0")

        my_group.select(".position_text")
            .attr("x",left_box_width/2)
            .attr("dy","2em")
            .attr("font-size","2.5em")
            .attr("text-anchor","middle")
            .text(d => d.position+1);

        my_group.select(".position_label")
            .attr("fill","#33a02c")
            .attr("x",left_box_width/2)
            .attr("dy","1.2em")
            .attr("font-size","2em")
            .attr("text-anchor","middle")
            .text("GOAL")

        my_group.select(".factor_title")
            .attr("fill","#1f78b4")
            .attr("x",left_box_width + ((width-left_box_width)/2))
            .attr("dy","1.2em")
            .attr("font-size","1.5em")
            .attr("text-anchor","middle")
            .text(d => onboarding.goals_data.factor_names[d.id].toUpperCase());

        my_group.select(".factor_bar")
            .attr("id",d => "fbar_" + d.id)
            .attr("width",width-8)
            .attr("stroke-width","4px")
            .attr("stroke","#A0A0A0")
            .attr("fill","white")
            .attr("height",step-8)

        //translate them
        my_group.select(".bar_items")
            .attr("id",d => "bar_item_" + d.id)
            .attr("transform",d => "translate(0," + y_scale(d.position) + ")")

        //add properties and drag action to invisible bar
        my_group.select(".invisible_bar")
            .attr("width",width)
            .attr("stroke-width","0")
            .attr("stroke","none")
            .attr("fill","transparent")
            .attr("height",step)
            .attr("y", d => y_scale(d.position))
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended));

            onboarding.goals_data.positions.current.forEach(function(d){
                if(changes.restrict_left[d.id] !== undefined){
                    d.restrict_left = changes.restrict_left[d.id].value;
                }
                if(changes.restrict_right[d.id] !== undefined){
                    d.restrict_right = changes.restrict_right[d.id].value;
                }

                if(changes.value[d.id] !== undefined){
                    d.value = changes.value[d.id].value;
                }
            })
            if(Object.keys(changes.restrict_left).length > 0){
                my_group.select(".restriction_rect_left")
                    .transition()
                    .delay(d => get_delay(d.restrict_left,"restrict_left",d.id))
                    .duration(1000)
                    .style("fill", d => d.restrict_left !== undefined ? onboarding.texture_gold.url(): onboarding.texture.url())
                    .attr("width",d => d.restrict_left !== undefined ? x_scale(d.restrict_left):  0)
                    .transition()
                    .duration(1000)
                    .style("fill",onboarding.texture.url())
            }
            if(Object.keys(changes.restrict_right).length > 0){
                my_group.select(".restriction_rect_right")
                    .transition()
                    .delay(d => get_delay(d.restrict_right,"restrict_right",d.id))
                    .duration(1000)
                    .style("fill", d => d.restrict_right !== undefined ? onboarding.texture_gold.url(): onboarding.texture.url())
                    .attr("width",d => d.restrict_right !== undefined ? x_scale(10) - x_scale(d.restrict_right):  x_scale(10) - x_scale(0))
                    .attr("transform", d => d.restrict_right !== undefined ? "translate(" + (left_box_width + margins + (x_scale(d.restrict_right))) + "," + (step*0.5) + ")":  "translate(" + (left_box_width + margins + (x_scale(0))) + "," + (step*0.5) + ")")
                    .transition()
                    .duration(1000)
                    .style("fill",onboarding.texture.url())
            }
            if(Object.keys(changes.value).length > 0){
                my_group.select(".triangle_marker")
                    .transition()
                    .delay(d => get_delay(d.value,"value",d.id))
                    .duration(1000)
                    .style("fill",d => d.value !== undefined ? "gold": "white")
                    .attr("transform", d => get_triangle_transform(d.value,0))
                    .transition()
                    .duration(1000)
                    .style("fill", "white");

                my_group.select(".invisible_triangle_marker")
                    .transition()
                    .delay(d => get_delay(d.value,"value",d.id))
                    .duration(1000)
                    .style("fill",d => d.value !== undefined ? "gold": "transparent")
                    .attr("transform", d => get_triangle_transform(d.value, y_scale(d.position)))
                    .transition()
                    .duration(1000)
                    .style("fill","transparent")
            }

            function get_triangle_transform(new_x_value,extra){
                return "translate(" + (left_box_width+margins + x_scale(new_x_value)) + "," + (((step*0.5)+15) + extra) + ")";
            }

            function get_delay(my_restrict,my_type,my_id){

                return (my_restrict === undefined || changes[my_type][my_id] === undefined) ? 0 : (changes[my_type][my_id].order * 1000);
            }

        function triangle_started(d) {
                if(view_mode === "edit"){
                    d3.selectAll("#triangle_" + d.id).style("fill","gold");
                }
        }


        function triangle_drag(d){
            if(view_mode === "edit"){
                d3.selectAll("#triangle_" + d.id).attr("transform","translate(" + d3.event.x + "," + ((step*0.5)+15) + ")");
                d3.select(this).attr("transform","translate(" + d3.event.x + "," + ((step*0.5)+15 + y_scale(d.position)) + ")");
            }
        }

        function triangle_dragended(d){
            if(view_mode === "edit") {
                //get the right point in the range
                var new_value = Math.round(x_scale.invert(d3.event.x - left_box_width - margins)).toFixed(0);
                if (new_value < d.restrict_left) {
                    new_value = d.restrict_left;
                } else if (new_value > d.restrict_right) {
                    new_value = d.restrict_right;
                }
                d3.selectAll("#triangle_" + d.id)
                    .transition()
                    .duration(1000)
                    .style("fill", "white")
                    .attr("transform", "translate(" + (left_box_width + margins + x_scale(new_value)) + "," + ((step * 0.5) + 15) + ")");

                d3.select(this)
                    .transition()
                    .duration(1000)
                    .attr("transform", "translate(" + (left_box_width + margins + x_scale(new_value)) + "," + ((step * 0.5) + 15 + y_scale(d.position)) + ")");

                onboarding.current_goal_start = "";

                var t = d3.timer(function (elapsed) {
                    if (elapsed > 0) {
                        if (d.value !== new_value) {
                            change_goals_data(["value", onboarding.goals_data.factor_names[d.id], d.id, d.value, new_value])
                        }
                        t.stop();
                    }
                }, 1000);
            };
        }


        function dragstarted(d) {
            if(view_mode === "edit") {
                //bring group to the front
                d3.selectAll("#bar_group_" + d.id + my_class).raise();
                d3.selectAll("#fbar_" + d.id).attr("fill", "gold");
            };
        }

        function dragged(d) {
            if(view_mode === "edit") {
                //change the y position (both invisible bar and group underneath)
                d3.select(this).attr("y", d3.event.y)
                d3.selectAll("#bar_item_" + d.id)
                    .attr("transform", d => "translate(0," + d3.event.y + ")");
            };
        }

        function dragended(d) {
            if(view_mode === "edit") {
                //get the right point in the range
                for (r in y_scale.range()) {
                    if (y_scale.range()[r] > d3.event.y) {
                        if (y_scale(d.position) > d3.event.y) {
                            var new_position = +r;
                        } else {
                            var new_position = +r - 1;
                        }

                        break;
                    }
                    ;
                }
                //reset if out of range
                if (new_position === undefined) {
                    new_position = y_scale.domain()[y_scale.domain().length - 1];
                }
                ;
                //store id and old position
                var my_id = d.id;
                var old_position = d.position;

                //now change positions
                d3.selectAll(".invisible_bar").each(function (d) {

                    if (d.id === my_id) {
                        d.position = new_position;
                    } else if (d.position >= new_position && d.position < old_position && old_position > new_position) {
                        d.position += 1
                    } else if (d.position <= new_position && d.position >= old_position) {
                        d.position -= 1
                    }
                    d3.selectAll("#bar_item_" + d.id)
                        .attr("transform", d => "translate(0," + y_scale(d.position) + ")");
                })
                    .transition()
                    .duration(1000)
                    .attr("y", d => y_scale(d.position))

                //reset position text values
                d3.selectAll(".position_text")
                    .transition()
                    .duration(1000)
                    .text(d => d.position + 1)

                //fade background back to normal
                d3.selectAll("#fbar_" + d.id).transition().duration(1000).attr("fill", "white");
                onboarding.current_goal_start = "";
                var t = d3.timer(function (elapsed) {
                    if (elapsed > 0) {
                        if (new_position !== old_position) {
                            change_goals_data(["position", onboarding.goals_data.factor_names[d.id], d.id, old_position, new_position])
                        }
                        t.stop();
                    }
                }, 1000);
            }

        }

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.changes = function(value) {
        if (!arguments.length) return changes;
        changes = value;
        return my;
    };


    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };

    my.view_mode = function(value) {
        if (!arguments.length) return view_mode;
        view_mode = value;
        return my;
    };



    return my;
}


function goals_chart_results() {

    var width=0,
        height=0,
        margins={},
        my_class="",
        transition_time = 0,
        show_goals = true,
        text_width = 300;


    function my(svg) {

        var allocations = Object.keys(onboarding.goal_allocation_max_vals);
        allocations.push("total return");

        var percent_format = d3.format(".0%");
        var triangle = d3.symbol().type(d3.symbolTriangle).size(150);

        if(d3.select(".goals_results_title" + my_class)._groups[0][0] === null) {
            svg.append("text").attr("class","goals_results_title" + my_class);
            svg.append("text").attr("class","goals_impact_title" + my_class);
        }

        var title_height = 40;
        if(show_goals === false){title_height = 0};
        var bar_total = (show_goals === true ? onboarding.goals_data.positions.current.length : 0) + allocations.length;
        var positions_height = (height - ((show_goals === true ? margins.top : 10) + (title_height*2) + margins.bottom))/bar_total;

        var impact_y = margins.top + (positions_height * onboarding.goals_data.positions.current.length) + (title_height * 2);

        if(show_goals === false){
            impact_y = margins.top + title_height + 20;
        } else {
            d3.select(".goals_results_title" + my_class)
                .attr("x",margins.left)
                .attr("y",margins.top +20)
                .attr("fill","#404040")
                .text("Your Goals");

            d3.select(".goals_impact_title" + my_class)
                .attr("x",margins.left)
                .attr("y",impact_y + 12)
                .attr("fill","#404040")
                .text("Return Potential");
        }
        if(show_goals === true){

            onboarding.goals_data.positions.current.sort((a,b) => d3.ascending(a.position,b.position));

            var triangle_x_scale = d3.scaleLinear().domain([0,10]).range([0,width - margins.left - margins.right - text_width]);

            const positions_group = svg.selectAll('.positions_group' + my_class)
                .data(onboarding.goals_data.positions.current, d => d.id)
                .join(function(group){  var triangle = d3.symbol().type(d3.symbolTriangle).size(200);
                    var enter = group.append("g").attr("class","positions_group" + my_class);
                    enter.append("rect").attr("class","positions_rect");
                    enter.append("text").attr("class","positions_no");
                    enter.append("text").attr("class","positions_description");
                    enter.append("g").attr("class","positions_value_axis");
                    enter.append("path").attr("class","triangle_marker positions_value_triangle")

                    return enter;
                });

            positions_group.select(".positions_value_axis")
                .attr("transform",(d,i) => "translate(" + (width - margins.right - triangle_x_scale.range()[1] - 20) + "," +
                    (margins.top + title_height +  (i * (positions_height)) + ((positions_height-4)/2)) + ")")
                .call(d3.axisBottom(triangle_x_scale).tickFormat("").tickSizeOuter(0));

            positions_group.select(".positions_value_triangle")
                .style("stroke-width",1)
                .attr("d",triangle)
                .transition()
                .duration(transition_time)
                .attr("transform",(d,i) => "translate(" + (width - margins.right - triangle_x_scale.range()[1] - 20 + (triangle_x_scale(d.value))) + "," +
                    (margins.top + title_height + (i * (positions_height)) + ((positions_height-4)/2) + 2) + ")")


            d3.selectAll(".positions_value_axis .tick line")
                .attr("y1",-3)
                .attr("y2",3);

            positions_group.select(".positions_rect")
                .transition()
                .duration(transition_time)
                .attr("x",margins.left)
                .attr("y",(d,i) => margins.top + title_height + (i * positions_height))
                .attr("width",width - margins.left - margins.right)
                .attr("height",positions_height-4)

            positions_group.select(".positions_no")
                .transition()
                .duration(transition_time)
                .attr("x",margins.left + 15)
                .attr("y",(d,i) => margins.top + title_height + (i * positions_height)+((positions_height-4)/2) + 4)
                .text(d => d.position+1);

            positions_group.select(".positions_description")
                .transition()
                .duration(transition_time)
                .attr("x",margins.left + 40)
                .attr("y",(d,i) => margins.top + title_height + (i * positions_height)+((positions_height-4)/2) + 4)
                .text(d => onboarding.goals_data.factor_names[d.id]);

        }

        var allocation_data = [];
        var my_allocations = onboarding.goals_data.allocations;
        allocations.forEach(function(d,i){
            var my_value = 0,my_max = 0;
            if(d === "total return"){
                my_value = d3.sum(Object.values(my_allocations));
                my_max = d3.sum(Object.values(onboarding.goal_allocation_max_vals));
            } else {
                my_value = my_allocations[d];
                my_max = onboarding.goal_allocation_max_vals[d];
            }
            var title_converter = {"strategic":"strategic allocation","tactical":"tactical allocation","fund":"fund selection","total return":"total return"}
            allocation_data.push({
                "id":i,
                "title":title_converter[d],
                "value": my_value,
                "max": my_max,
                "proportion": my_value/my_max
            })
        })

        var allocation_colours = [onboarding.colors.lightgold,onboarding.colors.middlegold,onboarding.colors.gold,onboarding.colors.midgreen]

        const allocations_group = svg.selectAll('.allocations_group' + my_class)
            .data(allocation_data)
            .join(function(group){
                var enter = group.append("g").attr("class","allocations_group" + my_class);
                enter.append("rect").attr("class","allocations_background_rect");
                enter.append("rect").attr("class","allocations_rect");
                enter.append("text").attr("class","allocations_title");
                enter.append("text").attr("class","allocations_label");

                return enter;
            });

        var allocations_gap = positions_height/12;
        var allocations_text_gap = positions_height/2.5;

        var text_left = allocation_data.find(f => f.proportion < 0.25) === undefined ? true : false;

        allocations_group.select(".allocations_title")
            .attr("x",text_left === true ? margins.left + 5 : width - margins.right - 5)
            .attr("text-anchor",text_left === true ? "start": "end")
            .attr("y",(d,i) => impact_y + title_height + (i* positions_height))
            .attr("dy",show_goals === true ? 0 :  6 + allocations_gap + (positions_height - allocations_text_gap)/2)
            .text(d => d.title);

        allocations_group.select(".allocations_background_rect")
            .attr("x",margins.left)
            .attr("y",(d,i) => impact_y + title_height+ (i* positions_height)+allocations_gap)
            .attr("width",width-margins.left-margins.right)
            .attr("height",positions_height - allocations_text_gap)
            .attr("fill",show_goals === true ? "white" : "#F0F0F0");

        allocations_group.select(".allocations_rect")
            .transition()
            .duration(transition_time)
            .attr("x",margins.left)
            .attr("y",(d,i) => impact_y + title_height + (i* positions_height)+allocations_gap)
            .attr("fill",(d,i) => allocation_colours[i])
            .attr("width",d => (width-margins.left-margins.right)*d.proportion)
            .attr("height",positions_height-allocations_text_gap);


        var font_size = positions_height-allocations_text_gap-(allocations_gap*2);
        allocations_group.select(".allocations_label")
            .transition()
            .duration(transition_time)
            .attr("font-size", font_size + "px")
            .attr("y",(d,i) => impact_y + title_height+ (i* positions_height) + allocations_gap + ((positions_height-allocations_text_gap)/2) + ((font_size-4)/2))
            .attr("x",d =>  margins.left  + ((width-margins.left-margins.right)*d.proportion))
            .attr("dx",d => d.proportion < 0.8 ? 5 : -5)
            .attr("text-anchor",d => d.proportion < 0.8 ? "start": "end")
            .text(d => percent_format(d.proportion));

    }

    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };


    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };


    my.margins = function(value) {
        if (!arguments.length) return margins;
        margins = value;
        return my;
    };

    my.transition_time = function(value) {
        if (!arguments.length) return transition_time;
        transition_time = value;
        return my;
    };

    my.show_goals = function(value) {
        if (!arguments.length) return show_goals;
        show_goals = value;
        return my;
    };



    return my;
}
