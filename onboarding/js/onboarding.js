load_dashboard();

function load_dashboard(){
    var promises = [];

    promises.push(d3.json("data/base_data.json"));
    promises.push(d3.csv("data/all_funds.csv"));
    promises.push(d3.csv("data/all_benchmark_data.csv"));
    promises.push(d3.json("data/goals_data.json"));
    promises.push(d3.json("data/tradeoffs.json"));
    promises.push(d3.json("data/min_max_settings.json"));


    Promise.all(promises).then(ready);

    function ready(all_data) {
        onboarding.fund_family_data = all_data[1];
        onboarding.benchmark_data = all_data[2];
        onboarding.goals_data = all_data[3];
        onboarding.goals_data.all_sets = Object.keys(all_data[3].sets);
        onboarding.goals_data.all_sets.push("");
        onboarding.goals_data.tradeoffs = all_data[4];
        onboarding.min_max_labels = all_data[5].sets["Default"].labels;
        onboarding.min_max_b_weight = all_data[5].sets["Default"].b_weight;
        onboarding.min_max_value = all_data[5].sets["Default"].value;
        onboarding.min_max_data = all_data[5];
        onboarding.min_max_sets = Object.keys(all_data[5].sets);
        var svg = draw_svg("progress_div");
        draw_progress_chart(svg,all_data[0].stages);
        draw_stage_divs(all_data[0],all_data[2]);
        draw_profile_div(all_data[0]);
    }
}

function draw_profile_div(base_data){
    var svg = draw_svg("profile_div");
    var width = +svg.attr("width");
    var height = +svg.attr("height");

    var required_height = d3.sum(base_data.stages[0].questions, s => s.min_height) + 30;
    if(height < required_height){
        height = required_height;
        svg.attr("height",height);
    }
    svg.append("rect")
        .attr("class","background_rect")
        .attr("x",15)
        .attr("width", width-30)
        .attr("height",height-15)
        .attr("fill","#F0F0F0");

    svg.append("g")
        .attr("class","chart_group")
}

function draw_stage_divs(my_data){
    d3.select("#questions_div").selectAll("div").remove();
    d3.select(".profile_div_svg .chart_group").selectAll("*").remove();
    d3.select(".button_svg").remove();

    var chart_div = document.getElementById("questions_div");
    var width = +chart_div.clientWidth;
    var height = +chart_div.clientHeight;

    var current_stage = my_data.stages.find(f => f.stage_id === onboarding.progress_stage);

    current_stage.questions.forEach(function(d,i){

        var div_height = height * d.height_percent;
        if(div_height < d.min_height){
            div_height = d.min_height;
        }
        var my_div = d3.select("#questions_div")
            .append("div")
            .attr("height",div_height)
            .attr("width", width);

        var my_svg = my_div.append("svg")
            .attr("id", "svg_" + i)
            .attr("height",div_height)
            .attr("width",width);

        my_svg.append("rect")
            .attr("class","title_rect")
            .attr("x",15)
            .attr("width",width-15)
            .attr("height",25)
            .attr("fill",onboarding.colors.darkgreen);

        my_svg.append("text")
            .attr("class","title_text")
            .attr("x",23)
            .attr("y",18)
            .attr("fill","white")
            .text(d.name);

        my_svg.append("text")
            .attr("class","title_icon fa")
            .attr("id",d.progress_point)
            .attr("x",width-20)
            .attr("y",18)
            .attr("fill","white")
            .text("\uf0c7")
            .on("mouseover",function(){d3.select(this).attr("cursor","pointer")})
            .on("mouseout",function(){d3.select(this).attr("cursor","default")})
            .on("click",function(){
                if(d3.select(this).text() === "\uf0c7"){
                    d3.select("#svg_" + i).attr("height",25)
                    d3.select(this).text('\uf044');
                    if(onboarding.progress_position < +this.id){
                        onboarding.progress_position = +this.id;
                        onboarding.progress_chart.reset_progress();
                    }
                } else {
                    if(onboarding.different_height["svg_" + i] === undefined){
                        d3.select("#svg_" + i).attr("height",div_height);
                    } else {
                        d3.select("#svg_" + i).attr("height",onboarding.different_height["svg_" + i]);
                    }
                    d3.select(this).text('\uf0c7')
                }
            })

        if(d.name === "risk categories"){
            draw_risk_categories(my_svg,25,my_data.stages);
        } else if (d.name === "tax status"){
            draw_tax_status(my_svg,25,my_data.stages)
        } else if (d.name === "account size"){
            draw_account_size(my_svg,my_data.stages)
        } else if (d.name === "team"){
            draw_team_chart(my_svg,onboarding.team,my_data.stages)
        } else if (d.name === "fund families"){
            onboarding.families_no = [];
            onboarding.families_yes = [];
            onboarding.funds_no = [];
            onboarding.funds_yes = [];
            onboarding.families_hierarchy = [];
            onboarding.funds_priority = {};
            onboarding.default_families.forEach(function(f){
                onboarding.families_yes.push(f);
                onboarding.families_hierarchy.push({"name": f, "value":1});
            })
            draw_fund_families(my_svg, my_data.stages);
            draw_fund_sunburst(d3.select(".profile_div_svg"),{"name":"all","children":onboarding.families_hierarchy});
        } else if (d.name === "favourite funds"){
            my_svg.attr("class","fund_favourites_svg");
            draw_fund_favourites();
        } else if (d.name === "benchmarks"){
            draw_benchmarks(my_svg);
        } else if (d.name === "min max defaults"){
            draw_min_max_defaults(my_svg);
        } else if (d.name === "other"){
            draw_other_preferences(my_svg);
        } else if (d.name === "goals and trade offs"){
            onboarding.goals_data.svg_id = "svg_" + i;
            onboarding.goals_data.positions = {"current": JSON.parse(JSON.stringify(onboarding.goals_data.sets["Defaults"].positions))};
//            onboarding.goals_data.allocations = JSON.parse(JSON.stringify(onboarding.goals_data.sets["Defaults"].allocations));
            var top_index = onboarding.goals_data.positions.current.findIndex(d => d.position === 0);
            var changes = add_goals_restrictions(top_index,false);
            onboarding.goals_data.allocations = {"strategic":strategic_potential(),"fund":fund_potential(),"tactical":tactical_potential()};
            draw_goals_and_trade_offs(my_svg, "goals_trade_offs",changes,0);
        }
    })

    var button_svg = d3.select("#questions_div")
        .append("svg")
        .attr("class","button_svg")
        .attr("width",width)
        .attr("height",20);

    button_svg.append("rect")
        .attr("class","button_data_rect")
        .attr("x",width-326)
        .attr("y",1)
        .attr("width",150)
        .attr("height",18)
        .attr("fill",onboarding.colors.darkgreen)
        .on("mouseover",function(d){d3.select(this).attr("cursor","pointer")})
        .on("mouseout",function(d){d3.select(this).attr("cursor","default")})
        .on("click",function(d){
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,'
                + encodeURIComponent(JSON.stringify(onboarding.current_download_data)));
            element.setAttribute('download', "my_filename");

            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            document.body.removeChild(element);
        })

    button_svg.append("text")
        .attr("class","button_load_text")
        .attr("x",width-250)
        .attr("y",15)
        .text("DOWNLOAD DATA")
        .attr("pointer-events","none")
        .attr("text-anchor","middle")
        .attr("fill","white");

    button_svg.append("rect")
        .attr("class","button_rect")
        .attr("x",width-171)
        .attr("y",1)
        .attr("width",170)
        .attr("height",18)
        .attr("fill",onboarding.colors.darkgreen)
        .on("mouseover",function(d){d3.select(this).attr("cursor","pointer")})
        .on("mouseout",function(d){d3.select(this).attr("cursor","default")})
        .on("click",function(d){
            onboarding.current_download_data = [];
            if(onboarding.progress_stage > 0){
                if(onboarding.progress_stage === 2){
                    onboarding.progress_stage = 3;
                    onboarding.progress_position = 9;
                    draw_stage_divs(my_data);
                } else if(onboarding.progress_stage === 3){
                    onboarding.progress_stage = 4;
                    onboarding.progress_position = 16;
                    d3.selectAll("#profile_div").selectAll("*").remove();
                    d3.selectAll("#questions_div").selectAll("*").remove();
                    d3.select("#questions_div").style("width","100%");
                    d3.select("#profile_div").style("width",0);
                    var elem = document.createElement("img");
                    elem.setAttribute("src", "bq_image.jpg");
                    elem.setAttribute("height", 740);
                    elem.setAttribute("width", 720);
                    document.getElementById("questions_div").appendChild(elem);
                    d3.select(this).attr("display","none");
                    d3.select(".button_text").attr("display","none");
                } else {
                    onboarding.progress_stage = 2;
                    onboarding.progress_position = 6;
                    draw_stage_divs(my_data);
                }
            } else {
                onboarding.progress_stage = 1;
                onboarding.progress_position = 4;
                draw_stage_divs(my_data);
            }
            onboarding.progress_chart.reset_progress();

        });


    button_svg.append("text")
        .attr("class","button_text")
        .attr("x",width-86)
        .attr("y",15)
        .text("MOVE TO NEXT STAGE")
        .attr("pointer-events","none")
        .attr("text-anchor","middle")
        .attr("fill","white");

}

function draw_other_preferences(svg){

    var width = +svg.attr("width");
    var margins = {"left":15 ,"right":15,"top":50,"bottom":15};

    svg.append("text")
        .style("text-transform","uppercase")
        .attr("x",margins.left)
        .attr("y",margins.top)
        .text("Allowable Investment Types");

    svg.append("text")
        .style("text-transform","uppercase")
        .attr("x",margins.left)
        .attr("y",margins.top + 60)
        .text("Buyable Universe");


    const allowable_group = svg.selectAll('.allowable_group')
        .data(onboarding.allowable_investments)
        .join(function(group){
            var enter = group.append("g").attr("class","allowable_group");
            enter.append("foreignObject").attr("class","allowable_object")
                .append("xhtml:body").append("div").attr("class","allowable_div");
            enter.append("text").attr("class","allowable_label");
            return enter;
        });

    allowable_group.select(".allowable_object")
        .attr("id",(d,i) => "allowable_object_" + i)
        .attr("width",20)
        .attr("height",20)
        .attr("transform","translate(" + margins.left + "," + (margins.top + 10) + ")");

    allowable_group.select(".allowable_div")
        .html((d,i) => "<input type='checkbox' class='allowable other_object' id='" + i + "'>");

    allowable_group.select(".allowable_label")
        .attr("id",(d,i) => "allowable_label_" + i)
        .text(d => d)
        .attr("transform","translate(" + (margins.left + 25) + "," + (margins.top + 24) + ")");

    var label_x = 0,label_y=0;
    d3.selectAll(".allowable_label").each(function(d,i){
        d3.select(this).attr("x",label_x).attr("y",label_y);
        d3.select("#allowable_object_" + i).attr("x",label_x).attr("y",label_y);
        var my_width = document.getElementById(this.id).getBoundingClientRect().width;
        label_x += (my_width+30);
    })

    const buyable_group = svg.selectAll('.buyable_group')
        .data(onboarding.buyable_universe)
        .join(function(group){
            var enter = group.append("g").attr("class","buyable_group");
            enter.append("foreignObject").attr("class","buyable_object")
                .append("xhtml:body").append("div").attr("class","buyable_div");
            enter.append("text").attr("class","buyable_label");
            return enter;
        });

    buyable_group.select(".buyable_object")
        .attr("id",(d,i) => "buyable_object_" + i)
        .attr("width",20)
        .attr("height",20)
        .attr("transform","translate(" + margins.left + "," + (margins.top + 70) + ")");

    buyable_group.select(".buyable_div")
        .html((d,i) => "<input type='radio' class='buyable other_object' name='buyable' id='" + i + "' value='buyable_" + i + "'>");

    buyable_group.select(".buyable_label")
        .attr("id",(d,i) => "buyable_label_" + i)
        .text(d => d)
        .attr("transform","translate(" + (margins.left + 25) + "," + (margins.top + 84) + ")");

    label_x = 0,label_y=0;
    d3.selectAll(".buyable_label").each(function(d,i){
        d3.select(this).attr("x",label_x).attr("y",label_y);
        d3.select("#buyable_object_" + i).attr("x",label_x).attr("y",label_y);
        var my_width = document.getElementById(this.id).getBoundingClientRect().width;
        label_x += (my_width+30);
        if((label_x+150) >  (width - margins.left - margins.right)){
            label_x = 0;
            label_y += 25;
        }
    });

    svg.append("foreignObject")
        .attr("width",200)
        .attr("height",30)
        .attr("x",label_x)
        .attr("y",label_y)
        .attr("transform","translate(" + margins.left + "," + (margins.top + 70) + ")")
        .append("xhtml:body").append("div")
        .html((d,i) => "<input type='text'  class='other_object' id='buyable_other' name='buyable_other'>");

    svg.append("text")
        .style("text-transform","uppercase")
        .attr("y",label_y + 60)
        .attr("transform","translate(" + margins.left + "," + (margins.top + 60) + ")")
        .text("Buyable Universe Exceptions");


    svg.append("foreignObject")
        .attr("width",width - margins.left - margins.right)
        .attr("height",60)
        .attr("y",label_y)
        .attr("transform","translate(" + margins.left + "," + (margins.top + 130) + ")")
        .append("xhtml:body").append("div")
        .html((d,i) => "<textarea class='other_object' id='buyable_exceptions' rows='1' cols='80'></textarea>");

    d3.selectAll(".other_object")
        .on("change",function(d){
            var allowable = [], buyable = [];
            d3.selectAll(".allowable").each(function(d){
                if(this.checked === true){
                    allowable.push(onboarding.allowable_investments[this.id]);
                }
            });
            d3.selectAll(".buyable").each(function(d){
                if(this.checked === true){
                    var my_value = onboarding.buyable_universe[this.id];
                    if(onboarding.buyable_universe[this.id].toLowerCase().includes("other") === true){
                        my_value = "Other: " + d3.select("#buyable_other").node().value;
                    }
                    buyable.push(my_value);
                }
            });
            if(onboarding.progress_position < 9){
                onboarding.progress_position = 9;
                onboarding.progress_chart.reset_progress();
            }
            draw_other_results(d3.select(".profile_div_svg"),
                {"allowable":allowable,"buyable":buyable,"exceptions":d3.select("#buyable_exceptions").node().value});
        })


}

function draw_other_results(svg, my_data){
    var margins = {"left":25 ,"right":15,"top":onboarding.position_ys["benchmark_bar"]+20,"bottom":15};

    if(d3.select(".allowable_investment_result")._groups[0][0] === null) {
        svg.append("text").attr("class","allowable_investment_result");
        svg.append("text").attr("class","buyable_universe");
        svg.append("text").attr("class","buyable_exceptions");
    }

    if(my_data.allowable.length > 0){
        d3.select(".allowable_investment_result")
            .attr("x",margins.left)
            .attr("y",margins.top)
            .text("ALLOWABLE INVESTMENT TYPES: " + my_data.allowable.join(", "));
        margins.top += 25;
    }

    if(my_data.buyable.length > 0){
        d3.select(".buyable_universe")
            .attr("x",margins.left)
            .attr("y",margins.top)
            .text("BUYABLE UNIVERSE: " + my_data.buyable.join(", "));

        margins.top += 25;
    }
    if(my_data.exceptions !== ""){
        d3.select(".buyable_exceptions")
            .attr("x",margins.left)
            .attr("y",margins.top)
            .text("BUYABLE UNIVERSE EXCEPTIONS: " + my_data.exceptions);
    }

    onboarding.current_download_data.push(my_data);



}




function draw_goals_and_trade_offs(svg,my_class,changes,transition_time){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top":80,"bottom":15};

    if(changes === null){
        transition_time = 0;
        changes = {"restrict_left":{},"restrict_right":{},"value":{}}
    }

    var my_chart = dial_chart()
        .width(width-margins.right)
        .height(height-margins.top)
        .my_class(my_class)
        .my_data(onboarding.goals_data.positions.current)
        .changes(changes);

    my_chart(svg);



    var results_svg = d3.select(".profile_div_svg");
    width = +results_svg.attr("width");
    height = +results_svg.attr("height")
    results_svg = results_svg.select(".chart_group");
    margins = {"left":30 ,"right":30,"top":5,"bottom":40};

    onboarding.current_download_data.push(onboarding.goals_data);

    var my_chart = goals_chart_results()
        .width(width)
        .height(height - 15)
        .margins(margins)
        .my_class(my_class)
        .transition_time(0);


    my_chart(results_svg);
}


function draw_min_max_defaults(svg){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top":80,"bottom":15};

    var my_chart = min_max_defaults()
        .width(width)
        .height(height-15)
        .margins(margins)
        .my_slider_svg("profile_div_svg")
        .my_class("min_max_chart");

    my_chart(svg);
}


function draw_min_max_sliders(svg,my_data,position_y){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":5 ,"right":40,"top":position_y-90,"bottom":15};

    onboarding.current_download_data.push(my_data);

    var my_chart = min_max_sliders()
        .width(width)
        .margins(margins)
        .my_data(my_data)
        .my_title("Min Max Limits")
        .my_class("min_max_sliders");

    my_chart(svg.select(".chart_group"));
}


function draw_fund_families(svg,my_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top":80,"bottom":15};

    var my_chart = fund_families()
        .width(width)
        .height(height-15)
        .margins(margins)
        .my_data(my_data)
        .fund_data(onboarding.fund_family_data)
        .my_class("fund_family_chart");

    my_chart(svg);
}


function draw_benchmarks(svg){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":2,"top":80,"bottom":15};

    var my_chart = benchmark_entry()
        .width(width)
        .height(height-15)
        .margins(margins)
        .my_data(onboarding.benchmark_data)
        .my_class("benchmark_entry_chart");

    my_chart(svg);
}


function draw_benchmark_bar(svg,my_data,height,position_y){

    var width = +svg.attr("width");
    var margins = {"left":20 ,"right":30,"top":0,"bottom":0};

    onboarding.current_download_data.push(my_data);

    var my_chart = benchmark_bar()
        .width(width)
        .height(height*1.5)
        .margins(margins)
        .my_data(my_data)
        .my_class("benchmark_profile_chart");

    my_chart(svg.select(".chart_group"));
}


function draw_fund_favourites(){

    var svg = d3.select(".fund_favourites_svg");
    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top":80,"bottom":15};

    var my_chart = fund_favourites()
        .width(width)
        .height(height-15)
        .margins(margins)
        .my_data(onboarding.families_hierarchy)
        .fund_data(onboarding.fund_family_data)
        .my_class("fund_favourites_chart");

    my_chart(svg);
}


function draw_fund_sunburst(svg,my_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":20 ,"right":60,"top":0,"bottom":15};

    onboarding.current_download_data.push(my_data);

    var my_chart = fund_sunburst()
        .width(width)
        .height(height-15)
        .margins(margins)
        .my_data(my_data)
        .my_class("fund_family_sunburst");

    my_chart(svg.select(".chart_group"));
}


function draw_team_chart(svg,my_data,base_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top":60,"bottom":0};

    var my_chart = team_chart().width(width)
        .height(height-15)
        .margins(margins)
        .my_data(my_data)
        .base_data(base_data)
        .my_class("team_chart");

    my_chart(svg);
}


function draw_team_profile(svg,my_data,height,position_y){

    var width = +svg.attr("width");
    var margins = {"left":15 ,"right":15,"top": position_y+15,"bottom":0};

    onboarding.current_download_data.push(my_data);

    var my_chart = team_profile().width(width)
        .height(height)
        .margins(margins)
        .my_data(my_data)
        .my_class("team_profile_chart");

    my_chart(svg.select(".chart_group"));
}



function draw_account_size(svg,base_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top": 15,"bottom":0};

    var my_chart = account_size().width(width)
        .height(height)
        .margins(margins)
        .base_data(base_data)
        .my_class("account_size_chart");

    my_chart(svg);
}


function draw_account_size_chart(svg,my_data,my_labels,height,position_y){

    var width = +svg.attr("width");
    var margins = {"left":20 ,"right":15,"top":position_y+15,"bottom":0};

    onboarding.current_download_data.push(my_data);

    var my_chart = account_size_circles().width(width)
        .height(height-15)
        .margins(margins)
        .my_data(my_data)
        .my_labels(my_labels)
        .my_class("account_size_circles");

    my_chart(svg.select(".chart_group"));
}


function draw_progress_chart(svg,my_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15,"right":15,"top":0,"bottom":0};

    onboarding.progress_chart = progress_bar().width(width)
        .height(height)
        .margins(margins)
        .my_data(my_data)
        .my_class("progress_bar");

    onboarding.progress_chart(svg);
}


function draw_risk_categories(svg,start_y,base_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":20,"right":20,"top":start_y + 15,"bottom":15};

    var my_chart = risk_categories().width(width)
        .height(height)
        .margins(margins)
        .base_data(base_data)
        .my_class("risk_categories");

    my_chart(svg);
}


function draw_tax_status(svg,start_y,base_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":20,"right":20,"top":start_y + 15,"bottom":15};

    var my_chart = tax_status().width(width)
        .height(height)
        .margins(margins)
        .base_data(base_data)
        .my_class("tax_status");

    my_chart(svg);
}


function draw_tree_map(my_data,div_height){

    var svg = d3.select(".profile_div" + "_svg");
    var width = +svg.attr("width");
    var margins = {"left":20,"right":20,"top": 40,"bottom":15};

    onboarding.current_download_data.push(my_data);

    var my_chart = tree_map().width(width)
        .height(div_height)
        .margins(margins)
        .my_data(my_data)
        .my_class("risk_category_treemap");

    my_chart(svg.select(".chart_group"));
}


function draw_tax_bar(my_data,div_height,start_y){

    var svg = d3.select(".profile_div" + "_svg");
    var width = +svg.attr("width");
    var margins = {"left":20,"right":20,"top": start_y,"bottom":15};

    onboarding.current_download_data.push(my_data);

    var my_chart = tax_bar().width(width)
        .height(div_height)
        .margins(margins)
        .my_data(my_data)
        .my_class("tax_status_bar");

    my_chart(svg.select(".chart_group"));
}


function draw_svg(div_id){

    //draw svg - responsive to container div.
    var chart_div = document.getElementById(div_id);
    var width = chart_div.clientWidth;
    var height = chart_div.clientHeight;

    if(d3.select("." + div_id + "_svg")._groups[0][0] === null){
        var svg = d3.select("#" + div_id)
            .append("svg")
            .attr("class",div_id + "_svg")
            .attr("width",width)
            .attr("height",height);

        onboarding.texture = textures.lines().thicker().stroke("#A0A0A0");
        onboarding.texture_gold = textures.lines().thicker().stroke("gold");
        svg.call(onboarding.texture);
        svg.call(onboarding.texture_gold);

    } else {
        var svg = d3.select("." + div_id + "_svg");
    }
    return svg;
}
