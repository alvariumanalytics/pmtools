
var view_mode = "summary";  //summary or edit

function cancel_button_click(){

}

function save_button_click(){

}

var promises = [];

promises.push(d3.json("data/goals_data.json"));
promises.push(d3.json("data/tradeoffs.json"));


Promise.all(promises).then(ready);

function ready(all_data) {
    onboarding.goals_data = all_data[0];
    onboarding.goals_data.all_sets = Object.keys(all_data[0].sets);
    onboarding.goals_data.all_sets.push("");
    onboarding.goals_data.tradeoffs = all_data[1];

    var svg = draw_svg("standalone_g_chart_div");

    onboarding.goals_data.positions = {"current": JSON.parse(JSON.stringify(onboarding.goals_data.sets["Defaults"].positions))};
    var top_index = onboarding.goals_data.positions.current.findIndex(d => d.position === 0);
    var changes = add_goals_restrictions(top_index,false);
    onboarding.goals_data.allocations = {"strategic":strategic_potential(),"fund":fund_potential(),"tactical":tactical_potential()};
    draw_goals_and_trade_offs(svg, "goals_trade_offs",changes,0);

}

function draw_goals_and_trade_offs(svg,my_class,changes,transition_time){

    console.log("BRYONY",changes)
    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = {"left":15 ,"right":15,"top":80,"bottom":150};

    if(changes === null){
        transition_time = 0;
        changes = {"restrict_left":{},"restrict_right":{},"value":{}}
    }

    var my_chart = dial_chart()
        .width(width-margins.right)
        .height(height-margins.top-margins.bottom)
        .my_class(my_class)
        .my_data(onboarding.goals_data.positions.current)
        .changes(changes)
        .view_mode(view_mode);

    my_chart(svg);


    var results_margins = {"left":15 ,"right":15,"top":60 + (height - margins.bottom - margins.top),"bottom":0};

    onboarding.current_download_data.push(onboarding.goals_data);

    var my_chart = goals_chart_results()
        .width(width)
        .height(height - results_margins.top - results_margins.bottom - 20)
        .margins(results_margins)
        .my_class(my_class)
        .transition_time(transition_time)
        .show_goals(false);

    my_chart(svg);
}


function draw_svg(div_id){


    //draw svg - responsive to container div.
    var chart_div = document.getElementById(div_id);
    var width = chart_div.clientWidth;
    var height = chart_div.clientHeight;

    if(d3.select("." + div_id + "_svg")._groups[0][0] === null){
        var svg = d3.select("#" + div_id)
            .append("svg")
            .attr("class",div_id + "_svg")
            .attr("id",div_id + "_svg")
            .attr("width",width)
            .attr("height",height);

        onboarding.goals_data.svg_id = div_id + "_svg";

        onboarding.texture = textures.lines().thicker().stroke("#A0A0A0");
        onboarding.texture_gold = textures.lines().thicker().stroke("gold");
        svg.call(onboarding.texture);
        svg.call(onboarding.texture_gold);

    } else {
        var svg = d3.select("." + div_id + "_svg");
    }
    return svg;
}
