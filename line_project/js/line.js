load_data();

function load_data(){

    var promises = [
        d3.csv("data/events.csv"),
        d3.csv("data/recessions.csv"),
        d3.csv("data/pc.csv")
    ];

    Promise.all(promises).then(ready);

    function ready(all_datasets) {

        all_datasets[2].forEach(function(d){
            d.date = convert_date(d.date);
        });

        all_datasets[0].forEach(function(d){
            d.date = convert_date(d.date);
        });

        all_datasets[1].forEach(function(d){
            d.start = convert_recession_date(d.start);
            d.end = convert_recession_date(d.end);
        });

        var date_range = d3.extent(all_datasets[2], d => d.date);
        var my_json = {
            "date_range":date_range,
            "events":all_datasets[0],
            "recessions":all_datasets[1],
            "line_charts":[all_datasets[2]]
        }
        draw_chart(my_json,"chart_div");
    }

}

function convert_date(my_date){

    my_date = my_date.split("/");
    return new Date(my_date[2],+my_date[0]-1,my_date[1]);
}

function convert_recession_date(my_date){
    my_date = my_date.split("-");
    return new Date(my_date[0],+my_date[1]-1,1);
}

function draw_chart(my_data,div_id) {

    var svg = draw_svg(div_id);
    draw_line_charts(svg, my_data);
}


function draw_line_charts(svg,my_data){

    var width = +svg.attr("width");
    var height = +svg.attr("height");
    var margins = 50;

    var my_chart = line_charts()
        .width(width - (margins*2))
        .height(height - (margins*2))
        .start_x(margins)
        .start_y(margins)
        .my_class("alv_line_charts")
        .my_data(my_data);

    my_chart(svg);

}

function draw_svg(div_id){

    //draw svg - responsive to container div.
    var chart_div = document.getElementById(div_id);
    var width = chart_div.clientWidth;
    var height = chart_div.clientHeight;

    if(d3.select("." + div_id + "_svg")._groups[0][0] === null){
        var svg = d3.select("#" + div_id)
            .append("svg")
            .attr("class",div_id + "_svg")
            .attr("width",width)
            .attr("height",height);

    } else {
        var svg = d3.select("." + div_id + "_svg");
    }
    return svg;
}
