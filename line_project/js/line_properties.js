var line_project = {
    gradient_stops:{"0":{"top":0.95,"top_mid":0.9}},
    threshold_below_average_step:{"0":0.05},
    chart_labels:{"0":{"main":"Main Metric","top":"50 day MA","bottom":"100 day MA"}},
    chart_selected:{"0":"main"},
    chart_selected_index:{"0":0},
    chart_titles:{
        "0":{"title":"PCA Market Drivers Concentration","description": "This chart shows the concentration of market drivers for the S&P 500 based on principal components analysis. High levels are an indication of market fragility and higher risk"}
    }
};
