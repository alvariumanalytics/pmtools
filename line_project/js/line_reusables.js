function line_charts() {
    //REUSABLE ticker chart

    var width=0,
        height=0,
        start_x=0,
        start_y=0,
        my_data = [],
        my_class="",
        y_scales={},
        data_columns={},
        extra_right = 100;


    function my(svg) {

        const focusHeight = 50;
        let chart_height = height - focusHeight - 50;
        let chart_width = width;
        let transform_x = 0;
        //line in brush is always "value" in the first dataset
        let brush_y_extent = d3.extent(my_data.line_charts[0],d => d.value);
        if(my_data.line_charts.length > 1){
            chart_width = (width/2)-10;
            transform_x = (width/2) + 5;
            chart_height = chart_height/(my_data.line_charts.length % 2);
        }
        const brush_x_scale = d3.scaleTime().domain(my_data.date_range).range([0,width]);
        const brush_y_scale = d3.scaleLinear().domain(brush_y_extent).range([focusHeight,0]);
        const x_scale = d3.scaleTime().domain(my_data.date_range).range([0,chart_width- extra_right]);
        const color_range =  ['#d7191c', '#fdae61', '#ffffbf', '#a6d96a', '#1a9641'];
        const time_format = d3.timeFormat("%d %b %Y");
        var percent_format = d3.format(".5%");
        var basic_percent_format = d3.format(".0%");


        if(d3.select(".brush_axis" + my_class)._groups[0][0] === null) {
            svg.append("g").attr("class", "axis brush_axis" + my_class);
            svg.append("g").attr("class", "brush brush_group" + my_class);
            svg.append("path").attr("class","brush_line brush_line" + my_class)
            svg.append("defs").append("clipPath").attr("id","clip")
                .append("rect").attr("class","clip_rect" + my_class);
            svg.append("circle").attr("class","mouse_item" + my_class + " mouse_dot mouse_dot" + my_class);
            svg.append("text").attr("class","mouse_item" + my_class + " mouse_text top_mouse_text" + my_class);
            svg.append("text").attr("class","mouse_item" + my_class + " mouse_text bottom_mouse_text" + my_class);
        }


        d3.select(".clip_rect" + my_class)
            .attr("width",chart_width - extra_right)
            .attr("height",chart_height);

        d3.select(".brush_axis" + my_class)
            .call(d3.axisBottom(brush_x_scale).tickSizeOuter(0))
            .attr("transform","translate(" + start_x + "," + (start_x + height) + ")");

        my_data.line_charts.forEach(function(data,i){
            data_columns[i] = data.columns.filter(f => f !== "date");
            var my_max = d3.max(data_columns[i], function(cols){
                return d3.max(data, m => +m[cols])
            });
            var my_min = d3.min(data_columns[i], function(cols){
                return d3.min(data, m => +m[cols])
            })
            var my_extent = [my_min * 0.95, my_max*1.05];
            data.value_average = d3.mean(data, m => m.value);
            y_scales[i] = d3.scaleLinear().domain(my_extent).range([chart_height,0]);
        })

        //this needs to be changes so there is a rect over every chart
        //only this and transform x/y adjustments and should be small multiple
        svg.on("mousemove",function(d){
            var current_date = x_scale.invert(d3.event.offsetX - start_x);
            if(current_date > x_scale.domain()[0]){
                var y_val = 0;
                var y_text = "";
                var y_date = "";
                var in_range = false;
                for(i = 0 ; i < my_data.line_charts[0].length ; i++){
                    y_date = my_data.line_charts[0][i].date;
                    if(y_date > current_date){
                        y_text = my_data.line_charts[0][i][data_columns[0][line_project.chart_selected_index[0]]];
                        y_val = y_scales[0](y_text);
                        in_range = true;
                        break;
                    }
                }
                if(in_range === true){
                    d3.select(".mouse_dot" + my_class)
                        .style("visibility","visible")
                        .attr("cx",d3.event.offsetX)
                        .attr("cy",start_y + y_val)
                        .raise();

                    d3.select(".top_mouse_text" + my_class)
                        .style("visibility","visible")
                        .attr("x",d3.event.offsetX)
                        .attr("y",start_y + y_val - 4)
                        .text(basic_percent_format(y_text))
                        .raise();

                    d3.select(".bottom_mouse_text" + my_class)
                        .style("visibility","visible")
                        .attr("x",d3.event.offsetX)
                        .attr("y",start_y + y_val + 10)
                        .text(time_format(y_date))
                        .raise();
                } else {
                    d3.selectAll(".mouse_item" + my_class)
                        .style("visibility","hidden");
                }
            }
        })

        const brush = d3.brushX()
            .extent([[0, 0.5], [width, focusHeight]])
            .on("brush end", brushed);

        d3.select(".brush_group" + my_class)
            .call(brush)
            .attr("transform","translate(" + start_x + "," + (start_y + height - focusHeight - 5) + ")")
            .call(brush.move, brush_x_scale.range());

        my_data.recessions = my_data.recessions.filter(d => d.end >= x_scale.domain()[0])
        //ticker rects and text labels
        var recession_group = svg.selectAll('.recession_rect_group')
            .data(my_data.recessions, d => d)
            .join(function(group){
                var enter = group.append("g").attr("class","recession_rect_group");
                enter.append("rect").attr("class","recession_rect");
                return enter;
            });

        recession_group.select(".recession_rect")
            .attr("pointer-events","none")
            .attr("clip-path","url(#clip)")
            .attr("height",chart_height)
            .attr("fill","steelblue")
            .attr("opacity",0.1)
            .attr("transform","translate(" + (start_x + transform_x) + "," + start_y + ")");


        //ticker rects and text labels
        var my_group = svg.selectAll('.line_chart_group')
            .data(my_data.line_charts, d => d)
            .join(function(group){
                var enter = group.append("g").attr("class","line_chart_group");
                enter.append("g").attr("class","axis x_axis");
                enter.append("g").attr("class","axis y_axis");
                enter.append("text").attr("class","title")
                enter.append("path").attr("class","all_paths range_area").attr("clip-path","url(#clip)");
                enter.append("line").attr("class","average_line").attr("clip-path","url(#clip)");
                enter.append("path").attr("class","all_paths main_line").attr("clip-path","url(#clip)");
                enter.append("path").attr("class","all_paths top_line").attr("clip-path","url(#clip)");
                enter.append("path").attr("class","all_paths bottom_line").attr("clip-path","url(#clip)");
                enter.append("text").attr("class","line_labels main_label");
                enter.append("text").attr("class","line_labels top_label");
                enter.append("text").attr("class","line_labels bottom_label");

                var gradient = enter.append("defs").append("linearGradient")
                    .attr("class","line_gradient");
                gradient.attr("gradientTransform", "rotate(90)");
                gradient.append("stop").attr("class","top");
                gradient.append("stop").attr("class","mid_top");
                gradient.append("stop").attr("class","middle");
                gradient.append("stop").attr("class","mid_bottom");
                gradient.append("stop").attr("class","bottom");
                return enter;
            });

        my_group.select(".title")
            .attr("x",start_x + transform_x)
            .attr("y",start_y-10)
            .text((d,i) => line_project.chart_titles[i].title)
            .on("mousemove",function(d,i){
                d3.select(this).attr("cursor","pointer");
                d3.select(".line_tooltip")
                    .style("visibility","visible")
                    .style("left",d3.event.offsetX + 10 + "px")
                    .style("top",d3.event.offsetY + 5 + "px")
                    .text(line_project.chart_titles[i].description);
            })
            .on("mouseout",function(d){
                d3.select(this).attr("cursor","default");
                d3.select(".line_tooltip").style("visibility","hidden")
            });

        my_group.select(".main_label")
            .attr("id","main_line")
            .attr("font-weight","bold")
            .attr("x",start_x + transform_x + chart_width - extra_right + 5)
            .attr("y",start_y + 15)
            .text((d,i) => line_project.chart_labels[i].main);

        my_group.select(".top_label")
            .attr("id","top_line")
            .attr("x",start_x + chart_width - extra_right + 5)
            .attr("y",start_y + 30)
            .text((d,i) => line_project.chart_labels[i].top);

        my_group.select(".bottom_label")
            .attr("id","bottom_line")
            .attr("x",start_x + chart_width - extra_right + 5)
            .attr("y",start_y + 45)
            .text((d,i) => line_project.chart_labels[i].bottom);

        d3.selectAll(".line_labels")
            .on("mouseover",function(d){
                d3.select(this).attr("cursor","pointer");
                label_mouseover(this,d,i);
            })
            .on("mouseout",function(d,i){
                d3.select(this).attr("cursor","default");
                d3.selectAll(".all_paths").attr("opacity",1).style("stroke-width",0.5).style("stroke","#D0D0D0");
                d3.select("." + line_project.chart_selected[0] + "_line")
                    .style("stroke",(d,i) => "url(#linear-gradient" + i +  ")")
                    .style("stroke-width",2);
                d3.selectAll(".line_labels").attr("opacity",1);
                d3.select("."  + line_project.chart_selected[0] + "_label").attr("font-weight","bold");
            })
            .on("click", function(d,i){
                line_project.chart_selected_index[0] = i;
                line_project.chart_selected[0] = this.id.split("_")[0];
                label_mouseover(this,d,i);
            });

            function label_mouseover (my_object, d, i){

                d3.selectAll(".line_labels").attr("font-weight","normal").attr("opacity",0.1);
                d3.select(my_object).attr("font-weight","bold").attr("opacity",1);
                d3.selectAll(".all_paths").attr("opacity",0.4).style("stroke-width",1).style("stroke","#A0A0A0");
                d3.select("." + my_object.id.split("_")[0] + "_line")
                    .attr("opacity",1)
                    .style("stroke",(d,i) => "url(#linear-gradient" + i +  ")")
                    .style("stroke-width",2)
            }

        my_group.select(".line_gradient")
            .attr("id",(d,i) => "linear-gradient" + i);


        my_group.select(".x_axis")
            .call(d3.axisBottom(x_scale).tickSizeOuter(0))
            .attr("transform","translate(" + (start_x + transform_x) + "," + (start_y + chart_height) + ")")

        my_group.select(".y_axis").each(function(d,i){
            d3.select(this)
                .call(d3.axisLeft(y_scales[i]).tickSizeOuter(0).tickFormat(d => d3.format(".0%")(d)))
                .attr("transform","translate(" + (start_x + transform_x) + "," + start_y  + ")");
        })

        my_group.select(".main_line")
            .style("stroke",function(d,i){
                var my_gradient = d3.select("#linear-gradient" + i);
                var top_threshold = line_project.gradient_stops[i]["top"];
                var domain_max = y_scales[i].domain()[1];
                var y_extent = y_scales[i].domain()[1] - y_scales[i].domain()[0];

                my_gradient.select(".top")
                    .attr("offset","0%")
                    .attr("stop-color",color_range[0]);

                var mid_top_percent = (domain_max - top_threshold)/y_extent;
                my_gradient.select(".mid_top")
                    .attr("offset",percent_format(mid_top_percent))
                    .attr("stop-color",color_range[1]);

               var top_mid_threshold =  line_project.gradient_stops[i]["top_mid"];
                var mid_percent = (domain_max - top_mid_threshold)/y_extent;
                my_gradient.select(".middle")
                    .attr("offset",percent_format(mid_percent))
                    .attr("stop-color",color_range[2]);

                var mid_bottom_percent = (domain_max - d.value_average)/y_extent;
                my_gradient.select(".mid_bottom")
                    .attr("offset",percent_format(mid_bottom_percent))
                    .attr("stop-color",color_range[3]);

                my_gradient.select(".bottom")
                    .attr("offset","100%")
                    .attr("stop-color",color_range[4]);

                return "url(#linear-gradient" + i +  ")";
            })
            .attr("transform","translate(" + (start_x + transform_x) + "," + start_y  + ")");

        my_group.select(".top_line")
            .attr("transform","translate(" + (start_x + transform_x) + "," + start_y  + ")");

        my_group.select(".bottom_line")
            .attr("transform","translate(" + (start_x + transform_x) + "," + start_y  + ")");

        my_group.select(".range_area")
            .attr("transform","translate(" + (start_x + transform_x) + "," + start_y  + ")");

        my_group.select(".average_line")
            .attr("x1",chart_width - extra_right)
            .attr("transform","translate(" + (start_x + transform_x) + "," + start_y  + ")");

        d3.select(".brush_line" + my_class)
            .attr("stroke",(d,i) => "url(#linear-gradient" + i +  ")")
            .attr("d", get_brush_line)
            .attr("transform","translate(" + (start_x + transform_x) + "," + (start_y + height - focusHeight - 5) + ")");


        my_data.events = my_data.events.filter(d => d.date >= x_scale.domain()[0]);
        my_data.events = my_data.events.filter(d => d.date <= x_scale.domain()[1]);
        //ticker rects and text labels
        var events_group = svg.selectAll('.events_circle_group')
            .data(my_data.events, d => d)
            .join(function(group){
                var enter = group.append("g").attr("class","events_circle_group");
                enter.append("line").attr("class","events_line");
                enter.append("text").attr("class","events_text");
                return enter;
            });

        events_group.select(".events_text")
            .attr("pointer-events","none")
            .attr("font-size",10);

        events_group.select(".events_line")
            .attr("pointer-events","none")
            .attr("clip-path","url(#clip)")
            .attr("stroke","steelblue")
            .attr("transform","translate(" + (start_x + transform_x) + "," + start_y + ")");


        draw_focus();

        function draw_focus(){

            d3.selectAll(".recession_rect")
                .attr("x",d => x_scale(d.start))
                .attr("width",d => x_scale(d.end) - x_scale(d.start))

            d3.selectAll(".events_line")
                .attr("x1",d => x_scale(d.date))
                .attr("x2",d => x_scale(d.date))
                .attr("y1",0)
                .attr("y2",chart_height);

            //cheat here as clip path doesn't work on rotated text..
            d3.selectAll(".events_text")
                .text(d => (x_scale(d.date) >= x_scale.range()[0]
                    && x_scale(d.date) <= x_scale.range()[1]) ? d.event : "")
                .attr("transform",d => "translate(" + (start_x + transform_x
                    + x_scale(d.date) + 5) + "," + (start_y + 5) + ") rotate(90)")


            d3.selectAll(".events_line_mouseover")
                .attr("x1",d => x_scale(d.date))
                .attr("x2",d => x_scale(d.date))
                .attr("y1",0)
                .attr("y2",chart_height);

            d3.selectAll(".main_line")
                .attr("d", (d,i) => generate_line("value",d,i))

            d3.selectAll(".top_line")
                .attr("d", (d,i) =>  data_columns[i][1] !== undefined ? generate_line(data_columns[i][1],d,i) : "")

            d3.selectAll(".bottom_line")
                .attr("d", (d,i) =>  data_columns[i][2] !== undefined ? generate_line(data_columns[i][2],d,i) : "")

            d3.selectAll(".range_area")
                .attr("d", (d,i) => generate_area(d,i))

            d3.selectAll(".average_line")
                .attr("x1",chart_width - extra_right)
                .attr("y2",(d,i) => y_scales[i](d.value_average))
                .attr("y1",(d,i) => y_scales[i](d.value_average))
        }
        function get_max(d,i){
            return d3.max(data_columns[i], m => isNaN(d[m]) === true ? y_scales[i].domain()[0] : +d[m])
        }

        function get_min(d,i){
            return d3.min(data_columns[i], m => isNaN(d[m]) === true ? y_scales[i].domain()[1] : +d[m])
        }

        function generate_area(d,i){
            return d3.area()
                .x(l => x_scale(l.date))
                .y0(l => y_scales[i](get_max(l,i)))
                .y1(l => y_scales[i](get_min(l,i)))
                (d)
        }
        function generate_line(my_var,d,i){
            return d3.line()
                .defined(l => l[my_var] !== "NA")
                .x(l => x_scale(l.date))
                .y(l => y_scales[i](l[my_var]))
                (d);
        }

        function get_brush_line(){
            return d3.line()
                .x(d => brush_x_scale(d.date))
                .y(d => brush_y_scale(d.value))
                (my_data.line_charts[0]);
        }

        function brushed() {
            if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
            var s = d3.event.selection || brush_x_scale.range();
            x_scale.domain(s.map(brush_x_scale.invert, brush_x_scale));
            draw_focus(my_group);
        }


    }


    my.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return my;
    };

    my.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return my;
    };

    my.start_x = function(value) {
        if (!arguments.length) return start_x;
        start_x = value;
        return my;
    };

    my.start_y = function(value) {
        if (!arguments.length) return start_y;
        start_y = value;
        return my;
    };

    my.my_data = function(value) {
        if (!arguments.length) return my_data;
        my_data = value;
        return my;
    };

    my.my_class = function(value) {
        if (!arguments.length) return my_class;
        my_class = value;
        return my;
    };
    return my;
}
